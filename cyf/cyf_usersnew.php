<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Users';
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
$( document ).on( "vclick", "a#vuser", function() {
	var p= {eaction: 'Vuser'};
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( "vclick", "a#duser", function() {
	var p= {
        eaction: 'Duser',
        eid:     $('#Gnid').val()
    };
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( "vclick", "a#cuser", function() {
	var p= {
        eaction: 'Cuser',
        eid:     $('#Gnid').val(),
        contact: $('#uContact').val(),
        address: $('#uAddress').val(),
        tel: $('#utel').val(),
        lang: $('#ulanguage').val(),
        dstart: $('#udstart').val(),
        dend: $('#udend').val(),
        cname: $('#ucname').val(),
        manager: $('#umanager').val(),
        membergroups: $('#uGroups').val(),
        email: $('#uemail').val()
    };
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( "vclick", "a#suser", function() {
	var p= {
        eaction: 'Suser',
        eid:     $('#Gnid').val(),
        owner:  $('select#Gowner').val()
    };
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( "vclick", "a#nuser", function() {
    //email contact address tel lang dstart dend cname manager status membergroups
	var p= {
        eaction: 'Nuser',
        contact: $('#uContact').val(),
        address: $('#uAddress').val(),
        tel: $('#utel').val(),
        lang: $('#ulanguage').val(),
        dstart: $('#udstart').val(),
        dend: $('#udend').val(),
        cname: $('#ucname').val(),
        manager: $('#umanager').val(),
        membergroups: $('#uGroups').val(),
        email: $('#uemail').val(),
        pass: $('upassword').val()
    };
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
//id="Gaid" data-user-id="1" data-user-email="CYFuser0@gmail.com" data-user-manager="CYF Manager"
function getUser(){
};
$( document ).on( 'vclick', "a#Gaid", function() {
    $('#Gnid').val($(this).data('userId'));
    if($(this).data('userId')!=0){
        $('#uemail').val($(this).data('userEmail'));
        var p= {eaction: 'Juser', eid: $('#Gnid').val()};
        $.ajax({url:"../cyf/cyf_user.php",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg.id!=0){
                $('#uContact').val(pg.contact);
                $('#uAddress').val(pg.address);
                $('#utel').val(pg.tel);
                $('#ulanguage').val(pg.lang);
                $('#udstart').val(pg.dstart);
                $('#udend').val(pg.dend);
                $('#ucname').val(pg.cname);
                $('#umanager').val(pg.manager);
                $('#uGroups').val(pg.membergroups);
                $('#uemail').val(pg.email);
                }
            }
            }
        });
    }
    return true;
});
$(document).on( "pageinit", "#listuser", function( event ) {
//	var p= {eaction: 'Vuser'};
//    console.log("pageinit");
//	$("#users").load("../cyf/cyf_user.php", p);
//    $('#listuser').find( ":jqmData(role=listview)" ).listview().listview("refresh");
});
/*
$(document).on( "pagebeforeshow", "#newuser", function( event ) {
//    $('select#Gowner').selectmenu().selectmenu('refresh',true);
    var p= {eaction: 'Juser', eid:$('#Gnid').val()};
//            console.log(p);    
	$.ajax({url:"../cyf/cyf_user.php",data: p,
            dataType:"json",
            success: function(msg){
            console.log("msg:"+msg);
            if(msg.length>0){
            var pg =   JSON.parse(msg);
            console.log(pg.id);
            $('#uContact').val(pg.contact);
            $('#uAddress').val(pg.address);
            $('#utel').val(pg.tel);
            $('#ulanguage').val(pg.lang);
            $('#udstart').val(pg.dstart);
            $('#udend').val(pg.dend);
            $('#ucname').val(pg.cname);
            $('#umanager').val(pg.manager);
            $('#uGroups').val(pg.membergroups);
            $('#uemail').val(pg.email);
//            $('upassword').val(pg.);
            }
        }
    });
});
*/
$(document).on( "pagebeforeshow", "#listuser", function( event ) {
//    console.log("pagebeforeshow");
//	var p= {eaction: 'Jusers'};
//    $.ajax({url: "../cyf/cyf_user.php",data: p,
//            success: function(msg){
//            console.log(msg);
//        }
//    });
	var p= {eaction: 'Vuser'};
	$("#users").load("../cyf/cyf_user.php", p);
//    $('#listuser').find( ":jqmData(role=listview)" ).listview().listview("refresh");
});
$(document).ready(function (){
//	var p= {eaction: 'Vuser'};
//	$("#users").load("../cyf/cyf_user.php", p);
    $.mobile.changePage('#listuser');
});
</script>
<?php
?>
</head>
<body>
<!-- cyf_users.php -->	
<input type='hidden' name='Gnid' id='Gnid'>
<?php
?>
<div data-role="page" id="blankuser" data-position="fixed"> <!-- page -->
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
	  </li>
      <li>
	  </li>
    </ul>
  </div>
</div>
<div data-role="content" class="ui-content">
<h3></h3>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listuser" class="ui-btn ui-btn-corner-all ui-shadow" id="vuser">Refresh Users</a>
<a href='#newuser' data-role="button" data-icon="plus" class="ui-bar">Create/Edit User</a>
</div>
</div>

<!-- page -->    
<div data-role="page" id="listuser" data-position="fixed"> 
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
	  </li>
      <li>
	  </li>
    </ul>
  </div>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="users">
<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>
<li><a href='#newuser' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid' data-user-id='0' data-user-gname=' ' data-user-owner='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#newuser' data-role="button" data-icon="plus" class="ui-bar">Create/Edit User</a>
</div>
</div>
    
 <!-- page -->   
<div data-role="page" data-dialog="true" data-mini="true" id="newuser"> 
<div data-role="header" data-position="fixed">
    <h2 id="usertitle">User</h2>
</div>
<div data-role="content" class="ui-content">
 <ul data-role="listview">

    <li>
		<label for="uContact">Contact:</label>
		<input type="text" name="uContact" id="uContact" placeholder="Contact name ...">
		<label for="uAddress">Address:</label>
		<input type="text" name="uAddress" id="uAddress" placeholder="Address ...">
		<label for="utel">tel:</label>
		<input type="text" name="utel" id="utel" placeholder="tel ...">
		<label for="ulanguage">language:</label>
		<select name="ulanguage" id="ulanguage" data-native-menu="false">
		  <option data-placeholder="true">en</option>
          <option value="en">en</option>
          <option value="fi">fi</option>
          <option value="ru">ru</option>
		</select>
    </li>
    <li>
		<label for="uGroups">Groups (membership):</label>
		<select name="uGroups" id="uGroups" data-native-menu="false" data-mini="true" multiple="multiple">
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
    </li>
    <li>
		<label for="uemail">email:</label>
		<input type="text" name="uemail" id="uemail" placeholder="email ...">
		<label for="upassword">password:</label>
		<input type="password" name="upassword" id="upassword">
    </li>
    <li>
		<label for="umanager">Manager:</label>
		<input type="text" name="umanager" id="umanager">
    </li>


    <li>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="duser">Delete user</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cuser">Change user</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nuser">Create user</a>
    </li>

 </ul>
</div>
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="vuser">Refresh Users</a>
</div>
</div>
</body>
</html>


                                                                        