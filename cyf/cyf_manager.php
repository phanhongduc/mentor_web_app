<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

if ($param['email'] == 'guest') header("Location: https://www.google.com");
$cyf = cyfConnect();
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Contracts';
$lang= $param['lang'];

if(!empty($_POST['act']) AND ($_POST['act']=="Nlogin")){
    $q = "INSERT INTO logins (name,ownergroups,membergroups,email,psword) VALUES (";
    $q.= " '".pg_escape_string($_POST['nm'])."'";
    $q.= ",'{".pg_escape_string($_POST['ogr'])."}'";
    $q.= ",'{".pg_escape_string($_POST['mgr'])."}'";
    if(!empty($_POST['em'])){
        $q.= ",'".pg_escape_string($_POST['em'])."'";
        if(!empty($_POST['ps'])){
            $q.= ",'".pg_escape_string($_POST['ps'])."'";
            $q.= ");";
            $res = pg_query($conn, $q);
            if ($res) {
                echo "Login created!";
            }else
                echo "Dublicate login";
        }
    }
    exit();
}
//delete User
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Duser")){
     if(!empty($_REQUEST['eid'])) {
        $q = "DELETE FROM users";
        $q.= " WHERE id={$_REQUEST['eid']}";
        $q.= " AND (";
        $q.= " manager='".$param['email']."'";
        $q.= " OR ARRAY[manager] && '{$param['ownergroups']}'";
        $q.= " OR ARRAY[manager] && '{$param['membergroups']}'";
        $q.= ");";
        $res = pg_query($cyf, $q);
        if ($res) {
            echo '{"r":"'.pg_affected_rows ($res).'","m":"User deleted!"}';
        }else
//            echo $q;
             echo '{"r":"0","m":"Delete error"}';
    }
    exit();
}
//insert User
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Nuser")){
     if(!empty($_REQUEST['email'])) {
         $i =array();
         $i['email'] = "'".pg_escape_string($_REQUEST['email'])."'";
         if(!empty($_REQUEST['cname'])) $i['cname'] = "'".pg_escape_string($_REQUEST['cname'])."'";
         if(!empty($_REQUEST['address'])) $i['address'] = "'".pg_escape_string($_REQUEST['address'])."'";
         if(!empty($_REQUEST['contact'])) $i['contact'] = "'".pg_escape_string($_REQUEST['contact'])."'";
         if(!empty($_REQUEST['tel']))     $i['tel'] = "'".pg_escape_string($_REQUEST['tel'])."'";
         if(!empty($_REQUEST['lang']))    $i['lang'] = "'".pg_escape_string($_REQUEST['lang'])."'";
         if(!empty($_REQUEST['manager'])) $i['manager'] = "'".pg_escape_string($_REQUEST['manager'])."'";
         if(!empty($_REQUEST['dstart']))  $i['dstart'] = "'".pg_escape_string($_REQUEST['dstart'])."'";
         if(!empty($_REQUEST['dend']))    $i['dend'] = "'".pg_escape_string($_REQUEST['dend'])."'";
         if(!empty($_REQUEST['status']))  $i['status'] = "'".pg_escape_string($_REQUEST['status'])."'";
         if(!empty($_REQUEST['membergroups']))
             $i['membergroups'] = "'{".pg_escape_string($_REQUEST['membergroups'])."}'";
         $n = array();
         $v =array();
         foreach($i as $k => $s){
             $n[] = $k;
             $v[] = $s; 
         }
         $q = "INSERT INTO users (".implode(',', $n).") VALUES (".implode(',', $v).");";
//echo $q."<br>";
         $res = pg_query($cyf, $q);
         if ($res) {
             echo '{"r":"'.pg_affected_rows ($res).'","m":"User created!"}';
         }else
//echo $q;
             echo '{"r":"0","m":"Dublicate User"}';
     }
    exit();
}
//delete contract
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Dcontract") AND !empty($_POST['eid'])){
    if($_POST['eid'] > 0){
        $q ="DELETE FROM contracts WHERE id=".pg_escape_string($_REQUEST['eid']);
        $q.=" AND (";        
        $q.=" manager='{$param['email']}'";        
        $q.=" OR ARRAY[manager] && '{$param['ownergroups']}'";        
//        $q.=" OR ARRAY[manager] && '{$param['membergroups']}'";        
        $q.=");";        
        $res = pg_query($cyf, $q);
        if ($res) {
            echo '{"r":"'.pg_affected_rows ($res).'","m":"Contract deleted!"}';
        }else
//            echo $q;
             echo '{"r":"0","m":"Delete error"}';
    }
    exit();
}
//insert contract
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Ncontract")){
    if(!empty($_POST['email']) AND !empty($_POST['cname']) AND !empty($_POST['manager'])){
        if(!empty($_REQUEST['manager'])) $i['manager'] = "'".pg_escape_string($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['cname'])) $i['cname']     = "'".pg_escape_string($_REQUEST['cname'])."'";
        if(!empty($_REQUEST['bname'])) $i['bname']     = "'".pg_escape_string($_REQUEST['bname'])."'";
        if(!empty($_REQUEST['bid'])) $i['bid']         = "'".pg_escape_string($_REQUEST['bid'])."'";
        if(!empty($_REQUEST['osec'])) $i['osec']       = "'".htmlspecialchars($_REQUEST['osec'])."'";
        if(!empty($_REQUEST['address'])) $i['address'] = "'".pg_escape_string($_REQUEST['address'])."'";
        if(!empty($_REQUEST['contact'])) $i['contact'] = "'".pg_escape_string($_REQUEST['contact'])."'";
        if(!empty($_REQUEST['tel'])) $i['tel']         = "'".pg_escape_string($_REQUEST['tel'])."'";
        if(!empty($_REQUEST['url'])) $i['url']         = "'".pg_escape_string($_REQUEST['url'])."'";
        if(!empty($_REQUEST['lang'])) $i['lang']       = "'".pg_escape_string($_REQUEST['lang'])."'";
        if(!empty($_REQUEST['status'])) $i['status']   = "'".pg_escape_string($_REQUEST['status'])."'";
        if(!empty($_REQUEST['dstart'])) $i['dstart']   = "'".pg_escape_string($_REQUEST['dstart'])."'";
        if(!empty($_REQUEST['dend'])) $i['dend']       = "'".pg_escape_string($_REQUEST['dend'])."'";
        if(!empty($_REQUEST['maxusers'])) $i['maxusers'] = "'".pg_escape_string($_REQUEST['maxusers'])."'";
        if(!empty($_REQUEST['contractgroups']))
            $i['contractgroups'] = "'".pg_escape_string($_REQUEST['contractgroups'])."'";
        if(!empty($_REQUEST['usersgroups']))
            $i['usersgroups'] = "'".pg_escape_string($_REQUEST['usersgroups'])."'";
        if(!empty($_REQUEST['apps']))
            $q.= ",apps='".pg_escape_string($_REQUEST['apps'])."'";
        if(!empty($_REQUEST['manager']))
            $i['manager'] = "'".pg_escape_string($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['email'])) $i['email'] = "'".pg_escape_string($_REQUEST['email'])."'";
         $n = array();
         $v =array();
         foreach($i as $k => $s){
             $n[] = $k;
             $v[] = $s; 
         }
         $q = "INSERT INTO contracts (".implode(',', $n).") VALUES (".implode(',', $v).");";
//echo $q."<br>";
         $res = pg_query($cyf, $q);
         if ($res) {
             echo "Contract created!";
         }else
//echo $q;
             echo "Dublicate Contract";
    }
    exit();
}
//change contract
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Ccontract")){
    if(!empty($_POST['email']) AND !empty($_POST['cname']) AND !empty($_POST['manager'])
    AND !empty($_POST['eid'])){
        if($_POST['eid'] > 0){
        $q = "UPDATE contracts SET ";
//        if(!empty($_REQUEST['cname'])) $q.= "cname='".pg_escape_string($_REQUEST['cname'])."'";
        if(!empty($_REQUEST['bname'])) $q.= " bname='".pg_escape_string($_REQUEST['bname'])."'";
        if(!empty($_REQUEST['bid'])) $q.= ",bid='".pg_escape_string($_REQUEST['bid'])."'";
        if(!empty($_REQUEST['osec'])) $q.= ",osec='".pg_escape_string($_REQUEST['osec'])."'";
        if(!empty($_REQUEST['address'])) $q.= ",address='".pg_escape_string($_REQUEST['address'])."'";
        if(!empty($_REQUEST['contact'])) $q.= ",contact='".pg_escape_string($_REQUEST['contact'])."'";
        if(!empty($_REQUEST['tel'])) $q.= ",tel='".pg_escape_string($_REQUEST['tel'])."'";
        if(!empty($_REQUEST['url'])) $q.= ",url='".pg_escape_string($_REQUEST['url'])."'";
        if(!empty($_REQUEST['lang'])) $q.= ",lang='".pg_escape_string($_REQUEST['lang'])."'";
        if(!empty($_REQUEST['status'])) $q.= ",status='".pg_escape_string($_REQUEST['status'])."'";
        if(!empty($_REQUEST['dstart'])) $q.= ",dstart='".pg_escape_string($_REQUEST['dstart'])."'";
        if(!empty($_REQUEST['dend'])) $q.= ",dend='".pg_escape_string($_REQUEST['dend'])."'";
        if(!empty($_REQUEST['maxusers'])) $q.= ",maxusers='".pg_escape_string($_REQUEST['maxusers'])."'";
        if(!empty($_REQUEST['contractgroups']))
            $q.= ",contractgroups='".pg_escape_string($_REQUEST['contractgroups'])."'";
        if(!empty($_REQUEST['usersgroups']))
            $q.= ",usersgroups='".pg_escape_string($_REQUEST['usersgroups'])."'";
        if(!empty($_REQUEST['apps']))
            $q.= ",apps='".pg_escape_string($_REQUEST['apps'])."'";
        if(!empty($_REQUEST['manager']))
            $q.= ",manager='".pg_escape_string($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['email'])) $q.= ",email='".pg_escape_string($_REQUEST['email'])."'";
        $q.= " WHERE id=".pg_escape_string($_REQUEST['eid']);
        $q.= " AND (";
        $q.= " ARRAY[manager] && '".$param['ownergroups']."'";
//        $q.= " OR ARRAY[manager] && '".$param['membergroups']."'";
        $q.= " OR manager='".$param['email']."'";
        $q.= ");";
        $res = pg_query($cyf, $q);
        if ($res) {
            echo "Contract changed!";
        }else
            echo $q;
//             echo "Change error";
        }
    }
    exit();
}
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Gownergroups")){
    if(!empty($_POST['email']) AND !empty($_POST['manager'])){
        $q = "SELECT id,gname,owner FROM groups";
        $q.= " WHERE owner='".pg_escape_string($_REQUEST['email'])."'";
        $q.= " OR ARRAY[owner] && '{".pg_escape_string($_REQUEST['manager'])."}'";
        $q.= " ORDER BY gname;";
        $res = pg_query($conn, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $d='';
            echo '[';
            while (($r=pg_fetch_assoc ( $res ))){
                echo $d;
                echo '{"id":"'.$r['id'].'","gn":"'.$r['gname'].'","own":"'.$r['owner'].'"}';
                $d=',';
            }
            echo ']';
            exit();
        }
    }
    echo '[{"id":"0"}]';
    exit();
}
//insert group
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Ngroup")){
    if(!empty($_POST['gname']) AND !empty($_POST['owner'])){
         $i =array();
         $i['gname'] = "'".pg_escape_string($_REQUEST['email'])."'";
         $i['owner'] = "'".pg_escape_string($_REQUEST['owner'])."'";
         $n = array();
         $v =array();
         foreach($i as $k => $s){
             $n[] = $k;
             $v[] = $s; 
         }
         $q = "INSERT INTO users (".implode(',', $n).") VALUES (".implode(',', $v).");";
//echo $q."<br>";
         $res = pg_query($cyf, $q);
         if ($res) {
             echo "Group created!";
         }else
//echo $q;
             echo "Dublicate Group";
    }
    exit();
}

//update Group
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Cgroup")){
    if(!empty($_POST['gname']) AND !empty($_POST['owner']) AND !empty($_POST['eid'])){
        $q = "UPDATE groups SET ";
        if($_REQUEST['eid'] > 0) {
            $q .=" gname='".pg_escape_string($_REQUEST['gname'])."'";
            $q .=",owner='".pg_escape_string($_REQUEST['owner'])."'";
            $q .=" WHERE id=".pg_escape_string($_REQUEST['eid'])." AND (";
            $q .=" ARRAY[owner] && '".$param['ownergroups']."'";
//           $q.= " OR ARRAY[owner] && '".$param['membergroups']."'";
            $q .=" OR owner='".$param['email']."');";
            $res = pg_query($conn, $q);
         if ($res) {
             echo "Group changed!";
         }else
echo $q;
//             echo "Change error";
        }
    }
    exit();
}
//delete Group
if(!empty($_POST['eaction']) AND ($_POST['eaction']=="Dgroup")){
    if(!empty($_POST['eid']) AND ($_POST['eid'] >0)){
        $q = "DELETE FROM groups";
        $q.=" WHERE id={$_REQUEST['eid']} AND (";
        $q.=" ARRAY[owner] && '".$param['ownergroups']."'";
//$q.= " OR ARRAY[owner] && '".$param['membergroups']."'";
        $q.=" OR owner='".$param['email']."');";
        $res = pg_query($conn, $q);
        if ($res) {
            echo "Group deleted!";
        }else
            echo $q;
//             echo "Change error";
    }
    exit();
}

$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Contracts';
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
var groupFid;
//email,contact,address,tel,lang,dstart,dend,cname,manager,status,membergroups
var user = {
  id : 0,
  email : "<?php echo $param['email']; ?>",
  cname : "",
  ownergroups : "",
  membergroups : "",
  lang : "",
  manager : "",
  start: "",
  dend : ""
};
//cname,bname,address,bid,osec,contact	lang,tel,email,url	dstart,dend,manager,status,maxusers,contractgroups,usersgroups,apps
var contract = {
      email : "",
      cname : "",
      manager : "",
      contractgroups : "",
      usersgroups : "",
      appsgroups : "",
      maxusers : 0,
      lang : "",
      start: "",
      dend : "",
      contact : "",
      address : "",
      tel : ""
    };
var param = {
  maxusers : 0,
  cusers : 0,
  ousers : 0,
  ausers : 0
};

function getParam(){
        var p= {eaction: 'Jparam'};
        $.ajax({url:"../cyf/cyf_contract.php",data: p,
            success: function(msg){
            if(msg.length>0){
//                console.log(msg);
                var pg =   JSON.parse(msg);
                param.maxusers=pg.musers;
                param.cusers=pg.cusers;
                param.ousers=pg.ousers;
                var o =pg.musers-pg.cusers-pg.ousers;
                param.ausers=o;
                
                $('#CmUsers').text(param.maxusers);
                $('#CcUsers').text(param.cusers);
                $('#CoUsers').text(param.ousers);
                $('#CaUsers').text(param.ausers);
                
                $('#UmUsers').text(param.maxusers);
                $('#UcUsers').text(param.cusers);
                $('#UoUsers').text(param.ousers);
                $('#UaUsers').text(param.ausers);

//                console.log("Available: "+o);
            }
            }
            });
};
//cname,bname,address,bid,osec,contact,lang,tel,email,url,dstart,dend,manager,status,maxusers,contractgroups,usersgroups,apps
function getContract(em){
    var p= {eaction: 'Gcontract', email: em};
    $.ajax({url:"../cyf/cyf_contract.php",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg[0].id!=0){
                    contract.email=pg[0].email;
                    contract.cname=pg[0].cname;
                    contract.manager=pg[0].manager;
                    contract.contractgroups=pg[0].contractgroups;
                    contract.usersgroups=pg[0].usersgroups;
                    contract.appsgroups=pg[0].apps;
                    contract.maxusers=pg[0].maxusers;
                    contract.lang=pg[0].lang;
                    contract.dstart=pg[0].dstart;
                    contract.dend=pg[0].dend;
                    contract.contact=pg[0].contact;
                    contract.tel=pg[0].tel;
                    contract.address=pg[0].address;
                    
                    $('#mmanager').text(contract.manager);
                    $('#mcname').text(contract.cname);
                    $('#memail').text(contract.email);
                    $('#mcontractgroups').text(contract.contractgroups);
                    $('#musersgroups').text(contract.usersgroups);
                    $('#mmaxusers').text(contract.maxusers);
                    
                    getOwnergroups(contract.email,"{"+contract.contractgroups[0]+"}");
                    getUser(contract.email);
                    getParam();
                }
            }
        }
    });
};
function getUser(em){
    var p= {eaction: 'Guser', email: em};
    $.ajax({url:"../cyf/cyf_user.php",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg.id!=0){
                    user.id = pg.id;
                    user.email = pg.email;
                    user.cname = pg.cname;
                    user.ownergroups = pg.ownergroups;
                    user.membergroups = pg.membergroups;
                    user.lang = pg.lang;
                    user.manager = pg.manager;
                    user.start = contract.start;
                    user.dend = contract.start;
                    $('#muemail').text(user.email);
                    $('#mogroups').text(user.ownergroups);
                    $('#mmgroups').text(user.membergroups);
                }
            }
        }});
	var p= {eaction: 'Vgroup'};
	$("#groups").load("../cyf/cyf_group.php", p)
};
function getOwnergroups(em,cm){
    var p= {eaction: 'Gownergroups', email: em, manager: cm};
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
//    console.log(msg);
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                var Cman = '';
                var CList = '';
                var pg =   JSON.parse(msg);
                for(i=0;i<pg.length;i++){
                   Cman += "<option value='"+pg[i].gn+"'>"+pg[i].gn+"</option>";
                   CList+="<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'";
                   CList+=" data-user-id='"+pg[i].id+"' data-user-gname='"+pg[i].gn+"'";
                   CList+=" data-user-owner='"+pg[i].own+"'>";
                   CList+="<p><b style='font-size:130%;'>"+pg[i].gn+" </b> (<i>"+pg[i].own+"</i>)</p>";
                   CList+="</a></li>";
                }
                Cman += "<option value='"+em+"'>"+em+"</option>";
                CList+="<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'";
                CList+=" data-user-id='"+0+"' data-user-gname='"+em+"'";
                CList+=" data-user-owner='"+em+"'>";
                CList+="<p><b style='font-size:130%;'>"+em+" </b> (<i>"+em+"</i>)</p>";
                CList+="</a></li>";
                //$('#cManager').empty().append(Cman);
                //$('#cContractgroups').empty().append(Cman);
                //$('#cUsersgroups').empty().append(Cman);
                //$('#Umanager').empty().append(Cman);
                //$('#Umembergroups').empty().append(Cman);
                //$('#Uownergroups').empty().append(Cman);
                //$('#Gowner').empty().append(Cman);
                $('#groups').empty().append(CList);
            }
        }
    });
};
function toGroups(f){
        var a = f.split(",");
        var ao ='{';
        var d = "";
        for (i = 0; i < a.length; i++) { 
            if(a[i] !=''){
                ao += d + '"'+a[i] + '"';
                d=',';
            }
        }
        ao += '}';
        return ao;
};

$( document ).on( "vclick", "a#vcontract", function() {
	var p= {eaction: 'Vcontract'};
	$("#contracts").load("../cyf/cyf_contract.php", p)
    return true;
});
$( document ).on( "vclick", "a#dcontract", function() {
	var p= {eaction: 'Dcontract',eid: $('#Cnid').val()};
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
            if(msg.length>0){
                 var pg =   JSON.parse(msg);
                $('#Cstate').text(pg.m);
                getParam();
//                if(pg.r>0)
//                  param.ausers=param.ausers+contract.maxusers;
            }
        }
    });
//	$("#contracts").load("../cyf/cyf_contract.php", p)
    return false;
});
//cname,bname,address,bid,osec,contact,lang,tel,email,url,dstart,dend,manager,status,maxusers,contractgroups,usersgroups,apps
$( document ).on( "vclick", "a#ncontract", function() {
    console.log("users:"+(parseInt(param.ausers)+parseInt($('#cMaxusers').val())));
    $('#Cstate').text("");
    if(parseInt(param.ausers) < parseInt($('#cMaxusers').val())){
        $('#Cstate').text("Too many users ...");
        return false;
    }
	var p= {eaction: 'Ncontract',
        cname: $('#cCname').val(),
        bname: $('#cBname').val(),
        bid: $('#cBid').val(),
        osec: $('#cOsec').val(),
        address: $('#cAddress').val(),
        contact: $('#cContact').val(),
        tel: $('#cTel').val(),
        url: $('#cUrl').val(),
        lang: $('#cLang').val(),
        manager: $('#cManager').val(),
        status: $('#cStatus').val(),
        dstart: $('#cDstart').val(),
        dend: $('#cDend').val(),
        email: $('#cEmail').val(),
        maxusers: $('#cMaxusers').val(),
        contractgroups: toGroups($('#cContractgroups').val()),
        usersgroups: toGroups($('#cUsersgroups').val()),
        apps: toGroups($('#cApps').val()),
        pass: $('cPassword').val()
    };
//    console.log(p);
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
            if(msg.length>0){
                $('#Cstate').text(msg);
                getParam();
            }
        }
    });
//	$("#contracts").load("../cyf/cyf_contract.php", p)
    return false;
});
$( document ).on( "vclick", "a#ccontract", function() {
    console.log("users:"+(parseInt(param.ausers)+parseInt($('#cMaxusers').val())));
    $('#Cstate').text("");
    if(parseInt(param.ausers) < parseInt($('#cMaxusers').val())){
        $('#Cstate').text("Too many users ...");
        return false;
    }
	var p= {
        eaction: 'Ccontract',
        eid: $('#Cnid').val(),
        cname: $('#cCname').val(),
        bname: $('#cBname').val(),
        bid: $('#cBid').val(),
        osec: $('#cOsec').val(),
        address: $('#cAddress').val(),
        contact: $('#cContact').val(),
        tel: $('#cTel').val(),
        url: $('#cUrl').val(),
        lang: $('#cLang').val(),
        manager: $('#cManager').val(),
        status: $('#cStatus').val(),
        dstart: $('#cDstart').val(),
        dend: $('#cDend').val(),
        email: $('#cEmail').val(),
        maxusers: $('#cMaxusers').val(),
        contractgroups: toGroups($('#cContractgroups').val()),
        usersgroups: toGroups($('#cUsersgroups').val()),
        apps: toGroups($('#cApps').val()),
        pass: $('cPassword').val()
    };
//    console.log(p);
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
            if(msg.length>0){
                $('#Cstate').text(msg);
                getParam();
            }
        }
    });
//    console.log(p);
//	$("#contracts").load("../cyf/cyf_contract.php", p)
    return false;
});
$( document ).on( 'vclick', "a#crcontract", function() {
    getParam();
    $('#Cnid').val(0);
    $('#cCname').val("New subcontract");
    $('#contracttitle').text($('#cCname').val());
    $('#cBname').val("");
    $('#cBid').val("");
    $('#cOsec').val("");
    $('#cAddress').val("");
    $('#cContact').val("");
    $('#cTel').val("");
    $('#cUrl').val("");
    $('#cLang').val("");
	$("#cManager").val("");
    $('#cStatus').val("");
    $('#cDstart').val("");
    $('#cDend').val("");
    $('#cEmail').val("");
    $('#cMaxusers').val(param.ausers);
    $('#cContractgroups').val("");
    $('#cUsersgroups').val("");
    $('#cApps').val("");
});
$( document ).on( 'vclick', "a#Caid", function() {
    $('#Cnid').val($(this).data('userId'));
    $('#contracttitle').text($(this).data('userCname'));
    getParam();
       
        var p= {eaction: 'Jcontract', eid: $('#Cnid').val()};
        $.ajax({url:"../cyf/cyf_contract.php",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg.id!=0){
                    $('#cCname').val(pg.cname);
                    $('#cBname').val(pg.bname);
                    $('#cBid').val(pg.bid);
                    $('#cOsec').val(pg.osec);
                    $('#cAddress').val(pg.address);
                    $('#cContact').val(pg.contact);
                    $('#cTel').val(pg.tel);
                    $('#cUrl').val(pg.url);
                    $('#cLang').val(pg.lang);
                    $('#cManager').val(pg.manager);
                    $('#cStatus').val(pg.status);
                    $('#cDstart').val(pg.dstart);
                    $('#cDend').val(pg.dend);
                    $('#cEmail').val(pg.email);
                    $('#cMaxusers').val(pg.maxusers);
                    $('#cContractgroups').val(pg.contractgroups);
                    $('#cUsersgroups').val(pg.usersgroups);
                    $('#cApps').val(pg.apps);
                }
            }
            }
            });
    return true;
});

//duser
$( document ).on( "vclick", "a#duser", function() {
	var p= {eaction: 'Duser',eid: $('#Unid').val()};
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#Ustate').text(pg.m);
                getParam();
//                param.ausers=param.ausers-pg.r;
//                console.log("Available: "+o);
            }
        }
    });
//	$("#users").load("../cyf/cyf_user.php", p)
    return false;
});

//nuser email,contact,address,tel,lang,dstart,dend,cname,manager,status,membergroups   
$( document ).on( "vclick", "a#nuser", function() {
    $('#Ustate').text("");
    if(param.maxusers - param.ausers <=0){
        $('#Ustate').text("Too many users ...");
        return false;
    }
	var p= {
      eaction: 'Nuser',
        cname: $('#Ucname').val(),
        address: $('#Uaddress').val(),
        contact: $('#Ucontact').val(),
        tel: $('#Utel').val(),
        lang: $('#Ulang').val(),
        manager: $('#Umanager').val(),
        status: $('#Ustatus').val(),
        dstart: $('#Udstart').val(),
        dend: $('#Udend').val(),
        email: $('#Uemail').val(),
        pass: $('#Upassword').val(),
      ownergroups: $('#cUsersgroups').val(),//toGroups($('#Uownergroups').val()),
      membergroups: $('#cContractgroups').val(),//toGroups($('#Umembergroups').val()),
      apps: ""//toGroups($('#Uapps').val())
    };
//    console.log(p);
    $.ajax({url:"../cyf/cyf_manager.php",type:"post",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#Ustate').text(pg.m);
                getParam();
//                param.ausers=param.ausers-pg.r;
//                console.log("Available: "+o);
            }
        }
    });
//	$("#users").load("../cyf/cyf_user.php", p)
    return false;
});
//cuser email,contact,address,tel,lang,dstart,dend,cname,manager,status,membergroups   
$( document ).on( "vclick", "a#cuser", function() {
	var p= {eaction: 'Cuser',
        eid: $('#Unid').val(),
        cname: $('#Ucname').val(),
        address: $('#Uaddress').val(),
        contact: $('#Ucontact').val(),
        tel: $('#Utel').val(),
        lang: $('#Ulang').val(),
        manager: $('#Umanager').val(),
        status: $('#Ustatus').val(),
        dstart: $('#Udstart').val(),
        dend: $('#Udend').val(),
        email: $('#Uemail').val(),
        pass: $('#Upassword').val(),
        ownergroups: toGroups($('#Uownergroups').val()),
        membergroups: toGroups($('#Umembergroups').val()),
        apps: toGroups($('#Uapps').val())
    };
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( "vclick", "a#vuser", function() {
	var p= {eaction: 'Vuser'};
	$("#users").load("../cyf/cyf_user.php", p)
    return true;
});
$( document ).on( 'vclick', "a#Uaid", function() {
    $('#Unid').val($(this).data('userId'));
    $('#usertitle').text($(this).data('userEmail'));
    getParam();
       
        var p= {eaction: 'Juser', eid: $('#Unid').val()};
        $.ajax({url:"../cyf/cyf_user.php",data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg.id!=0){
                    $('#Ucname').val(pg.cname);
                    $('#Uaddress').val(pg.address);
                    $('#Ucontact').val(pg.contact);
                    $('#Utel').val(pg.tel);
                    $('#Ulang').val(pg.lang);
                    $('#Umanager').val(pg.manager);
                    $('#Ustatus').val(pg.status);
                    $('#Uemail').val(pg.email);
                    $('#Umembergroups').val(pg.membergroups);
                    $('#Udstart').val(pg.dstart);
                    $('#Udend').val(pg.dend);
                }
            }
            }
            });
    return true;
});
$( document ).on( 'vclick', "a#cruser", function() {
    getParam();
    $('#Unid').val(0);
    $('#usertitle').text("New user");
    $('#Uemail').val("");
    $('#Upassword').val("");
    $('#Ucname').val(contract.cname);
    $('#Uaddress').val("");
    $('#Ucontact').val("");
    $('#Utel').val("");
    $('#Ulang').val(contract.lang);
    $('#Umanager').val(contract.email);
    $('#Ustatus').val("active");
    $('#Uownergroups').val("");
    $('#Umembergroups').val(contract.membergroups);
    $('#Udstart').val(contract.dstart);
    $('#Udend').val(contract.dend);
    $('#Ustate').text("");
    return true;
});

$( document ).on( 'vclick', "a#ccontractman", function() {
    getParam();
    $('#Unid').val(0);
    $('#usertitle').text("New user");
    $('#Uemail').val($("#cEmail").val());
    $('#Upassword').val($("#cPassword").val());
    $('#Ucname').val($("#cCname").val());
    $('#Uaddress').val($("#cAddress").val());
    $('#Ucontact').val($("#cContact").val());
    $('#Utel').val($("#cTel").val());
    $('#Ulang').val($("#cLang").val());
    $('#Umanager').val($("#cContractgroups").val());
    $('#Ustatus').val("active");
    $('#Uownergroups').val($("#cUsersgroups").val());
    $('#Umembergroups').val($("#cContractgroups").val());
    $('#Udstart').val($("#cDstart").val());
    $('#Udend').val($("#cDend").val());
    $('#Ustate').text("");
    return true;
});

$( document ).on( 'vclick', "a#cuserlog", function() {
    $('#Lemail').val($("#Uemail").val());
    $('#Lpassword').val($("#Upassword").val());
    $('#LUstate').text("");
    return true;
});
$( document ).on( 'vclick', "a#nlogin", function() {
    $('#Lstate').text("");
    if(param.maxusers < param.ausers){
        $('#Lstate').text("Too many users ...");
        return false;
    }
	var p= {
        act: 'Nlogin',
        nm : $('#Ucontact').val(),
        em : $('#Lemail').val(),
        ps : $('#Lpassword').val(),
        mgr: $('#cContractgroups').val(),
        ogr: $('#cUsersgroups').val()
    };
//    console.log(p);
    $.ajax({url:"../cyf/cyf_manager.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                   $('#Lstate').text(msg);
            }
        }});
});
$( document ).on( "vclick", "a#setgroup", function() {
    $(groupFid).val($("#Ggname").val());
    return true;
});
$( document ).on( "vclick", "a#addselgroup", function() {
    $(groupFid).val($(groupFid).val()+','+$("#Ggname").val());
    return true;
});
$( document ).on( "vclick", "a#selman", function() {
    groupFid = "#cManager";
    return true;
});
$( document ).on( "vclick", "a#selcgr", function() {
    groupFid = "#cContractgroups";
    getOwnergroups(contract.email,contract.contractgroups[0]);
    return true;
});
$( document ).on( "vclick", "a#selugr", function() {
    groupFid = "#cUsersgroups";
    getOwnergroups(contract.email,contract.contractgroups[0]);
//	var p= {eaction: 'Vgroup'};
//	$("#groups").load("../cyf/cyf_group.php", p);
    return true;
});
$( document ).on( "vclick", "a#selagr", function() {
    groupFid = "#cApps";
    return true;
});
$( document ).on( "vclick", "a#selogr", function() {
    groupFid = "#Uownergroups";
    return true;
});
$( document ).on( "vclick", "a#selmgr", function() {
    groupFid = "#Umembergroups";
    return true;
});
$( document ).on( "vclick", "a#selown", function() {
    groupFid = "#Gowner";
//    getOwnergroups(contract.email,contract.contractgroups[0]);
    return true;
});


$( document ).on( "vclick", "a#vgroup", function() {
	var p= {eaction: 'Vgroup'};
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( "vclick", "a#dgroup", function() {
	var p= {
        eaction: 'Dgroup',
        eid:     $('#Gnid').val()
    };
    $.ajax({url:"../cyf/cyf_manager.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                   $('#Gstate').text(msg);
            }
        }});
    return false;
});
$( document ).on( "vclick", "a#cgroup", function() {
	var p= {
        eaction: 'Cgroup',
        eid:     $('#Gnid').val(),
        gname:  $('#Ggname').val(),
        owner:  $('#Gowner').val()
    };
    $.ajax({url:"../cyf/cyf_manager.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                   $('#Gstate').text(msg);
            }
        }});
    return false;
});
$( document ).on( "vclick", "a#ngroup", function() {
	var p= {
        eaction: 'Ngroup',
        owner:  $('#Gowner').val(),
        gname:   $('#Ggname').val()
    };
    $.ajax({url:"../cyf/cyf_manager.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                   $('#Gstate').text(msg);
            }
        }});
    return false;
});
$( document ).on( 'vclick', "a#Gaid", function() {
    $('#Gnid').val($(this).data('userId'));
    $('#Ggname').val($(this).data('userGname'));
    $("#Gowner").val($(this).data('userOwner'));
    return true;
});

$(document).on( "pagebeforeshow", "#newlogin", function( event ) {
    $('#Lstate').text("");
});
$(document).on( "pagebeforeshow", "#newuser", function( event ) {
    $('#Ustate').text("");
});
$(document).on( "pagebeforeshow", "#listuser", function( event ) {
	var p= {eaction: 'Vuser'};
	$("#users").load("../cyf/cyf_user.php", p);
});

$(document).on( "pagebeforeshow", "#newgroup", function( event ) {
    $('#Gstate').text("");
});
$(document).on( "pagebeforeshow", "#listgroup", function( event ) {
    getOwnergroups(contract.email,"{"+contract.contractgroups[0]+"}");
//	var p= {eaction: 'Vgroup'};
//	$("#groups").load("../cyf/cyf_group.php", p);
//    $('#listgroup').find( ":jqmData(role=listview)" ).listview().listview("refresh");
});

$(document).on( "pagebeforeshow", "#newcontract", function( event ) {
    $('#Cstate').text("");
});
$(document).on( "pagebeforeshow", "#listcontract", function( event ) {
	var p= {eaction: 'Vcontract'};
	$("#contracts").load("../cyf/cyf_contract.php", p);
});

$(document).ready(function (){
    getContract(user.email);
//    $.mobile.changePage('#listcontract');
});

</script>
</head>
<body>
<!-- cyf_contracts.php -->	
<input type='hidden' name='Unid' id='Unid'>
<input type='hidden' name='Cnid' id='Cnid'>
<input type='hidden' name='Gnid' id='Gnid'>
<!-- page -->   
<div data-role="page" id="blankcontract" data-position="fixed">
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#' class='mybtn ui-btn ui-icon-home ui-btn-icon-right ui-btn-icon-notext'data-rel="back" ></a>
  <div data-role="navbar">
	<ul>
	  <li></li>
      <li></li>
    </ul>
  </div>
</div>
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview">
<li>
<a href="#popupContract" data-rel="popup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-info ui-btn-icon-right" data-transition="pop">Contract info</a>
<div data-role="popup" id="popupContract">
<p>name: <b><span id="mcname"></span></b></p>
<p>manager: <b><span id="mmanager"></span></b></p>
<p>groups: <b><span id="mcontractgroups"></span></b></p>
<p>user groups: <b><span id="musersgroups"></span></b></p>
<p>max users: <b><span id="mmaxusers"></span></b></p>
<p>email: <b><span id="memail"></span></b></p>
</div>
</li>
<li>  
<a href="#popupOwner" data-rel="popup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-info ui-btn-icon-right" data-transition="pop">Owner info</a>
<div data-role="popup" id="popupOwner">
<p>email:         <b><span id="muemail"></span></b></p>
<p>member groups: <b><span id="mmgroups"></span></b></p>
<p>owner groups:  <b><span id="mogroups"></span></b></p>
</div>
</li>
<li>
<a href='#listcontract' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="glistcont">
    Sub Contract list</a>   
</li>
<li>
 <a href='#listuser' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="glistuser">
    Contract User list</a>   
</li>
<li>
 <a href='#listgroup' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="glistgroup">
    Contract Group list</a>   
</li>
</ul>
    
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
</div>
</div>
    
<!-- page -->   
<div data-role="page" id="listcontract" data-position="fixed">
<div data-role="header" data-position="fixed">
<a href="#blankcontract" class="ui-btn ui-btn-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext">Go Back</a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#' class='mybtn ui-btn ui-icon-back ui-btn-icon-right ui-btn-icon-notext' data-rel="back"></a>
  <div data-role="navbar">
	<ul>
	  <li></li>
      <li></li>
    </ul>
  </div>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" class="ui-listview" id="contracts" data-filter="true" data-filter-placeholder="Search ..." data-inset="true">
<!-- <li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li> -->
<li><a href='#newcontract' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Caid' data-user-id='0' data-user-cname="" data-user-manager='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listcontract" class="ui-btn ui-btn-corner-all ui-shadow" id="vcontract">Refresh contracts</a>
<a href='#newcontract' data-role="button" data-icon="plus" class="ui-bar" id="crcontract">Create contract</a>
</div>
</div>

<!-- page -->   
<div data-role="page" data-dialog="true" data-mini="true" id="newcontract"> 
<div data-role="header" data-position="fixed">
    <h2 id="contracttitle">Contract</h2>
</div>

<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
      <p>Users limit on a main contract: <span id="CmUsers"></span></p>
      <p>Now users (own/subcontract users): <span id="CoUsers"></span>/<span id="CcUsers"></span></p>
      <p>Available users on a main contract: <span id="CaUsers"></span></p>
    </li>
     <li>
    <div>
  <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selman">
    <center>Select Contract Manager</center></a>
		<input type="text" name="cManager" id="cManager">
    </div>
     </li>
    <li class="ui-btn ui-btn-corner-all ui-mini">
		<label for="cCname">Contract name: *</label>
		<input type="text" name="cCname" id="cCname" placeholder="name ...">
     </li>
    <li data-role="collapsible" data-iconpos="right" data-inset="false" class="ui-btn ui-btn-corner-all ui-mini">
     <h2>Description *</h2>
     <ul data-role="listview" data-theme="a">
      <li>
		<label for="cBname">Business name:</label>
		<input type="text" name="cBname" id="cBname" placeholder="cBname ...">
		<label for="cBid">Business id:</label>
		<input type="text" name="cBid" id="cBid" placeholder="cBid ...">
		<label for="cOsec">Osec:</label>
		<input type="text" name="cOsec" id="cOsec" placeholder="cOsec ...">
		<label for="cAddress">Address:</label>
		<input type="text" name="cAddress" id="cAddress" placeholder="cAddress ...">
		<label for="cContact">Contact: *</label>
		<input type="text" name="cContact" id="cContact" placeholder="cContact ...">
		<label for="cTel">tel: *</label>
		<input type="text" name="cTel" id="cTel" placeholder="tel ...">
		<label for="cUrl">url:</label>
		<input type="text" name="cUrl" id="cUrl" placeholder="url ...">
		<label for="cLang">lang:</label>
		<input type="text" name="cLang" id="cLang" placeholder="lang ...">
		<label for="cDstart">start:</label>
		<input type="date" name="cDstart" id="cDstart">
		<label for="cDend">end:</label>
		<input type="date" name="cDend" id="cDend">
      </li>
     </ul>
     </li>
     <li data-role="collapsible" data-iconpos="right" data-inset="false" class="ui-btn ui-btn-corner-all ui-mini">
     <h2>Status *</h2>
     <ul data-role="listview" data-theme="a">
      <li>
        <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selcgr"><center>Select contract group</center></a>
		<input type="text" name="cContractgroups" id="cContractgroups" placeholder="{group,group, ... ,group}">

        <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selugr"><center>Select user groups</center></a>
		<input type="text" name="cUsersgroups" id="cUsersgroups" placeholder="{group,group, ... ,group}">
    
<!--
        <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selagr"><center>Select app groups</center></a>
-->
		<input type="hidden" name="cApps" id="cApps"><!-- placeholder="{group,group, ... ,group}"> -->
    
    <div class="ui-field-contain">
	 <label for="cMaxusers">Max users:</label>
	 <input type="text" name="cMaxusers" id="cMaxusers" placeholder="Max users">
     <label for="cStatus">Status:</label>
     <input disabled="disabled" type="text" name="cStatus" id="cStatus" placeholder="active" value="active">
     <label for="cEmail">email: *</label>
     <input type="text" name="cEmail" id="cEmail" placeholder="email ...">
     <label for="cPassword">password:</label>
     <input type="password" name="cPassword" id="cPassword">
    </div>
     <a href="#newuser" class="ui-btn ui-btn-corner-all ui-shadow ui-mini" id="ccontractman"><center>Create contract owner</center></a>
      </li>

     </ul>
     </li>
    </li>

</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
    <div id="Cstate"></div>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" id="dcontract"><center>Delete contract</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" id="ccontract"><center>Change contract</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" id="ncontract"><center>Create contract</center></a>
</div>
</div>
    
<!-- page -->   
<div data-role="page" id="listuser" data-position="fixed">
<div data-role="header" data-position="fixed">
<a href="#blankcontract" class="ui-btn ui-btn-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext">Go Back</a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#' class='mybtn ui-btn ui-icon-back ui-btn-icon-right ui-btn-icon-notext' data-rel="back"></a>
  <div data-role="navbar">
	<ul>
	  <li></li>
      <li></li>
    </ul>
  </div>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" class="ui-listview" id="users" data-filter="true" data-filter-placeholder="Search ..." data-inset="true">
<!-- <li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li> -->
<li><a href='#newcontract' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Uaid' data-user-id='0' data-user-cname="" data-user-manager='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listuser" class="ui-btn ui-btn-corner-all ui-shadow" id="vuser">Refresh user list</a>
<a href='#newuser' data-role="button" data-icon="plus" class="ui-bar" id="cruser">Create user</a>
</div>
</div>

<!-- page -->   
<div data-role="page" data-dialog="true" data-mini="true" id="newuser"> 
<div data-role="header" data-position="fixed">
    <h2 id="usertitle">User</h2>
</div>

<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
      <p>Users limit on a main contract: <span id="UmUsers"></span></p>
      <p>Now users (own/subcontract users): <span id="UoUsers"></span>/<span id="UcUsers"></span></p>
      <p>Available users on a main contract: <span id="UaUsers"></span></p>
    </li>
     <li class="ui-btn ui-btn-corner-all ui-mini">
		<label for="Ucname">Contract name: </label>
		<input style="color:blue;" disabled="disabled" type="text" name="Ucname" id="Ucname" placeholder="name ...">
		<label for="Umanager">User manager: </label>
		<input style="color:blue;" disabled="disabled" type="text" name="Umanager" id="Umanager">
     </li>

   <li data-role="collapsible" data-iconpos="right" data-inset="false" class="ui-btn ui-btn-corner-all ui-mini">
     <h2>Description *</h2>
     <ul data-role="listview" data-theme="a">
      <li>
		<label for="Uaddress">Address:</label>
		<input type="text" name="Uaddress" id="Uaddress" placeholder="cAddress ...">
		<label for="Ucontact">Contact: *</label>
		<input type="text" name="Ucontact" id="Ucontact" placeholder="cContact ...">
		<label for="Utel">tel: *</label>
		<input type="text" name="Utel" id="Utel" placeholder="tel ...">
		<label for="Ulang">lang:</label>
		<input type="text" name="Ulang" id="Ulang" placeholder="lang ...">
		<label for="Udstart">start:</label>
		<input type="date" name="Udstart" id="Udstart">
		<label for="Udend">end:</label>
		<input type="date" name="Udend" id="Udend">
      </li>
     </ul>
     </li>
   <li data-role="collapsible" data-iconpos="right" data-inset="false" class="ui-btn ui-btn-corner-all ui-mini">
     <h2>Status *</h2>
     <ul data-role="listview" data-theme="a">
      <li>
    <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selmgr"><center>Select member groups</center></a>
		<input type="text" name="Umembergroups" id="Umembergroups" placeholder="{group,group, ... ,group}">
        <a href="#listgroup" class="ui-btn ui-btn-corner-all  ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selogr"><center>Select owner groups</center></a>
		<input type="text" name="Uownergroups" id="Uownergroups" placeholder="{group,group, ... ,group}">
		<input type="hidden" name="Uapps" id="Uapps">
    
    <div class="ui-field-contain">
     <label for="Uemail">email: *</label>
     <input type="text" name="Uemail" id="Uemail" placeholder="email ...">
     <label for="Upassword">password:</label>
     <input type="password" name="Upassword" id="Upassword">
     <label for="Ustatus">status:</label>
     <input type="text" name="Ustatus" id="Ustatus" placeholder="active" value="active">
    </div>
    </li>
    <li>
     <a href="#newlogin" class="ui-btn ui-btn-corner-all ui-mini ui-shadow" id="cuserlog"><center>Create user login</center></a>
    </li>

     </ul>
     </li>

</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
        <div id="Ustate"></div>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="duser">Delete user</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cuser">Change user</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nuser">Create user</a>
</div>
</div>


<!-- page -->   
<div data-role="page" data-dialog="true" data-mini="true" id="newlogin"> 
<div data-role="header" data-position="fixed">
    <h2>Create new login</h2>
</div>

<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
     <label for="Lemail">email: *</label>
     <input type="text" name="Lemail" id="Lemail" placeholder="email ...">
     <label for="Lpassword">password:</label>
     <input type="password" name="Lpassword" id="Lpassword">
    </li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
    <div id="Lstate"></div>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" id="nlogin"><center>Create login</center></a>
</div>
</div>


    
<!-- page -->
<div data-role="page" data-position="fixed" data-dialog="true" data-mini="true" id="listgroup">
<div data-role="header" data-position="fixed">
<h6>Groups</h6>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" data-filter="true" data-filter-placeholder="Search ..." data-inset="true" class="ui-listview" id="groups">
<!-- <li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li> -->
<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid' data-user-id='0' data-user-gname=' ' data-user-owner='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#newgroup' data-role="button" data-icon="plus" class="ui-bar">Create/Edit Group</a>
</div>
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="newgroup"> <!-- page -->
<div data-role="header" data-position="fixed">
    <h2 id="grouptitle">Group</h2>
</div>
<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
    <div class="ui-field-contain">
  <a href="#listgroup" class="ui-btn ui-btn-corner-all ui-mini ui-shadow ui-icon-arrow-d ui-btn-icon-right" id="selown">
    <center>Select Owner</center></a>
	<input type="text" name="Gowner" id="Gowner">
    </div> 
    </li>
    <li>
		<label for="Ggname">Group:</label>
		<input type="text" name="Ggname" id="Ggname" placeholder="Group name ...">
    </li>
	<li> 
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-check" data-rel="back" id="setgroup">Select Group</a>
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-plus" data-rel="back" id="addselgroup">Add Group to Selected</a>
	</li>
 </ul>
</div>
<div data-role="footer" class="ui-bar" style="text-align:center;">
        <div id="Gstate"></div>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" data-role="button" id="dgroup"><center>Delete Group</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" data-role="button" id="cgroup"><center>Change Group</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" data-role="button" id="ngroup"><center>Create Group</center></a>
</div>
</div>

</body>
</html>
