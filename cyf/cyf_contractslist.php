<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cyf/cyf_contractslist.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CIS Contract List</title>
<style>
tr:nth-child(even) {background-color: #f2f2f2}
tr:hover {background-color: #ddd;}
td {
padding-left:6px;
text-align:left;
}
</style>
<script>
EOT;

echo "function setParam(e){";
if(!empty($_REQUEST['frame'])){
   echo "document.getElementById('gid').value=e.dataset.id;";
   echo "document.getElementById('fmanager').value='{$_REQUEST['fmanager']}';";
   echo "document.getElementById('{$_REQUEST['frame']}').submit();";
}
echo "}";
echo "</script></head><body ><center>";

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf=cyfConnect();
//echo "post: ";print_r($_POST);echo "<br>";

$fmanager = $_REQUEST['fmanager'];

    $q = <<<EOT
SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager,
to_char(dstart,'YYYY-MM-DD') as dstart,
to_char(dend,'YYYY-MM-DD') as dend,
status,maxusers,contractgroups,usersgroups,apps FROM contracts WHERE manager='{$fmanager}'
EOT;

//if($_POST['usefilters']=='usefilters'){
    $qs='';
    $filters = array();
//    if(!empty($_REQUEST['fmanager']))
//       $filters[] = "manager LIKE '%" . pg_escape_string($_REQUEST['fmanager']) ."%'";
    if(!empty($_REQUEST['fcname']))
       $filters[] = "cname LIKE '%" . pg_escape_string($_REQUEST['fcname']) ."%'";
    if(!empty($_REQUEST['fbname']))
       $filters[] = "bname LIKE '%" . pg_escape_string($_REQUEST['fbname']) ."%'";
    if(!empty($_REQUEST['faddress']))
       $filters[] = "address LIKE '%" . pg_escape_string($_REQUEST['faddress']) ."%'";
    if(!empty($_REQUEST['fbid']))
       $filters[] = "bid LIKE '%" . pg_escape_string($_REQUEST['fbid']) ."%'";
    if(!empty($_REQUEST['fosec']))
       $filters[] = "osec LIKE '%" . pg_escape_string($_REQUEST['fosec']) ."%'";
    if(!empty($_REQUEST['fcontact']))
       $filters[] = "contact LIKE '%" . pg_escape_string($_REQUEST['fcontact']) ."%'";
    if(!empty($_REQUEST['flang']))
       $filters[] = "lang LIKE '%" . pg_escape_string($_REQUEST['flang']) ."%'";
    if(!empty($_REQUEST['ftel']))
       $filters[] = "tel LIKE '%" . pg_escape_string($_REQUEST['ftel']) ."%'";
    if(!empty($_REQUEST['femail']))
       $filters[] = "email LIKE '%" . pg_escape_string($_REQUEST['femail']) ."%'";
    if(!empty($_REQUEST['furl']))
       $filters[] = "url LIKE '%" . pg_escape_string($_REQUEST['furl']) ."%'";
    if(!empty($_REQUEST['fstart']))
       $filters[] = "dstart <= '%" . pg_escape_string($_REQUEST['fdstart']) ."%'";
    if(!empty($_REQUEST['fend']))
       $filters[] = "dend <= '%" . pg_escape_string($_REQUEST['fdend']) ."%'";
    if(!empty($_REQUEST['fstatus']))
       $filters[] = "status = '" . pg_escape_string($_REQUEST['fstatus']) ."'";
    if(!empty($_POST['fcontractgroups']))
        $filters[] =  "'{".pg_escape_string($_POST['fcontractgroups'])."}' && contractgroups";
    if(!empty($_POST['fusersgroups']))
        $filters[] =  "'{".pg_escape_string($_POST['fusersgroups'])."}' && usersgroups";
    if(!empty($_POST['fapps']))
        $filters[] =  "'{".pg_escape_string($_POST['fapps'])."}' && apps";

    $qs = implode(' AND ',$filters);
    if($qs != '') $q .= ' AND ' . $qs;
//}
$q .= ' ORDER BY bname;';
//echo $q; echo '<br>';
$result = @pg_query($cyf, $q);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    if($firows >0){
        $fi=getValues($result);
        echo "<table>";
        for ($j=0;$j<$firows;$j++){
            echo "<tr onclick='setParam(this)' data-id='{$fi[$j]['id']}'><td>{$fi[$j]['bname']}</td>";
            echo "<td>{$fi[$j]['contact']}</td><td>{$fi[$j]['email']}</td></tr>";
        }
        echo "</table>";
    }
}

echo "</center>";
if(!empty($_POST['frame']) and !empty($_POST['action'])){
   echo "<form method='post' action='{$_POST['action']}' target='{$_POST['frame']}' id='{$_POST['frame']}'>";
   echo "<input type='hidden' id='gid' name='gid'>";
   echo "<input type='hidden' id='fmanager' name='fmanager' value='{$fmanager}'></form";
}

echo <<<EOT
</body></html>
EOT;
?>