<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Events';
//$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>

<script>
//$(document).bind('pageinit',
$(document).ready(
 function()
  {
    var p= $.param( {opt: 'uselect',eaction: 'Gsubscription'} );
    $("#Enecategories").load("../cyf/cyf_subscription.php", p);
//   $("#necategories").load("../cyf/cyf_subscription.php", {opt: 'uselect',eaction:'Gsubscription'});
    p= $.param( {opt: 'li',filters:'wotime',fcategs:'{}',eaction:'Vevent'} );
   $("#events").load("../cyf/cyf_event.php", p);
  });
$( document ).on( 'vclick', "a#Eaid", function() {
    $.mobile.changePage('#showevent');
//
    $('#Enid').val($(this).data('userId'));
    if($(this).data('userId')!=0){
            var p= {eid: $(this).data('userId'),eaction: 'Jevent'};
            $.ajax({url: "../cyf/cyf_event.php",typr:'post', data: p,
            success: function(msg){
			if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#Enid').val(pg.id);
                $("select#Eauthor").val(pg.author);
//                $('select#Eauthor').selectmenu().selectmenu('refresh',true);  
                $("select#Eagroups").val(pg.sgroups);
//                $('select#Eagroups').selectmenu().selectmenu('refresh',true);  
                $('select#Enecategories').val(pg.categs);
//                $('select#Enecategories').selectmenu('refresh',true);
                $('#Eevent').val(pg.descr);
                $('#Eurl').val(pg.url);
                $('#Esdate').val(pg.locat);
                $('#Estime').val(pg.longt);
                $('#Eedate').val(pg.lat);
                $('#Eetime').val(pg.aday);
                $('#Elocation').val(pg.opentime);
                $('#Elongt').val(pg.closetime);
                $('#Elat').val(pg.contact);
                $('#Econtact').val(pg.tel);
                $('#Etel').val(pg.tel);
                $('#Eemail').val(pg.email);
//                $('#contract').val(pg.contract);
            }
         }
    });
    }
    return true;
});
$( document ).on('change',"select#Efilters", function(){
    var p= $.param( {
        opt: 'li',
        filters: $("select#Efilters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eaction: 'Vevent'
    });
    $("#events").load("../cyf/cyf_event.php", p);
});
$( document ).on('change',"select#Efecategories", function(){
    var p= $.param( {
		opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eaction: 'Vevent'} );
    $("#events").load("../cyf/cyf_event.php", p);
});
$( document ).on( "vclick", "a#devent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#Efilters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eaction: 'Devent',
        eid:     $('#Enid').val()
    });
    $("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#sevent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#Efilters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eaction: 'Sevent',
        eid: $('#Enid').val(),
        author:  $('select#Eauthor').val(),
        agroups: '{'+($("select#Eagroups").val()!=null ? $("select#Eagroups").val().join() :"")+'}',
        categs: '{'+($("select#Enecategories").val()!=null ? $("select#Enecategories").val().join() : '')+'}'
    });
	$("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#cevent", function() {
	var p= $.param( {
        opt: 'li',
        eaction: 'Cevent',
        filters: $("select#Efilters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eid:     $('#Enid').val(),
        author:  $('select#Eauthor').val(),
        agroups: '{'+($("select#Eagroups").val()!=null ? $("select#Eagroups").val().join() :"")+'}',
        descr:   $('#Eevent').val(),
        url:     $('#Eurl').val(),
        stimes:   $('#Esdate').val() + ' '+ $('#Estime').val(),
        etimes:   $('#Eedate').val() + ' '+ $('#Eetime').val(),
        locat:   $('#Elocation').val(),
        longt:   $('#Elongt').val(),
        lat:     $('#Elat').val(),
        contact: $('#Econtact').val(),
        tel:     $('#Etel').val(),
        email:   $('#Eemail').val(),
        categs: '{'+($("select#Enecategories").val()!=null ? $("select#Enecategories").val().join() : '')+'}'
    });
		$("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#nevent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#Efilters").val(),
        fcategs: '{'+($("select#Efecategories").val()!=null ? $("select#Efecategories").val().join():"")+'}',
        eaction: 'Nevent',
        author:  $('select#Eauthor').val(),
        agroups:  '{'+($("select#Eagroups").val()!=null ? $("select#Eagroups").val().join() :"")+'}',
        descr:   $('#Eevent').val(),
        url:     $('#Eurl').val(),
        stimes:   $('#Esdate').val() + ' '+ $('#Estime').val(),
        etimes:   $('#Eedate').val() + ' '+ $('#Eetime').val(),
        locat:   $('#Elocation').val(),
        longt:   $('#Elongt').val(),
        lat:     $('#Elat').val(),
        contact: $('#Econtact').val(),
        tel:     $('#Etel').val(),
        email:   $('#Eemail').val(),
        categs: '{'+($("select#Enecategories").val()!=null ? $("select#Enecategories").val().join() : '')+'}'
    });
	$("#events").load("../cyf/cyf_event.php", p)
    return true;
});
	
//	$( document ).on( "vclick", "a#getcoorev", function() {
//		    geocoder = new google.maps.Geocoder();
//    geocoder.geocode( { 'address': $("#Elocation").val()}, function(results, status){
//	        if (status == 'OK') {
//			alert(results[0].geometry.location.lat);
//			alert(results[0].geometry.location.lng);
//            $("#Elat").val(results[0].geometry.location.lat);
//			$("#Elongt").val(results[0].geometry.location.lng);
//        }
//	});
//	});

</script>
</head>
<body>
<!-- cyf_events.html -->	
<form  id="f" method="POST" action="<?php echo $self; ?>">
  <div data-role="page" id="listevent" data-position="fixed"> <!-- page -->
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
		<select name="Efilters" id="Efilters" data-native-menu="false">
		  <option data-placeholder="true">Filters</option>
          <option value="all">All</option>
          <option value="lmonth">Last month</option>
          <option value="lweek">Last week</option>
          <option value="lday">Yesterday</option>
          <option value="tday">Today</option>
          <option value="nweek">Next week</option>
          <option value="nmonth">Next month</option>
          <option value="future">In future</option>
          <option value="wotime">No time</option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Efecategories[]" id="Efecategories" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Subscriptions</option>
<!--      <option value="1">Get subscriptions</option> -->
<?php
$q="SELECT theme FROM subscriptions WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
?>
		</select>
	  </li>
    </ul>
  </div>
	</div>
	  
<div data-role="main" class="ui-content">
        <h3></h3>
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="events">
          <li>empty</li>
        </ul>

</div> 
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="newevent"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle">Event</h2>
  </div>
  <div data-role="main" class="ui-content" id="ievent">
    <input type='hidden' name='Enid' id='Enid'>
	<ul data-role="listview" id="listevent">
	  <li >
		<label for="Eauthor">Author:</label>
		<select name="Eauthor" id="Eauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eagroups">for groups:</label>
		<select name="Eagroups[]" id="Eagroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eevent">message:</label>
		<textarea name="Eevent" id="Eevent" placeholder="Text ..."></textarea>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>date, time</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
			  <div class="ui-block-a"><label for="Estime">begin:</label><input data-mini="true" type="date" name="Esdate" id="Esdate"><input data-mini="true" type="time" name="Estime" id="Estime"></div>
			  <div class="ui-block-b"><label for="Eetime">end:</label><input data-mini="true" type="date" name="Eedate" id="Eedate"><input data-mini="true" type="time" name="Eetime" id="Eetime"></div>
    <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>location</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Eurl">URL:</label>
			<input type="url" name="Eurl" id="Eurl">
			<label for="Elocation">Address:</label>
			<textarea name="Elocation" id="Elocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorev">Get coordinates</a></p>
			<label for="Elongt">Longitude:</label>
			<input type="number" step="any" name="Elongt" id="Elongt">
			<label for="Elat">Latitude:</label>
			<input type="number" step="any" name="Elat" id="Elat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>contact</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Econtact">contact:</label>
			<input type="text" name="Econtact" id="Econtact" placeholder="Name ...">
			<label for="Etel">tel:</label>
			<input type="tel" name="Etel" id="Etel" placeholder="tel ...">
			<label for="Eemail">email:</label>
			<input type="email" name="Eemail" id="Eemail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Subscriptions</h2>
		<select class="Enecategories" multiple="multiple" data-native-menu="false" name="Enecategories[]" id="Enecategories" data-mini="true">
<!--		  <option data-placeholder="true">Choose subscriptions</option> -->
		</select>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing events ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="sevent">Publish event</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="devent">Delete event</a>
	  </li>
     
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For private group &quot;<?php echo $param['email'];?>&quot; only ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cevent">Change event</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nevent">Save new event</a>
	  </li>
	</ul>
  </div>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
</div> <!-- page -->

<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="main" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Lang: <?php echo $param['lang']; ?></li>
    <li>Groups member:<br><?php echo $param['membergroups']; ?></li>
    <li>Groups owner:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
	
  </div>
</div>
<div data-role="page" data-dialog="true" data-mini="true" id="showevent"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="poititle">Event</h2>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="a">
<!--    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r">Show on map</a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r">Show path</a></li>-->
    <li><a href="#newevent" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r">View/Edit event</a></li>
    </ul>
 </div>
</div><!-- page ends -->


</form>
</body>
</html>
