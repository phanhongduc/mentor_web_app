<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Groups';
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
$( document ).on( "vclick", "a#vgroup", function() {
	var p= {eaction: 'Vgroup'};
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( "vclick", "a#dgroup", function() {
	var p= {
        eaction: 'Dgroup',
        eid:     $('#Gnid').val()
    };
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( "vclick", "a#cgroup", function() {
	var p= {
        eaction: 'Cgroup',
        eid:     $('#Gnid').val(),
        gname:  $('#Ggname').val()
    };
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( "vclick", "a#sgroup", function() {
	var p= {
        eaction: 'Sgroup',
        eid:     $('#Gnid').val(),
        owner:  $('select#Gowner').val()
    };
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( "vclick", "a#ngroup", function() {
	var p= {
        eaction: 'Ngroup',
        owner:  $('select#Gowner').val(),
        gname:   $('#Ggname').val()
    };
	$("#groups").load("../cyf/cyf_group.php", p)
    return true;
});
$( document ).on( 'vclick', "a#Gaid", function() {
    $('#Gnid').val($(this).data('userId'));
    $('#Ggname').val($(this).data('userGname'));
    $("select#Gowner").val($(this).data('userOwner'));
//    $('select#Gowner').selectmenu().selectmenu('refresh',true);  
//    $.mobile.changePage('#newgroup');
    return true;
});
$(document).on( "pageinit", "#listgroup", function( event ) {
//	var p= {eaction: 'Vgroup'};
//    console.log("pageinit");
//	$("#groups").load("../cyf/cyf_group.php", p);
//    $('#listgroup').find( ":jqmData(role=listview)" ).listview().listview("refresh");
});
$(document).on( "pagebeforeshow", "#newgroup", function( event ) {
    $('select#Gowner').selectmenu().selectmenu('refresh',true);  
});
$(document).on( "pagebeforeshow", "#listgroup", function( event ) {
//    console.log("pagebeforeshow");
//	var p= {eaction: 'Jgroups'};
//    $.ajax({url: "../cyf/cyf_group.php",data: p,
//            success: function(msg){
//            console.log(msg);
//        }
//    });
	var p= {eaction: 'Vgroup'};
	$("#groups").load("../cyf/cyf_group.php", p);
//    $('#listgroup').find( ":jqmData(role=listview)" ).listview().listview("refresh");
});
$(document).ready(function (){
//	var p= {eaction: 'Vgroup'};
//	$("#groups").load("../cyf/cyf_group.php", p);
    $.mobile.changePage('#listgroup');
});
</script>
<?php
?>
</head>
<body>
<!-- cyf_groups.php -->	
<input type='hidden' name='Gnid' id='Gnid'>
<?php
?>
<div data-role="page" id="blankgroup" data-position="fixed"> <!-- page -->
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
	  </li>
      <li>
	  </li>
    </ul>
  </div>
</div>
<div data-role="content" class="ui-content">
<h3></h3>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listgroup" class="ui-btn ui-btn-corner-all ui-shadow" id="vgroup">Refresh Groups</a>
<a href='#newgroup' data-role="button" data-icon="plus" class="ui-bar">Create/Edit Group</a>
</div>
</div>
    
<div data-role="page" id="listgroup" data-position="fixed"> <!-- page -->
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
	  </li>
      <li>
	  </li>
    </ul>
  </div>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="groups">
<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>
<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid' data-user-id='0' data-user-gname=' ' data-user-owner='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#newgroup' data-role="button" data-icon="plus" class="ui-bar">Create/Edit Group</a>
</div>
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="newgroup"> <!-- page -->
<div data-role="header" data-position="fixed">
    <h2 id="grouptitle">Group</h2>
</div>
<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
    <div class="ui-field-contain">
		<label for="Gowner">Owner:</label>
		<select name="Gowner" id="Gowner" data-native-menu="false" data-mini="true" data-inset="true">
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
    echo "<option value='{$param['email']}'";
    echo ">{$param['email']}</option>";
//$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
//foreach ($a as $k => $v){
//    $w = ltrim(rtrim($v,'"'),'"');
//    echo "<option value='{$w}'";
//    echo ">{$w}</option>";
//}
?>
		</select>
    </div> 
    </li>
    <li>
		<label for="Ggname">Group:</label>
		<input type="text" name="Ggname" id="Ggname" placeholder="Group name ...">
    </li>
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing Group ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="sgroup">Publish Group</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dgroup">Delete Group</a>
	</li>
     
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For private group &quot;<?php echo $param['email'];?>&quot; Group only ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cgroup">Change Group</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="ngroup">Save new Group</a>
	</li>
 </ul>
</div>
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="vgroup">Refresh Groups</a>
</div>
</div>
</body>
</html>
