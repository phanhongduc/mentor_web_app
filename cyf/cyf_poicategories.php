<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services - POI categories' : $_REQUEST['provider'];
$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).bind('pageinit',
//$(document).ready(
 function()
  {
   var p= $.param( {opt: 'uselect',eaction:'Vpoicategory'});
   $("select#Psubthemes").load("../cyf/cyf_poicategory.php", p);
   p= $.param( {opt: 'li',eaction:'Vpoicategory'});
   $("#poicategories").load("../cyf/cyf_poicategory.php", p);
});
$( document ).on( 'vclick', "a#Paid", function() {
    $.mobile.changePage('#poicategory');
    $('#nid').val($(this).data('userId'));
    if($(this).data('userId')!=0){
            var p= $.param( {id: $(this).data('userId'),eaction: 'Jpoicategory'});
     $.ajax({url: "../cyf/cyf_poicategory.php",data: p,
            success: function(msg){
			if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#Pnid').val(pg.id);
                $("select#Powner").val(pg.owner);
                $('select#Powner').selectmenu().selectmenu('refresh',true);  
                $("select#Pallowgroups").val(pg.allowgroups);
                $('select#Pallowgroups').selectmenu().selectmenu('refresh',true);  
                $('select#Ppublishgroups').val(pg.publishgroups);
                $('select#Ppublishgroups').selectmenu('refresh',true);
                $("select#Psubthemes").val(pg.subthemes);
                $('select#Psubthemes').selectmenu().selectmenu('refresh',true);  
 
                $('#Ptheme').val(pg.theme);
            }
         }
    });
    }
    return false;
});
$( document ).on( "vclick", "a#dpoicategory", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Dpoicategory',
            eid: $('#Pnid').val()
    });
    $("#poicategories").load("../cyf/cyf_poicategory.php", p);
    return false;
});
$( document ).on( "vclick", "a#cpoicategory", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Cpoicategory',
            eid:   $('#Pnid').val(),
            owner: $('#Powner').val(),
            theme: $('#Ptheme').val(),
            subthemes: '{'+($("select#Psubthemes").val()!=null ? $("select#Psubthemes").val().join() : $('#Psubthemes').val())+'}',
            allowgroups: '{'+($("select#Pallowgroups").val()!=null ? $("select#Pallowgroups").val().join() : $('#Pallowgroups').val())+'}',
            publishgroups: '{'+($("select#Ppublishgroups").val()!=null ? $("select#Ppublishgroups").val().join() : $('#Ppublishgroups').val())+'}'
    });
    $("#poicategories").load("../cyf/cyf_poicategory.php", p);
    return true;
});
$( document ).on( "vclick", "a#npoicategory", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Npoicategory',
            owner: $('#Powner').val(),
            theme: $('#Ptheme').val(),
            subthemes: '{'+($("select#Psubthemes").val()!=null ? $("select#Psubthemes").val().join() : $('#Psubthemes').val())+'}',
            allowgroups: '{'+($("select#Pallowgroups").val()!=null ? $("select#Pallowgroups").val().join() : $('#Pallowgroups').val())+'}',
            publishgroups: '{'+($("select#Ppublishgroups").val()!=null ? $("select#Ppublishgroups").val().join() : $('#Ppublishgroups').val())+'}'
    });
    $("#poicategories").load("../cyf/cyf_poicategory.php", p);
    return true;
});
</script>
</head>
<body >
<form  id="f" method="POST" action="<?php echo $self; ?>">
  <div data-role="page" id="listevent" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
    </div>

    <div data-role="main" class="ui-content">
        <h3></h3>
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="poicategories">
          <li>empty</li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="poicategory">
  <div data-role="header" data-position="fixed">
    <h2>Theme</h2>
  </div>
    <div data-role="main" class="ui-content" id="mpoicategory">
	  <ul data-role="listview" id="poicategoryfields">
	  <li >
		<label for="Powner">Moderator:</label>
		<select name="Powner" id="Powner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ptheme">Theme:</label>
		<textarea name="Ptheme" id="Ptheme" placeholder="Text ..."></textarea>
        <input type='hidden' name='Pnid' id='Pnid'>
	  </li>
	  <li >
		<label for="Psubthemes">Cross reference themes:</label>
		<select name="Psubthemes[]" id="Psubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Themes</option>
		</select>
	  </li>
	  <li >
		<label for="Pallowgroups">Reading only groups:</label>
		<select name="Pallowgroups[]" id="Pallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ppublishgroups">Publishing groups:</label>
		<select name="Ppublishgroups[]" id="Ppublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoicategory">Delete POI category</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoicategory">Change POI category</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoicategory">Save new POI category</a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>

<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="main" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Groups member:<br><?php echo $param['membergroups']; ?></li>
    <li>Groups owner:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
	
  </div>
</div>
</form>
</body>
</html>
