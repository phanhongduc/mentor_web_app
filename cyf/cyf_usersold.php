<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cyf/cyf_contracts.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf=cyfConnect();
echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<style>
td {vertical-align:top;margin-left: 6px;text-align:left;}
th {margin-left: 6px;text-align:center;font-size:1.2em;}
</style>
<title>Users Control Tool</title>
<script>
window.onload = function(){}
</script>
</head><body>
<center><br>
EOT;

//echo "post: ";print_r($_REQUEST);echo "<br>";
$fmanager = ltrim(rtrim($_REQUEST['rgroups'],'"}'),'{"'); //$param['email']

echo <<<EOT
<table >
<tr><th>Users<br>
<i style="font-size:0.9em;">(Contact, Email, Membership)</i></th><th>{$fmanager} Users Control Tool <div style="color:#777;">manager: {$param['email']}</div><a href="../cis/login.php">Exit</a></th>
<!-- <th>{$fmanager} Groups<br>(Group / Owner)</th> -->
</tr>
<tr>
<td>
<iframe name="userslist" width="600" height="680" src="../cyf/cyf_userslist.php?usefilters=usefilters&fmanager={$param['email']}"></iframe></td>
<td>
<iframe name="usersform" width="800" height="680" src="../cyf/cyf_usersform.php?usefilters=usefilters&fmanager={$fmanager}"></iframe>
</td>
<!-- <td>
<iframe name="groupslist" width="300" height="680" src="../cyf/cyf_groupslist.php?usefilters=usefilters&fmanager={$fmanager}"></iframe></td> -->
  </tr>
</table>
<!-- border:none; -->
</center>
</body>
</html>
EOT;
?>