$( document ).on('change',"select#filters", function(){
    var p= $.param( {
        opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eaction: 'Vevent'
    });
    $("#events").load("../cyf/cyf_event.php", p);
});
$( document ).on('change',"select#fecategories", function(){
    var p= $.param( {
		opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eaction: 'Vevent'} );
    $("#events").load("../cyf/cyf_event.php", p);
});
$( document ).on( "vclick", "a#devent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eaction: 'Devent',
        eid:     $('#nid').val()
    });
    $("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#sevent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eaction: 'Sevent',
        eid: $('#nid').val(),
        author:  $('select#author').val(),
        agroups: '{'+($("select#agroups").val()!=null ? $("select#agroups").val().join() :"")+'}',
        categs: '{'+($("select#necategories").val()!=null ? $("select#necategories").val().join() : '')+'}'
    });
	$("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#cevent", function() {
	var p= $.param( {
        opt: 'li',
        eaction: 'Cevent',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eid:     $('#nid').val(),
        author:  $('select#author').val(),
        agroups: '{'+($("select#agroups").val()!=null ? $("select#agroups").val().join() :"")+'}',
        descr:   $('#tevent').val(),
        url:     $('#url').val(),
        stimes:   $('#sdate').val() + ' '+ $('#stime').val(),
        etimes:   $('#edate').val() + ' '+ $('#etime').val(),
        locat:   $('#location').val(),
        longt:   $('#longt').val(),
        lat:     $('#lat').val(),
        contact: $('#contact').val(),
        tel:     $('#tel').val(),
        email:   $('#email').val(),
        categs: '{'+($("select#necategories").val()!=null ? $("select#necategories").val().join() : '')+'}'
    });
		$("#events").load("../cyf/cyf_event.php", p);
    return true;
});
$( document ).on( "vclick", "a#nevent", function() {
	var p= $.param( {
        opt: 'li',
        filters: $("select#filters").val(),
        fcategs: '{'+($("select#fecategories").val()!=null ? $("select#fecategories").val().join():"")+'}',
        eaction: 'Nevent',
        author:  $('select#author').val(),
        agroups:  '{'+($("select#agroups").val()!=null ? $("select#agroups").val().join() :"")+'}',
        descr:   $('#tevent').val(),
        url:     $('#url').val(),
        stimes:   $('#sdate').val() + ' '+ $('#stime').val(),
        etimes:   $('#edate').val() + ' '+ $('#etime').val(),
        locat:   $('#location').val(),
        longt:   $('#longt').val(),
        lat:     $('#lat').val(),
        contact: $('#contact').val(),
        tel:     $('#tel').val(),
        email:   $('#email').val(),
        categs: '{'+($("select#necategories").val()!=null ? $("select#necategories").val().join() : '')+'}'
    });
	console.log(p);
	$("#events").load("../cyf/cyf_event.php", p)
    return true;
});
