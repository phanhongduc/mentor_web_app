<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

if ($param['email'] == 'guest') header("Location: https://www.google.com");
$cyf = cyfConnect();
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
function prnGroups($res,$opt){
if ($res and pg_num_rows($res) >0) {
    $r = array();
    switch($opt){
    case 'li':
        while (($r=pg_fetch_assoc ( $res ))){
            echo "<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'";
            echo " data-user-id='{$r['id']}' data-user-gname='{$r['gname']}' data-user-owner='{$r['owner']}'>";
            echo "<p><b style='font-size:130%;'>".$r['gname']." </b> (<i>".$r['owner']."</i>)</p>";
            echo "</a>";
            echo "</li>";
        }
    break;
    case 'select':
        while (($r=pg_fetch_assoc ( $res ))){
                echo "<option value='".$r['gname']."'>".$r['gname']."</option>";
        }
        echo "<option value='".$param['email']."'>".$param['email']."</option>";
        break;
    default:
        $d='[';
        while (($r=pg_fetch_assoc ( $res ))){
            echo $d.'{"id":"'.$r['id'].'","gname":"'.$r['gname'].'","owner":"'.$r['owner'].'"}';
            $d=',';
        }
        echo ']';
    }
}else{
    switch($opt){
    case 'select':
        echo "<option value='".$param['email']."'>".$param['email']."</option>";
    break;
    case 'li':
            echo "<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'";
            echo " data-user-id='0' data-user-gname='{$param['email']}' data-user-owner='{$param['email']}'>";
            echo "<p><b style='font-size:130%;'>".$param['email']." </b> (<i>".$param['email']."</i>)</p>";
            echo "</a>";
            echo "</li>";
        break;
    default:
        echo '[{"id":0}]';
    }
}
};
//opt li, select, json
//owner        default $param['email']
//ownergroups  default $param['ownergroups'] or ownergroups: {"Group 1",...,"Group N"}
//member       default empty
//membergroups default empty
//             if(!empty(member))
//             membergroups default $param['membergroups'] or membergroups: {"Group 1",...,"Group N"}
$opt =  empty($_REQUEST['opt']) ? 'json' : pg_escape_string($_REQUEST['opt']);


$owner =  empty($_REQUEST['owner']) ? $param['email']
    : pg_escape_string($_REQUEST['owner']);

//$email =  empty($_REQUEST['email']) ? $param['email'] : $_REQUEST['email'];
$ownergroups =  empty($_REQUEST['ownergroups']) ? $param['ownergroups']
    : pg_escape_string($_REQUEST['ownergroups']);
if(!empty($_REQUEST['member']))
$membergroups =  empty($_REQUEST['membergroups']) ? $param['membergroups']
    : pg_escape_string($_REQUEST['membergroups']);

//owner        default $param['email']
//allowgroups   default $param['email']
//publishgroups default $param['email']

//"{".$param['email']."}" :
$allowgroups =  empty($_REQUEST['allowgroups']) ? $membergroups :
    pg_escape_string($_REQUEST['allowgroups']);
//"{".$param['email']."}" :
$publishgroups =  empty($_REQUEST['publishgroups']) ? $ownergroups : 
    pg_escape_string($_REQUEST['publishgroups']);

    switch($_REQUEST['eaction']){
    case 'Igroup':
//
// eaction: 'Igroup'      
// eid: id
// opt:'json'   = Jgroup
//    [{"id":"id","gname":"gname","owner":"owner"}...{"id":"id","gname":"gname","owner":"owner"}]
// opt:'select' = <option value='".$r['gname']."'>".$r['gname']."</option>
// opt:'li'     = default
//<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'
// data-user-id='{$r['id']}' data-user-gname='{$r['gname']}' data-user-owner='{$r['owner']}'>
//<p><b style='font-size:130%;'>".$r['gname']." </b> (<i>".$r['owner']."</i>)</p></a></li>
        
$q = "SELECT id,gname,owner FROM groups";
$q.= " WHERE id=".$_REQUEST['eid']." AND (";
$q.= " ARRAY[owner] && '".$param['ownergroups']."'";
//$q.= " OR ARRAY[owner] && '".$param['membergroups']."'";
$q.= " OR owner='".$param['email']."'";
$q.= ")";
$q.= " ORDER BY gname;";
//echo "<br>".$q."<br>";
$res = pg_query($conn, $q);
prnGroups($res,$opt);
    break;

    case 'Vgroups':
//= Jgroups
// eaction: 'Vgroups'      
// opt:'json'   = Jgroups
//    [{"id":"id","gname":"gname","owner":"owner"}...{"id":"id","gname":"gname","owner":"owner"}]
// opt:'select' = Sgroups  <option value='".$r['gname']."'>".$r['gname']."</option>
// opt:'li'     = default
//<li><a href='#newgroup' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Gaid'
// data-user-id='{$r['id']}' data-user-gname='{$r['gname']}' data-user-owner='{$r['owner']}'>
//<p><b style='font-size:130%;'>".$r['gname']." </b> (<i>".$r['owner']."</i>)</p></a></li>
        
$q = "SELECT id,gname,owner FROM groups";
$q.= " WHERE owner='".$owner."'";
$q.= " OR ARRAY[owner] && '".$ownergroups."'";
if(!empty($_REQUEST['member']))
$q.= " OR ARRAY[owner] && '".$membergroups."'";
$q.= " ORDER BY gname";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
$res = pg_query($conn, $q);
prnGroups($res,$opt);
    break;

    case 'Apoicategories':
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM poicategories";
$q.=" WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}'";
$q.=" OR allowgroups && '{$param['membergroups']}'";
$q.=" OR allowgroups && '{$param['ownergroups']}'";    
$q.= " ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    $d='[';
    while (($r=pg_fetch_assoc ( $res ))){
        echo $d.'{';
        $agr= json_encode(explode('~^~',$r['agr'] ));
        $pgr= json_encode(explode('~^~',$r['pgr']));
        $subthem= json_encode(explode('~^~',$r['subthem']));
        echo '"id":"'.$r['id'].'","theme":"'.$r['theme'].'"';
        echo ',"owner":"'.$r['owner'].'"';
        echo ',"subthemes":'.$subthem;
        echo ',"allowgroups":'.$agr;
        echo ',"publishgroups":'.$pgr;
        echo "}";
        $d=',';
    }
    echo ']';
}else{
    echo '[{"id":0}]';
}
break;
    
    case 'Vpoicategories':
//owner         default $param['email']
//allowgroups   default $param['email']
//publishgroups default $param['email']
        switch($opt){
        case 'json':
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM poicategories";
            break;
        default:
$q ="SELECT id,owner,theme,subthemes,allowgroups,publishgroups FROM poicategories";
        }
$q.=" WHERE ";
//[owner:""]
//req owner == owner poicategories or $param['email'] == owner poicategories
if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
    $q.= " owner='".pg_escape_string($_REQUEST['owner'])."'";
else
    $q.= " owner='{$param['email']}'";
//owner:"",member:""[,membergroups:""]
//owner poicategories && req membergroups or owner poicategories && $param['membergroups']
if(!empty($_REQUEST['owner']) AND !empty($_REQUEST['member'])){ 
    if(!empty($_REQUEST['membergroups'])AND $_REQUEST['membergroups'] !=='')
        $q.= " OR ARRAY[owner] && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR ARRAY[owner] && '{$param['membergroups']}'";
}
//publish:""[,owner:""]
//req owner && publishgroups poicategories or $param['email'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
        $q.= " OR publishgroups && '{".pg_escape_string($_REQUEST['owner'])."}'";
    else
        $q.= " OR publishgroups && '{{$param['email']}}'";
}
//publish:""[,publishgroups:""]
//req publishgroups && publishgroups poicategories or $param['ownergroups'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['publishgroups']) AND $_REQUEST['publishgroups'] !=='')
        $q.= " OR publishgroups && '".pg_escape_string($_REQUEST['publishgroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['ownergroups']}'";
}
//member:"",publish:""[,membergroups:""]
//req membergroups && publishgroups poicategories or $param['membergroups'] && publishgroups poicategories
if(!empty($_REQUEST['member']) AND !empty($_REQUEST['publish'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " publishgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['membergroups']}'";
}
//member:"",allow:""[,membergroups:""]
//req membergroups && allowgroups poicategories or $param['membergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['member'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " allowgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['membergroups']}'";
}
//owner:"",allow:""[,allowgroups:""]
//req allowgroups && allowgroups poicategories or $param['ownergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['owner'])){
    if(!empty($_REQUEST['allowgroups']) AND $_REQUEST['allowgroups'] !=='')
        $q.= " OR allowgroups && '".pg_escape_string($_REQUEST['allowgroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['ownergroups']}'";
}
$q .= " ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
//echo $q;
$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    switch($opt){
    case 'li':
        while (($r=pg_fetch_assoc ( $res ))){
            echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>{$r['theme']}</strong> ({$r['owner']})</li>";
            echo "<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'";
            echo " data-user-id='{$r['id']}'";
            echo ">";
            echo "<h3><strong>{$r['theme']}</strong></h3>";
            echo "<p><strong>Cross reference themes: {$r['subthemes']}</strong></p>";
//                echo "<p><strong>Publishing groups: {$r['publishgroups']}</strong></p>";
            echo "<p><strong>Allow groups: {$r['allowgroups']}</strong></p>";
            echo "</a>";
            echo "</li>";
        }
    break;
    case 'select':
        while (($r=pg_fetch_assoc ( $res ))){
                echo "<option value='".$r['theme']."'>".$r['theme']."</option>";
        }
        break;
    default:
        $d='[';
        while (($r=pg_fetch_assoc ( $res ))){
            echo $d.'{';
            $agr= json_encode(explode('~^~',$r['agr'] ));
            $pgr= json_encode(explode('~^~',$r['pgr']));
            $subthem= json_encode(explode('~^~',$r['subthem']));
            echo '"id":"'.$r['id'].'","theme":"'.$r['theme'].'"';
            echo ',"owner":"'.$r['owner'].'"';
            echo ',"subthemes":'.$subthem;
            echo ',"allowgroups":'.$agr;
            echo ',"publishgroups":'.$pgr;
            echo "}";
            $d=',';
        }
        echo ']';
    }
}else{
    switch($opt){
    case 'li':
    case 'select':
    break;
    default:
        echo '[{"id":0}]';
    }
}
    break;

    case 'Asubscriptions':
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
$q.=" WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}'";
$q.=" OR allowgroups && '{$param['membergroups']}'";
$q.=" OR allowgroups && '{$param['ownergroups']}'";    
$q.= " ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    $d='[';
    while (($r=pg_fetch_assoc ( $res ))){
        echo $d.'{';
        $agr= json_encode(explode('~^~',$r['agr'] ));
        $pgr= json_encode(explode('~^~',$r['pgr']));
        $subthem= json_encode(explode('~^~',$r['subthem']));
        echo '"id":"'.$r['id'].'","theme":"'.$r['theme'].'"';
        echo ',"owner":"'.$r['owner'].'"';
        echo ',"subthemes":'.$subthem;
        echo ',"allowgroups":'.$agr;
        echo ',"publishgroups":'.$pgr;
        echo "}";
        $d=',';
    }
    echo ']';
}else{
    echo '[{"id":0."src:"}]';
}
break;
    
    case 'Vsubscriptions':
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
$q.= " WHERE ";
//[owner:""]
//req owner == owner poicategories or $param['email'] == owner poicategories
if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
    $q.= " owner='".pg_escape_string($_REQUEST['owner'])."'";
else
    $q.= " owner='{$param['email']}'";
//owner:"",member:""[,membergroups:""]
//owner poicategories && req membergroups or owner poicategories && $param['membergroups']
if(!empty($_REQUEST['owner']) AND !empty($_REQUEST['member'])){ 
    if(!empty($_REQUEST['membergroups'])AND $_REQUEST['membergroups'] !=='')
        $q.= " OR ARRAY[owner] && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR ARRAY[owner] && '{$param['membergroups']}'";
}
//publish:""[,owner:""]
//req owner && publishgroups poicategories or $param['email'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
        $q.= " OR publishgroups && '{".pg_escape_string($_REQUEST['owner'])."}'";
    else
        $q.= " OR publishgroups && '{{$param['email']}}'";
}
//publish:""[,publishgroups:""]
//req publishgroups && publishgroups poicategories or $param['ownergroups'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['publishgroups']) AND $_REQUEST['publishgroups'] !=='')
        $q.= " OR publishgroups && '".pg_escape_string($_REQUEST['publishgroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['ownergroups']}'";
}
//member:"",publish:""[,membergroups:""]
//req membergroups && publishgroups poicategories or $param['membergroups'] && publishgroups poicategories
if(!empty($_REQUEST['member']) AND !empty($_REQUEST['publish'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " publishgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['membergroups']}'";
}
//member:"",allow:""[,membergroups:""]
//req membergroups && allowgroups poicategories or $param['membergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['member'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " allowgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['membergroups']}'";
}
//owner:"",allow:""[,allowgroups:""]
//req allowgroups && allowgroups poicategories or $param['ownergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['owner'])){
    if(!empty($_REQUEST['allowgroups']) AND $_REQUEST['allowgroups'] !=='')
        $q.= " OR allowgroups && '".pg_escape_string($_REQUEST['allowgroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['ownergroups']}'";
}
$q .= " ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
//echo $q;
$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
        $d='[';
        while (($r=pg_fetch_assoc ( $res ))){
            echo $d.'{';
            $agr= json_encode(explode('~^~',$r['agr'] ));
            $pgr= json_encode(explode('~^~',$r['pgr']));
            $subthem= json_encode(explode('~^~',$r['subthem']));
            echo '"id":"'.$r['id'].'","theme":"'.$r['theme'].'"';
            echo ',"owner":"'.$r['owner'].'"';
            echo ',"subthemes":'.$subthem;
            echo ',"allowgroups":'.$agr;
            echo ',"publishgroups":'.$pgr;
            echo "}";
            $d=',';
        }
        echo ']';
    }else{
        echo '[{"id":0}]';
    }
    break;

    case 'Psubscriptions':
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
$q.= " WHERE ";
//[owner:""]
//req owner == owner poicategories or $param['email'] == owner poicategories
if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
    $q.= " owner='".pg_escape_string($_REQUEST['owner'])."'";
else
    $q.= " owner='{$param['email']}'";
//owner:"",member:""[,membergroups:""]
//owner poicategories && req membergroups or owner poicategories && $param['membergroups']
if(!empty($_REQUEST['owner']) AND !empty($_REQUEST['member'])){ 
    if(!empty($_REQUEST['membergroups'])AND $_REQUEST['membergroups'] !=='')
        $q.= " OR ARRAY[owner] && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR ARRAY[owner] && '{$param['membergroups']}'";
}
//publish:""[,owner:""]
//req owner && publishgroups poicategories or $param['email'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['owner']) AND $_REQUEST['owner'] !=='')
        $q.= " OR publishgroups && '{".pg_escape_string($_REQUEST['owner'])."}'";
    else
        $q.= " OR publishgroups && '{{$param['email']}}'";
}
//publish:""[,publishgroups:""]
//req publishgroups && publishgroups poicategories or $param['ownergroups'] && publishgroups poicategories
if(!empty($_REQUEST['publish'])){                               
    if(!empty($_REQUEST['publishgroups']) AND $_REQUEST['publishgroups'] !=='')
        $q.= " OR publishgroups && '".pg_escape_string($_REQUEST['publishgroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['ownergroups']}'";
}
//member:"",publish:""[,membergroups:""]
//req membergroups && publishgroups poicategories or $param['membergroups'] && publishgroups poicategories
if(!empty($_REQUEST['member']) AND !empty($_REQUEST['publish'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " publishgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR publishgroups && '{$param['membergroups']}'";
}
//member:"",allow:""[,membergroups:""]
//req membergroups && allowgroups poicategories or $param['membergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['member'])){
    if(!empty($_REQUEST['membergroups']) AND $_REQUEST['membergroups'] !=='')
        $q.= " allowgroups && '".pg_escape_string($_REQUEST['membergroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['membergroups']}'";
}
//owner:"",allow:""[,allowgroups:""]
//req allowgroups && allowgroups poicategories or $param['ownergroups'] && allowgroups poicategories
if(!empty($_REQUEST['allow']) AND !empty($_REQUEST['owner'])){
    if(!empty($_REQUEST['allowgroups']) AND $_REQUEST['allowgroups'] !=='')
        $q.= " OR allowgroups && '".pg_escape_string($_REQUEST['allowgroups'])."'";
    else
        $q.= " OR allowgroups && '{$param['ownergroups']}'";
}
$q .= " ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";
echo $q;
break;
    default:
    }
?>