<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Contracts';
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
$( document ).on( "vclick", "a#vcontract", function() {
	var p= {eaction: 'Vcontract'};
	$("#contracts").load("../cyf/cyf_contract.php", p)
    return true;
});
$( document ).on( "vclick", "a#dcontract", function() {
	var p= {eaction: 'Dcontract',eid: $('#Cnid').val()};
	$("#contracts").load("../cyf/cyf_contract.php", p)
    return true;
});
//cname,bname,address,bid,osec,contact,lang,tel,email,url,dstart,dend,manager,status,maxusers,contractgroups,usersgroups,apps
$( document ).on( "vclick", "a#ncontract", function() {
	var p= {eaction: 'Ncontract',
        cname: $('#cCname').val(),
        bname: $('#cBname').val(),
        bid: $('#cBid').val(),
        osec: $('#cOsec').val(),
        address: $('#cAddress').val(),
        contact: $('#cContact').val(),
        tel: $('#cTel').val(),
        url: $('#cUrl').val(),
        lang: $('#cLang').val(),
        manager: $('#cManager').text(),
        status: $('#cStatus').val(),
        dstart: $('#cDstart').val(),
        dend: $('#cDend').val(),
        email: $('#cEmail').val(),
        maxusers: $('#cMaxusers').val(),
        contractgroups: $('#cContractgroups').val(),
        usersgroups: $('#cUsersgroups').val(),
        apps: $('#cApps').val(),
        pass: $('cPassword').val()
    };
	$("#contracts").load("../cyf/cyf_contract.php", p)
    return true;
});
$( document ).on( "vclick", "a#ccontract", function() {
	var p= {
        eaction: 'Ccontract',
        cname: $('#cCname').val(),
        bname: $('#cBname').val(),
        bid: $('#cBid').val(),
        osec: $('#cOsec').val(),
        address: $('#cAddress').val(),
        contact: $('#cContact').val(),
        tel: $('#cTel').val(),
        url: $('#cUrl').val(),
        lang: $('#cLang').val(),
        manager: $('#cManager').text(),
        status: $('#cStatus').val(),
        dstart: $('#cDstart').val(),
        dend: $('#cDend').val(),
        email: $('#cEmail').val(),
        maxusers: $('#cMaxusers').val(),
        contractgroups: $('#cContractgroups').val(),
        usersgroups: $('#cUsersgroups').val(),
        apps: $('#cApps').val(),
        pass: $('cPassword').val()
    };
	$("#contracts").load("../cyf/cyf_contract.php", p)
    return true;
});
function getParam(){
        var p= {eaction: 'Jparam'};
        $.ajax({url:"../cyf/cyf_contract.php",data: p,
            success: function(msg){
            if(msg.length>0){
//                console.log(msg);
                var pg =   JSON.parse(msg);
                $('#mUsers').text(""+pg.musers);
                $('#cUsers').text(""+pg.cusers);
                $('#oUsers').text(""+pg.ousers);
                var o = 0+pg.musers-pg.cusers-pg.ousers;
                $('#aUsers').text(""+o);
            }
            }
            });
};
$( document ).on( 'vclick', "a#Caid", function() {
    $('#Cnid').val($(this).data('userId'));
    $('#contracttitle').text($(this).data('userCname'));
    getParam();
       
        var p= {eaction: 'Jcontract', eid: $('#Cnid').val()};
        $.ajax({url:"../cyf/cyf_contract.php",data: p,
            success: function(msg){
            if(msg.length>0){
//                console.log(msg);
                var pg =   JSON.parse(msg);
                if(pg.id!=0){
                    $('#cCname').val(pg.cname);
                    $('#cBname').val(pg.bname);
                    $('#cBid').val(pg.bid);
                    $('#cOsec').val(pg.osec);
                    $('#cAddress').val(pg.address);
                    $('#cContact').val(pg.contact);
                    $('#cTel').val(pg.tel);
                    $('#cUrl').val(pg.url);
                    $('#cLang').val(pg.lang);
                    $('#cManager').text(pg.manager);
                    $('#cStatus').val(pg.status);
                    $('#cDstart').val(pg.dstart);
                    $('#cDend').val(pg.dend);
                    $('#cEmail').val(pg.email);
                    $('#cMaxusers').val(pg.maxusers);
                    $('#cContractgroups').val(pg.contractgroups);
                    $('select#cgroups').val(pg.contractgroups);
//                    $('#contractmanager').text(pg.contractgroups);
                    $('#cUsersgroups').val(pg.usersgroups);
                    $('#cApps').val(pg.apps);
                }
            }
            }
            });
//    }
    return true;
});
$( document ).on( 'vclick', "a#crcontract", function() {
//    var m ="<?php echo htmlspecialchars($param['membergroups']); ?>";
//    var m =m.replace(/{/g, "").replace(/}/g, "");
//    $('#contractmanager').text(m);
    $('#cCname').val("New subcontract");
    $('#contracttitle').text($('#cCname').val());
    $('#cBname').val("");
    $('#cBid').val("");
    $('#cOsec').val("");
    $('#cAddress').val("");
    $('#cContact').val("");
    $('#cTel').val("");
    $('#cUrl').val("");
    $('#cLang').val("");
    $('#cManager').text("");
    $('#cStatus').val("");
    $('#cDstart').val("");
    $('#cDend').val("");
    $('#cEmail').val("");
    $('#cMaxusers').val("");
    $('#cContractgroups').val("");
//    $('#contractmanager').text("");
    $('#cUsersgroups').val("");
    $('#cApps').val("");
    $('#Cnid').val(0);
    
    getParam();
});
$( document ).on( 'vclick', "a#cgroups", function() {
    $('select#cgroups').selectmenu().selectmenu('refresh',true); 
});

$(document).on( "pagebeforeshow", "#newcontract", function( event ) {
	var p= {eaction: 'Sgroups'};
	$("#cgroups").load("../cyf/cyf_group.php", p);
    $('select#cgroups').selectmenu('refresh',true); 
});
$(document).on( "pagebeforeshow", "#listcontract", function( event ) {
	var p= {eaction: 'Vcontract'};
	$("#contracts").load("../cyf/cyf_contract.php", p);
});
$(document).ready(function (){
    $.mobile.changePage('#listcontract');
});
</script>
</head>
<body>
<!-- cyf_contracts.php -->	
<input type='hidden' name='Cnid' id='Cnid'>
<!-- page -->   
<div data-role="page" id="blankcontract" data-position="fixed">
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li></li>
      <li></li>
    </ul>
  </div>
</div>
<div data-role="content" class="ui-content">
<h3></h3>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listcontract" class="ui-btn ui-btn-corner-all ui-shadow" id="vcontract">Refresh contracts</a>
<a href='#newcontract' data-role="button" data-icon="plus" class="ui-bar">Create/Edit contract</a>
</div>
</div>
    
<!-- page -->   
<div data-role="page" id="listcontract" data-position="fixed">
<div data-role="header" data-position="fixed">
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li></li>
      <li></li>
    </ul>
  </div>
</div>
    
<div data-role="content" class="ui-content">
<h3></h3>
<ul data-role="listview" data-theme="d" class="ui-listview" id="contracts" data-filter="true" data-filter-placeholder="Search ..." data-inset="true">
<!-- <li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li> -->
<li><a href='#newcontract' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Caid' data-user-id='0' data-user-cname="" data-user-manager='<?php echo $param['email']; ?>'>empty</a></li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#listcontract" class="ui-btn ui-btn-corner-all ui-shadow" id="vcontract">Refresh contracts</a>
<a href='#newcontract' data-role="button" data-icon="plus" class="ui-bar" id="crcontract">Create contract</a>
</div>
</div>

<!-- page -->   
<div data-role="page" data-dialog="true" data-mini="true" id="newcontract"> 
<div data-role="header" data-position="fixed">
    <h2 id="contracttitle">Contract</h2>
</div>

<div data-role="content" class="ui-content">
 <ul data-role="listview">
    <li>
    <!--
    <p>Manager of the contract: <span id="contractmanager"></span></p>
    <p>Manager of the main contract: <span id="cManager"></span></p>
    -->
    <p>Users limit on a main contract: <span id="mUsers"></span></p>
    <p>Now users (own/subcontract users): <span id="oUsers"></span>/<span id="cUsers"></span></p>
    <p>Available users on a main contract: <span id="aUsers"></span></p>
    </li>
    <li>
		<label for="cCname">Contract name:</label>
		<input type="text" name="cCname" id="cCname" placeholder="name ...">
    <ul data-role="listview" data-inset="true" data-shadow="false">
     <li data-role="collapsible" data-iconpos="right" data-inset="false">
     <h2>Description</h2>
     <ul data-role="listview" data-theme="a">
      <li>
		<label for="cBname">Business name:</label>
		<input type="text" name="cBname" id="cBname" placeholder="cBname ...">
		<label for="cBid">Business id:</label>
		<input type="text" name="cBid" id="cBid" placeholder="cBid ...">
		<label for="cOsec">Osec:</label>
		<input type="text" name="cOsec" id="cOsec" placeholder="cOsec ...">
		<label for="cAddress">Address:</label>
		<input type="text" name="cAddress" id="cAddress" placeholder="cAddress ...">
		<label for="cContact">Contact:</label>
		<input type="text" name="cContact" id="cContact" placeholder="cContact ...">
		<label for="cTel">tel:</label>
		<input type="text" name="cTel" id="cTel" placeholder="tel ...">
		<label for="cUrl">url:</label>
		<input type="text" name="cUrl" id="cUrl" placeholder="url ...">
		<label for="cLang">lang:</label>
		<input type="text" name="cLang" id="cLang" placeholder="lang ...">
		<label for="cDstart">start:</label>
		<input type="date" name="cDstart" id="cDstart">
		<label for="cDend">end:</label>
		<input type="date" name="cDend" id="cDend">
      </li>
     </ul>
     </li>
    </ul>
    <ul data-role="listview" data-inset="true" data-shadow="false">
     <li data-role="collapsible" data-iconpos="right" data-inset="false">
     <h2>Status</h2>
     <ul data-role="listview" data-theme="a">
      <li>
		<label for="cgroups">Contract group:</label>
		<select name="cgroups" id="cgroups" data-native-menu="false">
<?php
//$q="SELECT theme FROM poicategories WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
////echo $q."<br>";
//$res = pg_query($cyf, $q);
//if ($res) {
//    $r = array();
//    while (($r=pg_fetch_assoc ( $res ))){
//		if(!empty($_REQUEST['theme']) and $_REQUEST['theme']===$r['theme'])
//	        echo "<option value='{$r['theme']}' selected>{$r['theme']}</option>";
//	  else
//            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
//            //echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
//    }
//}
?>
		</select>
    
		<label for="cContractgroups">Contract group:</label>
		<input type="text" name="cContractgroups" id="cContractgroups" placeholder="{group,group, ... ,group}">
		<label for="cUsersgroups">User groups:</label>
		<input type="text" name="cUsersgroups" id="cUsersgroups" placeholder="{group,group, ... ,group}">
		<label for="cApps">Apps:</label>
		<input type="text" name="cApps" id="cApps" placeholder="{app,app, ... ,app}">
    <div class="ui-field-contain">
	 <label for="cMaxusers">Max users:</label>
	 <input type="text" name="cMaxusers" id="cMaxusers" placeholder="Max users">
     <label for="cStatus">Status:</label>
     <input disabled="disabled" type="text" name="cStatus" id="cStatus" placeholder="active" value="active">
     <label for="cEmail">email:</label>
     <input type="text" name="cEmail" id="cEmail" placeholder="email ...">
     <label for="cPassword">password:</label>
     <input type="password" name="cPassword" id="cPassword">
    </div>
     <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dcontract"><center>Create contract manager</center></a>
      </li>
     </ul>
     </li>
    </ul>
    </li>

     <li>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dcontract"><center>Delete contract</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="ccontract"><center>Change contract</center></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="ncontract"><center>Create contract</center></a>
    </li>
</ul>
</div>
    
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href="#vcontract" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="vuser">Refresh contracts</a>
</div>
</div>
</body>
</html>
