<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - POIs';
//$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
$lang= $param['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
     
<script>
var myLocation = {lat: 60.0,lng: 30.0};
var myLocationMarker;
var myInfoWindow;
var selectedplace= {};
var Gmap;
var myKey;
var myRadius = 500;
var infoWindow;
var geocoder;
var directionsDisplay;
var directionsService;

var markers=[];
// Sets the map on all markers in the array.
function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
};
// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
};
// Shows any markers currently in the array.
function showMarkers() {
        setMapOnAll(Gmap);
};
// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
};
var Placeservice;
var places;
function getMapApi(callback) {
    var p= $.param( {key: "web"} );
    $.ajax({url: "../cis/cis_apis.php",data: p,
            success: function(msg){
//            alert(msg);
			if(msg.length>0){
                var pg =   JSON.parse(msg);
                if(pg.key!=""){
                    myKey = pg.key;
					var src ='https://maps.googleapis.com/maps/api/js?key=';
					src += pg.key + "&libraries=places&callback="+callback;
					var script = document.createElement('script');
					script.src = src;
					script.async = false;
					document.head.appendChild(script);
				}
			}
			}
		   });
};
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
}
function getLocation(idMap) {//<div id="Map"></div> getLocation('Map');
    setMap(idMap,myLocation,12);
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            myLocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
            infoWindow.setPosition(myLocation);
            infoWindow.setContent('Location found.');
            Gmap.setCenter(myLocation);
    myLocationMarker = new google.maps.Marker({ //set marker
        position: myLocation,
        map: Gmap
    });

    google.maps.event.addListener(myLocationMarker, 'click', function() {
       selectedplace.name = 'You location.';
       selectedplace.geometry.location = myLocation;
       infoWindow.setContent('You location.');
       infoWindow.open(Gmap, this);
    });
       }, function() {
            handleLocationError(true, infoWindow, Gmap.getCenter());
        });
    } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, Gmap.getCenter());
    }

};
function setMap(idMap,pos,Zoom) {
	Gmap = new google.maps.Map(document.getElementById(idMap), {
      center: pos,//{lat: pos.lat, lng: pos.lng},
	  zoom: Zoom
	});
    Gmap.setCenter(pos);
    infoWindow = new google.maps.InfoWindow({map: Gmap});
    geocoder = new google.maps.Geocoder();
    Placeservice = new google.maps.places.PlacesService(Gmap);
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(Gmap);

};
function calcRoute(dest) {
  var request = {
    origin: myLocation,//start,
    destination: dest,//end,
    travelMode: 'WALKING'
  };
  directionsService.route(request, function(result, status) {
    if (status == 'OK') {
      directionsDisplay.setDirections(result);
    }
  });
}
function setInfoWindow(pos,txt) {
	infoWindow = new google.maps.InfoWindow({map: Gmap});
	infoWindow.setPosition(pos);
    infoWindow.setContent(txt);
    Gmap.setCenter(pos);
};
function Callback() {
    getLocation('Map');
};
$(document).ready(
 function(){
      getMapApi("Callback");
   var p= $.param( {opt: 'uselect',eaction:'Gpoicategory'});
   $("#Mnecategories").load("../cyf/cyf_poicategory.php", p);
});
$( document ).on('change',"select#Moptions", function(){
    var t = $("select#Moptions").val();
    $("select#Moptions").val('');
    $("select#Moptions").selectmenu( "refresh" );
	switch(t){
	case 'asearch':
		$.mobile.changePage( "#AddrSearch");
		break;
	case 'nsearch':
		$.mobile.changePage( "#NearbySearch");
		break;
	case 'csearch':
        deleteMarkers();
        directionsDisplay.set('directions', null);
		break;
	case 'rsearch':
        directionsDisplay.set('directions', null);
        setMapOnAll(Gmap);
		break;
	case 'addfav':
        var request = {placeId: selectedplace.place_id};
        Placeservice.getDetails(request, pGetDetails);
		$.mobile.changePage( "#viewpoi");
		break;
	case 'gfav':
        var p= $.param({
          opt:'li',filters:'wotime', //fcategs:'{}',
		  fcategs: '{'+($("select#Mfecategories").val()!=null ? $("select#Mfecategories").val().join():"")+'}',
          lt: $('#xlt').val(),
          ln: $('#xln').val(),
          eaction:'Vpoi'
          });
        $("#poilist").load("../cyf/cyf_poi.php", p);
		$.mobile.changePage( "#Poilist");
//		$("#poilist").html($("#poilist").html()+"&lt;li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'&gt;&lt;/li&gt;&lt;li&gt;&lt;a href='#newpoi' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Mpid'data-user-id='0'&gt;&lt;h6&gt;Create new POI...&lt;/h6&gt;&lt;/a&gt;&lt;/li&gt;");
		break;
	case 'gpath':
        var dest = selectedplace.geometry.location;
        clearMarkers();
        calcRoute(dest);
		break;
	case 'myloc':
        clearMarkers();
        directionsDisplay.set('directions', null);
        Gmap.setCenter(myLocation);
	default:
	}
});

function pgetDetails(place, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    selectedplace = place;
  }
}
function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: Gmap,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infoWindow.setContent(place.name);
          infoWindow.open(Gmap, this);

          selectedplace = place;

//          var request = {placeId: place.place_id};
//          Placeservice.getDetails(request, pgetDetails);
        });
        markers.push(marker);
};
$( document ).on( 'vclick', "a#asr", function() {
    var address = $("#addrsearch").val();
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == 'OK') {
            createMarker(results[0]);
            Gmap.setCenter(results[0].geometry.location);
        }
    });
	return true;
});

function nearbyCallback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
      places = results;
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
};
$( document ).on( 'vclick', "a#nsr", function() {
	//alert($("#radius").val());
    var p= {
      location : myLocation,
      radius : $("#radius").val(),//50000, //m
//		language: language,
//	type : type, //Restricts the results to places matching the specified type. Only one type may be specified 
        keyword : $("#nearbysearch").val() //name, type, and address, as well as customer reviews and other third-party content.
	};
            Placeservice.nearbySearch(p, nearbyCallback);
	return true;
});
function pGetDetails(place, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    selectedplace = place;
    $("#Mpoiname").val(selectedplace.name);
    $("#Mdescr").val(selectedplace.name);
    $("#Murl").val(selectedplace.website);
    $("#Mlocation").val(selectedplace.vicinity);
    $("#Mlongt").val(""+selectedplace.geometry.location.lng());
    $("#Mlat").val(""+selectedplace.geometry.location.lat());
    $("#Mtel").val(selectedplace.international_phone_number);
    var Maday ="";
    var del = "";
    for (var i = 0, weekday; weekday = selectedplace.opening_hours.weekday_text[i]; i++) {
        Maday += del + weekday;
        del =',';
    }
    $("#Maday").val(Maday);
  }
}
$( document ).on( 'vclick', "a#favp", function() {//Add to favorites
          var request = {placeId: selectedplace.place_id};
          Placeservice.getDetails(request, pGetDetails);
	return true;
});
$( document ).on( 'vclick', "a#getp", function() {//Get path
    var dest = selectedplace.geometry.location;
    clearMarkers();
    calcRoute(dest);
	return false;
});
$( document ).on( 'vclick', "a#clearp", function() {//Clear
    deleteMarkers();
    directionsDisplay.set('directions', null);
	return false;
});
$( document ).on( 'vclick', "a#backp", function() {//Back
    directionsDisplay.set('directions', null);
    setMapOnAll(Gmap);
	return false;
});
function setSelected(){
//    selectedplace.name = $("#Mdescr").val();
    selectedplace = {name : $("#Mpoiname").val(), vicinity : $("#Mlocation").val(), website : $("#Murl").val(), international_phone_number : $("#Mtel").val(), geometry: {location : {lat: parseFloat($("#Mlat").val()), lng: parseFloat($("#Mlongt").val())}}};
};
$( document ).on( "vclick", "a#spoi", function() {
   clearMarkers();
   directionsDisplay.set('directions', null);
   setSelected();
   createMarker(selectedplace);
   Gmap.setCenter(selectedplace.geometry.location);
   return true;
});
$( document ).on( "vclick", "a#ppoi", function() {
    setSelected();
    var dest = selectedplace.geometry.location;
    clearMarkers();
    calcRoute(dest);
    return true;
});
//$( document ).on( "vclick", "a#dpoi", function() {
//	alert($('#Mnid').val());
//	alert($('select#Mauthor').val());
// var p= $.param( {
//        opt: 'li',
//        lt: $('#xlt').val(),
//        ln: $('#xln').val(),
//        filters: $("select#Mfilters").val(),
//        fcategs: '{'+($("select#Mfecategories").val()!=null ? $("select#Mfecategories").val().join():"")+'}',
//        eaction: 'Dpoi',
//        author:  $('select#Mauthor').val(),
//        eid: $('#Mnid').val()
//    });
//    $("#pois").load("../cyf/cyf_poi.php", p);
//    return true;
//});
//$( document ).on( "vclick", "a#cpoi", function() {
//	alert($('#Mnid').val());
//	alert($('select#Mauthor').val());
//	alert($('select#Mdescr').val());
//    var p= $.param( {
//        opt: 'li',
//        eaction: 'Cpoi',
//        lt: $('#xlt').val(),
//        ln: $('#xln').val(),
//        filters: $("select#Mfilters").val(),
//        fcategs: '{'+($("select#Mfecategories").val()!=null ? $("select#Mfecategories").val().join():"")+'}',
//        eid:     $('#Mnid').val(),
//        author:  $('select#Mauthor').val(),
//        agroups: '{'+($("select#Magroups").val()!=null ? $("select#Magroups").val().join() :"")+'}',
//        poiname: $('#Mpoiname').val(),
//        descr:   $('#Mdescr').val(),
//        url:     $('#Murl').val(),
//        aday:     $('#Maday').val(),
//        opentime:   $('#Mopentime').val(),
//       closetime:   $('#Mclosetime').val(),
//        locat:   $('#Mlocation').val(),
//        longt:   $('#Mlongt').val(),
//        lat:     $('#Mlat').val(),
//        contact: $('#Mcontact').val(),
//        tel:     $('#Mtel').val(),
//        email:   $('#Memail').val(),
//        categs: '{'+($("select#Mnecategories").val()!=null ? $("select#Mnecategories").val().join() : '')+'}'
//    });
//    $("#pois").load("../cyf/cyf_poi.php", p);
//    return true;
//});
function deletePOI (){
 var p= $.param( {
        opt: 'noselect',
//        lt: $('#xlt').val(),
//        ln: $('#xln').val(),
//        filters: $("select#Mfilters").val(),
//        fcategs: '{'+($("select#Mfecategories").val()!=null ? $("select#Mfecategories").val().join():"")+'}',
        eaction: 'Dpoi',
        author:  $('select#Mauthor').val(),
        eid: $('#Mnid').val()
    });
	$.ajax({url:"../cyf/cyf_poi.php",data: p,
            dataType:"json",
            success: function(msg){
//            alert(msg);
//            if(msg.status =="OK"){
//                $('#poistatus').text(msg.status);
//            }
        }
            });
}
function changePOI (){
	    var p= $.param( {
        opt: 'noselect',
        eaction: 'Cpoi',
        lt: $('#xlt').val(),
        ln: $('#xln').val(),
        filters: $("select#Mfilters").val(),
        fcategs: '{'+($("select#Mfecategories").val()!=null ? $("select#Mfecategories").val().join():"")+'}',
        eid:     $('#Mnid').val(),
        author:  $('select#Mauthor').val(),
        agroups: '{'+($("select#Magroups").val()!=null ? $("select#Magroups").val().join() :"")+'}',
        poiname: $('#Mpoiname').val(),
        descr:   $('#Mdescr').val(),
        url:     $('#Murl').val(),
        aday:     $('#Maday').val(),
        opentime:   $('#Mopentime').val(),
        closetime:   $('#Mclosetime').val(),
        locat:   $('#Mlocation').val(),
        longt:   $('#Mlongt').val(),
        lat:     $('#Mlat').val(),
        contact: $('#Mcontact').val(),
        tel:     $('#Mtel').val(),
        email:   $('#Memail').val(),
        categs: '{'+($("select#Mnecategories").val()!=null ? $("select#Mnecategories").val().join() : '')+'}'
    });
	$.ajax({url:"../cyf/cyf_poi.php",data: p,
            dataType:"json",
            success: function(msg){
//            alert(msg);
//            if(msg.status =="OK"){
//                $('#poistatus').text(msg.status);
//            }
        }
            });
}
function insertPOI(){
    var p= $.param( {
            opt: 'noselect',
            eaction: 'Npoi',
            author:  $('select#Mauthor').val(),
            agroups: '{'+($("select#Magroups").val()!=null ? $("select#Magroups").val().join() :"")+'}',
            poiname: $('#Mpoiname').val(),
            descr:   $('#Mdescr').val(),
            url:     $('#Murl').val(),
            aday:    $('#Maday').val(),
            opentime:   $('#Mopentime').val(),
            closetime:  $('#Mclosetime').val(),
            locat:   $('#Mlocation').val(),
            longt:   $('#Mlongt').val(),
            lat:     $('#Mlat').val(),
            contact: $('#Mcontact').val(),
            tel:     $('#Mtel').val(),
            email:   $('#Memail').val(),
            categs: '{'+($("#Mnecategories").val()!=null ? $("#Mnecategories").val().join() : '')+'}'
    });
    $.ajax({url:"../cyf/cyf_poi.php",data: p,
            dataType:"json",
            success: function(msg){
//            alert(msg);
//            if(msg.status =="OK"){
//                $('#poistatus').text(msg.status);
//            }
        }
            });
};
$( document ).on( "vclick", "a#dpoi", function() {
	deletePOI ();
	return true;
});	
$( document ).on( "vclick", "a#cpoi", function() {
	changePOI ();
	return true;
});	
$( document ).on( "vclick", "a#npoi", function() {
    insertPOI();
    return true;
});
$( document ).on( 'vclick', "a#Mpid", function() {
    $('#Mnid').val($(this).data('userId'));
    if($(this).data('userId')!=0){
            var p= $.param( {id: $(this).data('userId'),eaction: 'Jpoi'});
     $.ajax({url: "../cyf/cyf_poi.php",data: p,
             // complete:
            success: function(msg){
			if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#Mnid').val(pg.id);
                $("select#Mauthor").val(pg.author);
//                $('select#Mauthor').selectmenu().selectmenu('refresh',true);  
                $("select#Magroups").val(pg.sgroups);
//                $('select#Magroups').selectmenu().selectmenu('refresh',true);  
                $('select#Mnecategories').val(pg.categs);
//                $('select#Mnecategories').selectmenu().selectmenu('refresh',true);
                $('#Mpoiname').val(pg.poiname);
                $('#Mdescr').val(pg.descr);
                $('#Murl').val(pg.url);
                $('#Mlocation').val(pg.locat);
                $('#Mlongt').val(pg.longt);
                $('#Mlat').val(pg.lat);
                $('#Maday').val(pg.aday);
                $('#Mstime').val(pg.opentime);
                $('#Metime').val(pg.closetime);
                $('#Mcontact').val(pg.contact);
                $('#Mtel').val(pg.tel);
                $('#Memail').val(pg.email);
                $('#poititle').text(pg.poiname);
                $('#Mpoititle').text(pg.poiname);
            }
         }
    });
    }
    return true;
});
	
	$( document ).on( "vclick", "a#getcoorpoi", function() {
//		    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': $("#Mlocation").val()}, function(results, status){
	        if (status == 'OK') {
//			alert(results[0].geometry.location.lat);
//			alert(results[0].geometry.location.lng);
            $("#Mlat").val(results[0].geometry.location.lat);
			$("#Mlongt").val(results[0].geometry.location.lng);
        }
	});
	});

</script>
</head>
<body>
<!-- cyf_pois.html-->

<div data-role="page" id="Pois" data-position="fixed"> <!-- page -->
	<div data-role="header" data-position="fixed">
	  <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
  <div data-role="navbar">
	<ul>
	  <li>
		<select class="Coptions" id="Moptions" data-native-menu="false">
		  <option data-placeholder="true">Mode</option>
          <option value="myloc">My location</option>
          <option value="gfav">Favorites</option>
          <option value="nsearch">Nearby Search place</option>
          <option value="asearch">Address location</option>
          <option value="csearch">Clear search result</option>
          <option value="addfav">Add to favorites</option>
          <option value="rsearch">Restore search result</option>
          <option value="gpath">Get path</option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Mfecategories[]" id="Mfecategories" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Categories</option>
<!--      <option value="1">Get categories</option> -->
<?php
$q="SELECT theme FROM poicategories WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
//echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
		if(!empty($_REQUEST['theme']) and $_REQUEST['theme']===$r['theme'])
	        echo "<option value='{$r['theme']}' selected>{$r['theme']}</option>";
	  else
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
            //echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
?>
		</select>
	  </li>
    </ul>
  </div>
</div>

<div data-role="main" class="ui-content">
    <div id="Map" style="height: 400px;width: 100%;"></div>
</div> 
</div>

  <input type='hidden' name='xlt' id='xlt' value='0'>
  <input type='hidden' name='xln' id='xln' value='0'>

<div data-role="page" data-dialog="true" data-mini="true" id="Poilist">
  <div data-role="header" data-position="fixed">
    <h2>Favorites</h2>
  </div>

  <div data-role="main" class="ui-content">
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="poilist">
          <li>empty</li>
        </ul>
	<ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview">
	<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>
<li><a href='#newpoi' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Mpid'
 data-user-id='0'>
<h6>Create new POI ..</h6>
</a></li>	
	</ul>
  </div> 
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="newpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="poititle">POI</h2>
  </div>
  <div data-role="main" class="ui-content">
	<ul data-role="listview" data-theme="a">
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="spoi">Show on map</a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="ppoi">Show path</a></li>
    <li><a href="#viewpoi" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="epoi">Edit POI</a></li>
    </ul>
 </div>
</div> <!-- page data-dialog="true" -->

<input type='hidden' name='Mnid' id='Mnid'>

  <div data-role="page" data-dialog="true" data-mini="true" id="viewpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="Mpoititle">New POI</h2>
  </div>
  <div data-role="main" class="ui-content">
	<ul data-role="listview">
	  <li >
		<label for="Mpoiname">POI name:</label>
        <input type="text" name="Mpoiname" id="Mpoiname" placeholder="POI name ...">
		<label for="Mdescr">POI description:</label>
		<textarea name="Mdescr" id="Mdescr" placeholder="POI description ..."></textarea>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>date, time</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
              <label for="Maday">available:</label><input type="text" name="Maday" id="Maday">
			  <div class="ui-block-a"><label for="Mopentime">open:</label>
                <input data-mini="true" type="time" name="Mopentime" id="Mopentime"></div>
			  <div class="ui-block-b"><label for="Mclosetime">close:</label>
                <input data-mini="true" type="time" name="Mclosetime" id="Mclosetime"></div>
              <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>location</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Murl">URL:</label>
			<input type="url" name="Murl" id="Murl">
			<label for="Mlocation">Address:</label>
			<textarea name="Mlocation" id="Mlocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorpoi">Get coordinates</a></p>
			<label for="Mlongt">Longitude:</label>
			<input type="number" step="any" name="Mlongt" id="Mlongt">
			<label for="Mlat">Latitude:</label>
			<input type="number" step="any" name="Mlat" id="Mlat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>contact</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Mcontact">contact:</label>
			<input type="text" name="Mcontact" id="Mcontact" placeholder="Name ...">
			<label for="Mtel">tel:</label>
			<input type="tel" name="Mtel" id="Mtel" placeholder="tel ...">
			<label for="Memail">email:</label>
			<input type="email" name="Memail" id="Memail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
<!--	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Categories</h2> -->
	<li >
<div class="ui-field-contain">
		<label for="Mnecategories">Categories:</label>
		<select multiple="multiple" data-native-menu="false" name="Mnecategories[]" id="Mnecategories" data-mini="true" data-inset="true">
<!--		  <option data-placeholder="true">Categories</option>-->
		</select>
</div> 
<!--	</li>
	<li >-->
<div class="ui-field-contain">
		<label for="Mauthor">Moderator:</label>
		<select name="Mauthor" id="Mauthor" data-native-menu="false" data-mini="true" data-inset="true">
<!--     <option data-placeholder="true">Moderator</option>-->
<?php
    echo "<option value='{$param['email']}'";
    echo ">{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
<div class="ui-field-contain">
		<label for="Magroups">for groups:</label>
		<select name="Magroups[]" id="Magroups" multiple="multiple" data-native-menu="false" data-mini="true" data-inset="true">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
	</li>
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing POI ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="upoi">Publish POI</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoi">Delete POI</a>
	</li>
     
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For private group &quot;<?php echo $param['email'];?>&quot; POI only ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoi">Change POI</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoi">Save new POI</a>
	</li>
    </ul>
  </div>
</div> <!-- page data-dialog="true" -->
<!-- --------------- -->
<div data-role="page" data-dialog="true" data-mini="true" id="AddrSearch">
  <div data-role="header" data-position="fixed">
    <h2>Address location</h2>
  </div>
<div data-role="main" class="ui-content">
<label for="addrsearch">Address location Requests:</label>
<input type="search" id="addrsearch" placeholder="Enter a address: 275-291 Bedford Ave, Brooklyn, NY 11211, USA" autocomplete="off" value="">
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="asr" >Search</a>
  </div>
</div>
<!-- --------------- -->

<div data-role="page" data-dialog="true" data-mini="true" id="NearbySearch">
  <div data-role="header" data-position="fixed">
    <h2>Nearby Search Requests</h2>
  </div>
                                                                        
<div data-role="main" class="ui-content">
<label for="nearbysearch">Nearby Search Requests:</label>
<input type="search" id="nearbysearch" placeholder="Enter a query" autocomplete="off" value="">
		<select name="radius" id="radius" data-native-menu="false">
		  <option data-placeholder="true">Radius</option>
          <option value="500" selected>radius &lt;0.5 km</option>
          <option value="1000">radius &lt;1 km</option>
          <option value="3000">radius &lt;3 km</option>
          <option value="50000">radius &gt;3 km</option>
		</select>
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsr" >Search</a>
  </div>
</div>

<div data-role="page" data-mini="true" id="poimap"> <!-- page -->
    <div data-role="main" class="ui-content">
       <div id="map" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Back</a>
    </div>
</div> <!-- page -->

<div data-role="page" data-mini="true" id="poipath"> <!-- page -->
    <div data-role="main" class="ui-content">
       <div id="pmap" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="poipathback">Back</a>
    </div>
</div> <!-- page -->

<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="main" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Lang: <?php echo $param['lang']; ?></li>
    <li>Groups member:<br><?php echo $param['membergroups']; ?></li>
    <li>Groups owner:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
  </div>
</div>

</body>
</html>
