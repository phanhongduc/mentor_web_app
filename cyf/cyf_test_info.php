<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

if ($param['email'] == 'guest') header("Location: https://www.google.com");
$cyf = cyfConnect();
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Contracts';
$lang= $param['lang'];

?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>

$( document ).on( "vclick", "a#Vgroups", function() {
	var p= {eaction: 'Vgroups',opt:'li'};
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                $('#grouplist').empty().append(msg);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#OVgroups", function() {
	var p= {eaction: 'Vgroups',opt:'li',
            owner:   'tu@gmail.com',
            ownergroups: '{Turku University}',
            member: 'yes', 
            membergroups: '{Turku Municipality}'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                $('#grouplist').empty().append(msg);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#Sgroups", function() {
	var p= {eaction: 'Vgroups',opt:'select'};
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                $('select#sgroups').empty().append('<option data-placeholder="true">Choose group</option>');
                $('select#sgroups').append(msg);
                $('select#sgroups').selectmenu().selectmenu('refresh',true);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#OSgroups", function() {
	var p= {eaction: 'Vgroups',opt:'select',
            owner:   'tu@gmail.com', 
            ownergroups: '{Turku University}',
            member: 'yes'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                $('select#sgroups').empty().append('<option data-placeholder="true">Choose group</option>');
                $('select#sgroups').append(msg);
                $('select#sgroups').selectmenu().selectmenu('refresh',true);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#JSgroups", function() {
	var p= {eaction: 'Vgroups'};
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('select#sgroups').empty().append('<option data-placeholder="true">Choose group</option>');
                for(i=0;i<pg.length;i++){
                    $('select#sgroups').append("<option value='"+pg[i].gname+"'>"+pg[i].gname+"</option>");
                }
                $('select#sgroups').selectmenu().selectmenu('refresh',true);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#OJSgroups", function() {
	var p= {eaction: 'Vgroups',
            ownergroups: '{"Turku University","CYF Manager"}'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('select#sgroups').empty().append('<option data-placeholder="true">Choose group</option>');
                for(i=0;i<pg.length;i++){
                    $('select#sgroups').append("<option value='"+pg[i].gname+"'>"+pg[i].gname+"</option>");
                }
                $('select#sgroups').selectmenu().selectmenu('refresh',true);
            }
        }});
    return true;
});

$( document ).on( "vclick", "a#Vpoicats", function() {
	var p= {eaction: 'Vpoicategories',opt:'li',
//            owner:   'tu@gmail.com', 
            publish: 'yes',
            publishgroups: '{Turku University}',
            allow: 'yes',
            allowgroups: '{Turku University Manager}',
            member: 'yes'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                $('#grouplist').empty().append(msg);
            }
        }});
    return true;
});
$( document ).on( "vclick", "a#MVpoicats", function() {
	var p= {eaction: 'Vpoicategories',
//            owner:   'tu@gmail.com', 
            publish: 'yes',
            publishgroups: '{Turku University}',
            allow: 'yes',
            allowgroups: '{Turku University Manager}',
            member: 'yes'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                var h='';
                $('#grouplist').empty();
                if(pg[0].id !=0){
               for(i=0;i<pg.length;i++){
                    h+="<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>"+pg[i].theme+"</strong> ("+pg[i].owner+")</li>";
                    h+="<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'";
                    h+=" data-user-id='"+pg[i].id+"'>";
                    h+="<h3><strong>"+pg[i].theme+"</strong></h3>";
                    h+="<p><strong>Cross reference themes: ";
                    var j=0;
                    var d='';
                    while (pg[i].subthemes[j]) {
                        h+=d+pg[i].subthemes[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Publishing groups: ";
                    j=0;
                    d='';
                    while (pg[i].publishgroups[j]) {
                        h+=d+pg[i].publishgroups[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Allow groups: ";
                    j=0;
                    d='';
                    while (pg[i].allowgroups[j]) {
                        h+=d+pg[i].allowgroups[j]; d=','; j++;
                    }
                    h+="</strong></p></a></li>";
                        
                }
                }
            }
            $('#grouplist').empty().append(h);
        }});
    return true;
});

$( document ).on( "vclick", "a#MVsubscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
//            owner:   'tu@gmail.com', 
            publish: 'yes',
            publishgroups: '{Turku University}',
            allow: 'yes',
            allowgroups: '{Turku University Manager}',
            member: 'yes'
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
//            console.log(msg);
                var h='';
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#grouplist').empty();
                if(pg[0].id !=0){
                for(i=0;i<pg.length;i++){
                    h+="<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>"+pg[i].theme+"</strong> ("+pg[i].owner+")</li>";
                    h+="<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'";
                    h+=" data-user-id='"+pg[i].id+"'>";
                    h+="<h3><strong>"+pg[i].theme+"</strong></h3>";
                    h+="<p><strong>Cross reference themes: ";
                    var j=0;
                    var d='';
                    while (pg[i].subthemes[j]) {
                        h+=d+pg[i].subthemes[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Publishing groups: ";
                    j=0;
                    d='';
                    while (pg[i].publishgroups[j]) {
                        h+=d+pg[i].publishgroups[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Allow groups: ";
                    j=0;
                    d='';
                    while (pg[i].allowgroups[j]) {
                        h+=d+pg[i].allowgroups[j]; d=','; j++;
                    }
                    h+="</strong></p></a></li>";
                        
                }
                }
                    $('#grouplist').empty().append(h);
            }
        }});
    return true;
});
function printPOIcateg(p){
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            var h='';
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                $('#grouplist').empty();
                if(pg[0].id !=0){
                    for(i=0;i<pg.length;i++){
                        h+="<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>"+pg[i].theme+"</strong> ("+pg[i].owner+")</li>";
                        h+="<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'";
                        h+=" data-user-id='"+pg[i].id+"'>";
                        h+="<h3><strong>"+pg[i].theme+"</strong></h3>";
                        h+="<p><strong>Cross reference themes: ";

                        var d='';
                        for(j=0;j<pg[i].subthemes.length;j++){
                            h+=d+pg[i].subthemes[j]; d=',';
                        }
                        h+="</strong></p>";

                        h+="<p><strong>Publishing groups: ";

                        d='';
                        for(j=0;j<pg[i].publishgroups.length;j++){
                            h+=d+pg[i].publishgroups[j]; d=',';
                        }
                        h+="</strong></p>";

                        h+="<p><strong>Allow groups: ";

                        d='';
                        for(j=0;j<pg[i].allowgroups.length;j++){
                            h+=d+pg[i].allowgroups[j]; d=',';
                        }
                        h+="</strong></p></a></li>";
                        
                    }
                }
            }
            $('#grouplist').empty().append(h);
        }});
};
//owner:"",allow:""[,allowgroups:""]
$( document ).on( "vclick", "a#MVsubscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            allow: "yes",
            member: 'yes'
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#TVsubscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            owner:   'Turku University Manager',
            allow: "yes"
//            publish: "yes", //publishgroups && '{$owner}'",
//            publishgroups: '{Turku University}',
//            allow: "yes", //allowgroups && '{$owner}'",
//            allowgroups: '{Turku University}',
//            member: 'yes'
            //publish publishgroups && '{$membergroups}'
            //'allow allowgroups && '{$membergroups}'
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T1subscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            publish: "yes",
            member: 'yes'
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T2subscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            publish: "yes"
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T3subscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            publishgroups: '{Turku University}',
            publish: "yes"
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T4subscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            owner:   'Turku University Manager',
            member: 'yes'
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T5subscriptions", function() {
	var p= {eaction: 'Vsubscriptions',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    printPOIcateg(p);
    return true;
});
$( document ).on( "vclick", "a#T6subscriptions", function() {
	var p= {eaction: 'Psubscriptions',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            var h='';
            if(msg.length>0){
                $('#grouplist').empty().append("<li>"+msg+"</li>");
            }
        }});
    return true;
});



</script>
</head>
<body>
<div data-role="page" id="pblank" data-position="fixed">
<div data-role="header" data-position="fixed">
    <h2 id="pblanktitle">Groups</h2>
</div>

<div data-role="content" class="ui-content">
<h3></h3>
 <ul data-role="listview" id="cmdlist">
    <li>empty</li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="Vgroups">
    Get group list as li</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="OVgroups">
    Get group list as li by email or owner</a></li>
 <li><a href='#sgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="Sgroups">
    Get group list as select</a></li>
 <li><a href='#sgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="OSgroups">
    Get group list as select by email or owner</a></li>
 <li><a href='#sgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="JSgroups">
    Get group list as json to select</a></li>
 <li><a href='#sgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="OJSgroups">
    Get group list as json to select by email tu@gmail.com or owner {"Turku University","CYF Manager"}</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="Vpoicats">
    Get category list as li by member,publish,publishgroups,allow,allowgroups</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="MVpoicats">
    Get category list as li by owner or owner</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="MVsubscriptions">
    Get subscriptions list by allow: "yes" member: 'yes'</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T1subscriptions">
    Get subscriptions list publish: "yes", member: 'yes'</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T2subscriptions">
    Get subscriptions list by publish: "yes"</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T3subscriptions">
    Get subscriptions list by publishgroups: '{Turku University}', publish: "yes"</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T4subscriptions">
    Get subscriptions list by owner:   'Turku University Manager', member: 'yes'</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T5subscriptions">
    Get subscriptions list by owner: 'Turku University Manager'</a></li>
 <li><a href='#pgrouplist' class='mybtn ui-btn ui-icon-bullets ui-btn-icon-right' id="T6subscriptions">
    Get subscriptions list by owner: 'Turku University Manager'</a></li>
</ul>
</div>

    
<div data-role="footer" class="ui-bar" style="text-align:center;">
   <div id="State"></div>
</div>
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="pgrouplist"> <!-- page -->
<div data-role="header" data-position="fixed">
    <h2 id="pgrouplisttitle">Groups</h2>
</div>

<div data-role="content" class="ui-content">
<h3></h3>
 <ul data-role="listview" id="grouplist">
    <li>empty</li>
 </ul>
</div>

<div data-role="footer" class="ui-bar" style="text-align:center;">
<a href='#' class='mybtn ui-btn ui-icon-back ui-btn-icon-right ui-btn-icon-notext' data-rel="back">Back</a>
</div>
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="sgrouplist"> <!-- page -->
<div data-role="header" data-position="fixed">
    <h2 id="sgrouplisttitle">Groups</h2>
</div>

<div data-role="content" class="ui-content">
<h3></h3>
 <ul data-role="listview" id="grouplist">
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Groups</h2>
		<select multiple="multiple" data-native-menu="false" name="sgroups[]" id="sgroups" data-mini="true">
		  <option data-placeholder="true">Choose group</option>
		</select>
	  </li>
 </ul>
</div>

<div data-role="footer" class="ui-bar" style="text-align:center;">
<a href='#' class='mybtn ui-btn ui-icon-back ui-btn-icon-right ui-btn-icon-notext' data-rel="back">Back</a>
</div>
</div>

    
</body>
</html>
