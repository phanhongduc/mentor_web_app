function loadSubscriptions(s,t){
	//loadSubscriptions('select#Enecategories','Choose subscriptions'){
	var p= {eaction: 'Vsubscriptions',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                var h ='<option data-placeholder="true">'+t+'</option>';
//                var h ='';
                if(pg[0].id !=0){
                    for(i=0;i<pg.length;i++){
						h+="<option value='"+pg[i].theme+"'>"+pg[i].theme+"</option>";
                    }
					$(s).empty().append(h);
//					$(s).selectmenu().selectmenu('refresh',true);
               }
            }
			}});
    return true;
};
function loadSubscriptionsList(s){
	//loadSubscriptionsList("#subscriptions"){
	var p= {eaction: 'Vsubscriptions',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
//            console.log(msg);
                var h='';
            if(msg.length>0){
                var pg =   JSON.parse(msg);
//                $('#grouplist').empty();
                if(pg[0].id !=0){
                for(i=0;i<pg.length;i++){
                    h+="<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>"+pg[i].theme+"</strong> ("+pg[i].owner+")</li>";
                    h+="<li><a href='#subscription' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Said'";
                    h+=" data-user-id='"+pg[i].id+"'>";
                    h+="<h3><strong>"+pg[i].theme+"</strong></h3>";
                    h+="<p><strong>Cross reference themes: ";
                    var j=0;
                    var d='';
                    while (pg[i].subthemes[j]) {
                        h+=d+pg[i].subthemes[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Publishing groups: ";
                    j=0;
                    d='';
                    while (pg[i].publishgroups[j]) {
                        h+=d+pg[i].publishgroups[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Allow groups: ";
                    j=0;
                    d='';
                    while (pg[i].allowgroups[j]) {
                        h+=d+pg[i].allowgroups[j]; d=','; j++;
                    }
                    h+="</strong></p></a></li>";
                        
                }
                }
                    $(s).empty().append(h);
            }
        }});
    return true;
};

function loadPOIcategories(s,t){
	//loadSubscriptions('select#Enecategories','Choose subscriptions'){
	var p= {eaction: 'Vpoicategories',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
            if(msg.length>0){
                var pg =   JSON.parse(msg);
                var h ='<option data-placeholder="true">'+t+'</option>';
//                var h ='';
                if(pg[0].id !=0){
                    for(i=0;i<pg.length;i++){
						h+="<option value='"+pg[i].theme+"'>"+pg[i].theme+"</option>";
                    }
					$(s).empty().append(h);
//					$(s).selectmenu().selectmenu('refresh',true);
               }
            }
			}});
    return true;
};
function loadPOIcategoriesList(s){
	//loadPOIcategoriesList("#poicategories"){
	var p= {eaction: 'Vsubscriptions',
            owner:   '', //owner='{$param['email']}'";
            publish: 'yes', //publishgroups && '{{$param['email']}}'";
            publishgroups: '', //publishgroups && '{$param['ownergroups']}'";
            allow: 'yes',
            allowgroups: '',//allowgroups && '{$param['ownergroups']}'";
            member: 'yes' //allowgroups && '{$param['membergroups']}' OR ARRAY[owner] && '{$param['membergroups']}'";
    };
    $.ajax({url:"../cyf/cyf_info.php",type: "post", data: p,
            success: function(msg){
//            console.log(msg);
                var h='';
            if(msg.length>0){
                var pg =   JSON.parse(msg);
//                $('#grouplist').empty();
                if(pg[0].id !=0){
                for(i=0;i<pg.length;i++){
                    h+="<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>"+pg[i].theme+"</strong> ("+pg[i].owner+")</li>";
                    h+="<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'";
                    h+=" data-user-id='"+pg[i].id+"'>";
                    h+="<h3><strong>"+pg[i].theme+"</strong></h3>";
                    h+="<p><strong>Cross reference themes: ";
                    var j=0;
                    var d='';
                    while (pg[i].subthemes[j]) {
                        h+=d+pg[i].subthemes[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Publishing groups: ";
                    j=0;
                    d='';
                    while (pg[i].publishgroups[j]) {
                        h+=d+pg[i].publishgroups[j]; d=','; j++;
                    }
                    h+="</strong></p>";

                    h+="<p><strong>Allow groups: ";
                    j=0;
                    d='';
                    while (pg[i].allowgroups[j]) {
                        h+=d+pg[i].allowgroups[j]; d=','; j++;
                    }
                    h+="</strong></p></a></li>";
                        
                }
                }
                    $(s).empty().append(h);
            }
        }});
    return true;
};

