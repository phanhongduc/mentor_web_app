import fs from 'fs';

const lang = ['en', 'fi'];

const newSrc = JSON.parse(fs.readFileSync('dumbLocale.json', 'utf8')).en;

lang.forEach((l) => {
  const src = JSON.parse(fs.readFileSync(`api/lang/${l}.json`, 'utf8'));

  for (const key in newSrc) {
    if (!src.hasOwnProperty(key)) {
      console.log(`[${l}] need translate for ${key}. Default is "${newSrc[key]}" .Find *Translate* keyword in build file`);
      src[key] = '*Translate*';
    }
  }

  fs.writeFileSync(`api/lang/${l}.json`, JSON.stringify(src));
});

