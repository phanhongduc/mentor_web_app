<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
legend {font-size:1.2em;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
 document.getElementById('frame').value='groupsform';
 document.getElementById('action').value='../cis/util_groupsform.php';
 if(document.getElementById('ufltrs').checked){
   document.getElementById('fltrs').value='y';
   if(document.getElementById('ifgname').checked)
     document.getElementById('fgname').value=document.getElementById('igname').value;
   if(document.getElementById('ifowner').checked)
     document.getElementById('fowner').value=document.getElementById('iowner').value;
 }
 document.getElementById('groupslist').submit();
}
</script>
</head>
<body style="font-size:12px;" onload="setParam();">
EOT;

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';

//echo "post: ";print_r($_POST);echo "<br>";

$gid = 0;
if(!empty($_REQUEST['gid'])) $gid = $_REQUEST['gid'];
/* $gname = ltrim(rtrim($_REQUEST['gname'],'}" '),'{" '); */
/* $gowner = ltrim(rtrim($_REQUEST['owner'],'}" '),'{" '); */
$gname =  $_REQUEST['gname'];
$gowner = $_REQUEST['owner'];

echo '<form id="f" target="groupsform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

if(!empty($_REQUEST['gid']) and !empty($_REQUEST['pcmd']) and !empty($_REQUEST['tcmd'])){
//self    
    if(isset($_REQUEST['tcmd'])){
        if($_REQUEST['tcmd']=="Insert"){
            $query = "INSERT INTO groups (gname,owner) VALUES (";
            $query .= " '".pg_escape_string($gname)."'";
            $query .= ",'".pg_escape_string($gowner)."'";
            $query .= ");";
//            echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }else if($_REQUEST['tcmd']=="Update"){
                $query = "UPDATE groups SET";
                $query .= " gname='".pg_escape_string($gname)."'";
                $query .= ",owner='".pg_escape_string($gowner)."'";
                $query .= " WHERE id=".pg_escape_string($gid).";";
                echo $query . "<br>";
                $result = @pg_query($conn, $query);
                if (!$result) $last_error = pg_last_error($conn);
        }else if($_REQUEST['tcmd']=="Delete"){
            $query = "DELETE FROM groups WHERE id=".pg_escape_string($gid).";";
//            echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }
    }
}
if(empty($_REQUEST['ufltrs']) and !empty($_REQUEST['gid'])){
//callback
    $q = "SELECT id,gname,owner FROM groups WHERE id={$gid}";
//    echo $q; echo '<br>';
    $result = @pg_query($conn, $q);
    if (!$result) $last_error = pg_last_error($conn);
    else{
        $firows = pg_num_rows($result);
        if($firows >0){
            $fi=getValues($result);
            for ($j=0;$j<$firows;$j++){
                $gid = $fi[$j]['id'];
                $gname = $fi[$j]['gname'];
                $gowner = $fi[$j]['owner'];
            }
        }
    }
}
echo "<input type='hidden' id='currid' name='gid' value='{$gid}'>";

echo "<center>";

echo "<table><caption style='text-align:center;'>Groups/Filters</caption>";

echo "<tr><td>name:</td><td><input type='text' id='igname' name='gname' value='{$gname}'></td>";
echo "<td><input type='checkbox' id='ifgname' name='ifgname'";
if(!empty($_REQUEST['ifgname'])) echo ' checked';
echo ">view with name like this</td></tr>";

echo "<tr><td>owner:</td><td><input type='text' id='iowner' name='owner' value='{$gowner}'></td>";
echo "<td><input type='checkbox' id='ifowner' name='ifowner'";
if(!empty($_REQUEST['ifowner'])) echo ' checked';
echo ">view with owner like this</td></tr>";

echo "</table>";

echo <<<EOT
<table><caption>&nbsp;</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td><input type="checkbox" id="ufltrs" name="ufltrs"
EOT;
if(!empty($_POST['ufltrs'])) echo ' checked';
echo '>use filters</td><td><input type="submit" name="pcmd" value="Submit" style=""></td></tr>';
echo '</table>';

echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
</center>
</form>

<form method="post" action="../cis/util_groupslist.php" target="groupslist" id="groupslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="fltrs" name="fltrs">
<input type="hidden" id="fgname" name="fgname">
<input type="hidden" id="fowner" name="fowner">
</form>
</body></html>
EOT;
?>