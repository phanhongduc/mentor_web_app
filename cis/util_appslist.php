<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CIS Apps List</title>
<style>
tr:nth-child(even) {background-color: #f2f2f2}
tr:hover {background-color: #ddd;}
td {
padding-left:6px;
text-align:left;
}
</style>
<script>
EOT;

echo "function setParam(e){";
if(!empty($_POST['frame'])){
   echo "document.getElementById('gid').value=e.dataset.id;";
   echo "document.getElementById('{$_POST['frame']}').submit();";
}
echo "}";
echo "</script></head><body style='font-size:12px;'><center><table>";

//echo "post: ";print_r($_POST);echo "<br>";

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';

$query = 'SELECT id,name,rungroups,modelurl,iconurl,owner FROM apps';

if($_POST['usefilters']=='usefilters'){
    $qs='';
    $filters = array();
    if(!empty($_POST['fname']))
        $filters[] = "name LIKE '%" . pg_escape_string($_POST['fname']) ."%'";
    if(!empty($_POST['frungroups']))
        $filters[] =  "'{".pg_escape_string($_POST['frungroups'])."}' && rungroups";
    if(!empty($_POST['fmodelurl']))
        $filters[] = "modelurl LIKE '%" . pg_escape_string($_POST['fmodelurl']) ."%'";
    if(!empty($_POST['ficonurl']))
        $filters[] = "iconurl LIKE '%" . pg_escape_string($_POST['ficonurl']) ."%'";
    if(!empty($_POST['fowner']))
        $filters[] = "owner LIKE '%" . pg_escape_string($_POST['fowner']) ."%'";
    $qs = implode(' AND ',$filters);
    if($qs != '') $query .= ' WHERE ' . $qs;
}
$query .= ' ORDER BY name;';
//echo $query; echo '<br>';
$result = @pg_query($conn, $query);
if (!$result) $last_error = pg_last_error($conn);
else{
    $firows = pg_num_rows($result);
    if($firows >0){
        $fi=getValues($result);
        for ($j=0;$j<$firows;$j++){
            echo "<tr onclick='setParam(this)' data-id='{$fi[$j]['id']}' data-modelurl='{$fi[$j]['modelurl']}' data-iconurl='{$fi[$j]['iconurl']}' data-owner='{$fi[$j]['owner']}'><td>{$fi[$j]['name']}</td>";
            echo "<td>{$fi[$j]['rungroups']}</td></tr>";
        }
    }
}

echo "</table></center>";
if(!empty($_POST['frame']) and !empty($_POST['action'])){
   echo "<form method='post' action='{$_POST['action']}' target='{$_POST['frame']}' id='{$_POST['frame']}'>";
   echo "<input type='hidden' id='gid' name='gid'></form>"; // value='{$gid}'
}

echo <<<EOT
</body></html>
<!--
for embed
<iframe name="appslist" width="400" height="800" src="../cis/util_appslist.php"></iframe>

<form method="post" action="../cis/util_appslist.php" target="appslist" id="appslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fname" name="fname">
<input type="hidden" id="fmodelurl" name="fmodelurl">
<input type="hidden" id="ficonurl" name="ficonurl">
<input type="hidden" id="fowner" name="fowner">
</form>
-->
EOT;
?>