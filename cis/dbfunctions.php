<?php
include_once dirname(__DIR__) . "/Dao/GlobalConfiguration.php";

class DBException extends Exception {}
function getName($result) {
    $j=0;
    $num = pg_num_fields($result);
    $fieldname = array();
    while ($j<$num){
        $fieldname[$j] = pg_field_name($result, $j);
        $j++;
    }
    return $fieldname;
}
function getValues($result) {
	$fieldname = array();
	$values = array();
    $fieldname = getName($result);
    $i=0;
    while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
        $j=0;
        $v=array ();
        foreach ($line as $col_value) {
            $v[$fieldname[$j]] = $col_value;
            $j++;
        }
        $values[$i]=$v;
        $i++;
    }
    return $values;
}
function cyfConnect() {
    $cyf = pg_connect(GlobalConfiguration::getCyfConnectionString())
    or die('May be later ...');
    return $cyf;
}
function pgDelete($conn,$tab,$where){
    foreach($where as $k => $v){
        $s[] = $k."=".$v; 
    }
    $q = "DELETE FROM ".$tab." WHERE ".implode(' AND ', $s).";";
//echo $q."<br>";
    return pg_query($conn, $q);
}
function pgUpdate($conn,$tab,$set,$where){
    $s = array();
    foreach($set as $k => $v){
        $s[] = $k."=".$v; 
    }
    foreach($where as $k => $v){
        $w[] = $k."=".$v; 
    }
    $q = "UPDATE ".$tab." SET ".implode(',', $s)." WHERE ".implode(' AND ', $w).";";;
//echo $q."<br>";
//    $ret = @pg_query($conn, $q);
//    if(!$ret) echo pg_last_error($conn);
//    return $ret;
    return pg_query($conn, $q);
}
function pgInsert($conn,$tab,$set){
    $n = array();
    $i =array();
    foreach($set as $k => $v){
        $s[] = $k;
        $i[] = $v; 
    }
    $q = "INSERT INTO ".$tab."(".implode(',', $s).") VALUES (".implode(',', $i).");";
//echo $q."<br>";
    return pg_query($conn, $q);
}

?>