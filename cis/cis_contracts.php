<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cis/cis_contracts.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<style>
td {vertical-align:top;margin-left: 6px;text-align:left;border-width:1px;border-color: green;}
th {margin-left: 6px;text-align:center;border-width:1px;border-color: red;}
</style>
<title>Contracts Control Tool</title>
<script>
window.onload = function(){}
</script>
</head>
<body>
<center><br>

<table border=1>
<tr><th>Contracts</th><th>Contracts Control Tool (<a href="../cis/cis_users.php">Users Control Tool</a> or
 <a href="../cis/login.php">Exit</a>)</th><th>Groups</th></tr>
  <tr>
    <td rowspan="2">
<iframe name="contractslist" width="400" height="800" src="../cis/util_contractslist.php"></iframe>
    </td>
    <td>
<iframe name="contractsform" width="740" height="620" src="../cis/util_contractsform.php"></iframe>
    </td>
    <td rowspan="2">
<iframe name="groupslist" width="360" height="800" src="../cis/util_groupslist.php"></iframe>
    </td>
  </tr>
  <tr>
    <td>
<iframe name="groupsform" width="740" height="180" src="../cis/util_groupsform.php"></iframe>
    </td>
  </tr>
</table>
<!-- border:none; -->
</center>
</body>
</html>
EOT;

?>