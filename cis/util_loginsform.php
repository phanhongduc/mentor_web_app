<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Users Control Tool</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
input[type=text] {width:300px;}
input[type=password] {width:300px;}
textarea {width:300px;}
legend {font-size:1.2em;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
 document.getElementById('frame').value='loginsform';
 document.getElementById('action').value='../cis/util_loginsform.php';
 if(document.getElementById('ufilters').checked){
   document.getElementById('usefilters').value='usefilters';
   if(document.getElementById('ifname').checked)
     document.getElementById('fname').value=document.getElementById('iname').value;
   if(document.getElementById('ifemail').checked)
     document.getElementById('femail').value=document.getElementById('iemail').value;
   if(document.getElementById('ifownergroups').checked)
     document.getElementById('fownergroups').value=document.getElementById('iownergroups').innerHTML;
   if(document.getElementById('ifmembergroups').checked)
     document.getElementById('fmembergroups').value=document.getElementById('imembergroups').innerHTML;
 }
 document.getElementById('loginslist').submit();
}
</script>
</head>
<body style="font-size:12px;" onload="setParam();">
EOT;

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';

//echo "post: ";print_r($_POST);echo "<br>";

$gid = 0;
if(!empty($_POST['gid'])) $gid = $_POST['gid'];
$name = $_POST['name'];
$email = $_POST['email'];
$ownergroups = $_POST['ownergroups'];
$membergroups = $_POST['membergroups'];
$psword = $_POST['psword'];

echo '<form id="f" target="loginsform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

if(!empty($_POST['pcmd']) and !empty($_POST['tcmd'])){
//self    
    if(isset($_POST['tcmd'])){
        if($_POST['tcmd']=="Insert"){
            $query = "INSERT INTO logins (name,ownergroups,membergroups,email,psword) VALUES (";
            $query .= " '".pg_escape_string($_POST['name'])."'";
//            $query .= ",'{".pg_escape_string($_POST['ownergroups'])."}'";
//            $query .= ",'{".pg_escape_string($_POST['membergroups'])."}'";
            $query .= ",'".pg_escape_string($ownergroups)."'";
            $query .= ",'".pg_escape_string($membergroups)."'";
            if(!empty($_POST['email'])){
                $query .= ",'".pg_escape_string($_POST['email'])."'";
                if(!empty($_POST['psword'])){
                    $query .= ",'".pg_escape_string($_POST['psword'])."'";
                    $query .= ");";
                    echo $query . "<br>";
                    $result = @pg_query($conn, $query);
                    if (!$result) $last_error = pg_last_error($conn);
                }
            }
        }else if($_POST['tcmd']=="Update" and !empty($_POST['gid'])){
            $query = "UPDATE logins SET";
            $query .= " name='".pg_escape_string($_POST['name'])."'";
//            $query .= ",ownergroups='{".pg_escape_string($_POST['ownergroups'])."}'";
//            $query .= ",membergroups='{".pg_escape_string($_POST['membergroups'])."}'";
            $query .= ",ownergroups='".pg_escape_string($ownergroups)."'";
            $query .= ",membergroups='".pg_escape_string($membergroups)."'";
            if(!empty($_POST['email'])){
                $query .= ",email='".pg_escape_string($_POST['email'])."'";
                if(!empty($_POST['psword'])){
                    $query .= ",psword='".pg_escape_string($_POST['psword'])."'";
                }
                $query .= " WHERE id=".pg_escape_string($_POST['gid']).";";
                echo $query . "<br>";
                $result = @pg_query($conn, $query);
                if (!$result) $last_error = pg_last_error($conn);
            }
        }else if($_POST['tcmd']=="Delete" and !empty($_POST['gid'])){
            $query = "DELETE FROM logins WHERE id=".pg_escape_string($_POST['gid']).";";
//            echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }
    }
}
if(empty($_POST['usefilters']) and !empty($_POST['gid'])){
//callback
    $q = "SELECT id,name,ownergroups,membergroups,email FROM logins WHERE id={$_POST['gid']}";
//    echo $q; echo '<br>';
    $result = @pg_query($conn, $q);
    if (!$result) $last_error = pg_last_error($conn);
    else{
        $firows = pg_num_rows($result);
        if($firows >0){
            $fi=getValues($result);
            for ($j=0;$j<$firows;$j++){
                $gid = $fi[$j]['id'];
                $name = $fi[$j]['name'];
                $ownergroups = $fi[$j]['ownergroups'];
                $membergroups = $fi[$j]['membergroups'];
                $email = $fi[$j]['email'];
            }
        }
    }
}
echo "<input type='hidden' id='currid' name='gid' value='{$gid}'>";
echo "<center>";

echo "<table><caption style='text-align:center;'>Logins/Filters</caption>";

echo "<tr><td>name:</td><td><input type='text' id='iname' name='name' value='{$name}'></td>";
echo "<td><input type='checkbox' id='ifname' name='ifname'";
if(!empty($_POST['ifname'])) echo ' checked';
echo ">view with name like this</td></tr>";

echo "<tr><td>email:</td><td><input type='text' id='iemail' name='email' value='{$email}'></td>";
echo "<td><input type='checkbox' id='ifemail' name='ifemail'";
if(!empty($_POST['ifemail'])) echo ' checked';
echo ">view with email like this</td></tr>";

echo "<tr><td>password:</td><td><input type='password' id='ipsword' name='psword' value=''></td>";
echo "<td></td></tr>";

echo "<tr><td>ownergroups:</td><td><textarea id='iownergroups' name='ownergroups'>";
//echo rtrim( ltrim($ownergroups,'{'),'}')."</textarea></td>";
echo $ownergroups."</textarea></td>";
echo "<td><input type='checkbox' id='ifownergroups' name='ifownergroups'";
if(!empty($_POST['ifownergroups'])) echo ' checked';
echo ">view with ownergroups contains</td></tr>";

echo "<tr><td>membergroups:</td><td><textarea id='imembergroups' name='membergroups'>";
//echo rtrim( ltrim($membergroups,'{'),'}')."</textarea></td>";
echo $membergroups."</textarea></td>";
echo "<td><input type='checkbox' id='ifmembergroups' name='ifmembergroups'";
if(!empty($_POST['ifmembergroups'])) echo ' checked';
echo ">view with membergroups contains</td></tr>";

echo "</table>";

echo <<<EOT
<table><caption>&nbsp;</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td><input type="checkbox" id="ufilters" name="usefilters" value="usefilters"
EOT;
if(!empty($_POST['usefilters'])) echo ' checked';
echo '>use filters</td><td><input type="submit" name="pcmd" value="Submit" style=""></td></tr>';
echo '</table>';


echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
</center>
</form>

<form method="post" action="../cis/util_loginslist.php" target="loginslist" id="loginslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fname" name="fname">
<input type="hidden" id="femail" name="femail">
<input type="hidden" id="fownergroups" name="fownergroups">
<input type="hidden" id="fmembergroups" name="fmembergroups">
</form>
</body>
</html>
EOT;
