<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<style>
td {margin-left: 6px;}
th {margin-left: 6px;text-align:center;}
</style>
<title>CIS Apps Control Tool</title>
<script>
window.onload = function(){}
</script>
</head><body style="font-size:12px;">
<center><table>
<caption style="text-align:center;font-size:12px;">
CIS Apps Control Tool (<a href="../cis/cis_logins.php">CIS Logins Control Tool</a> or
 <a href="../cis/login.php">Exit</a>)
</caption>
<tr><th>Apps (Name/Run groups)</th><th></th><th>Groups (Name/Owner)</th></tr>
<tr>
<td rowspan="2"><iframe name="appslist" width="600" height="720" src="../cis/util_appslist.php"></iframe></td>

<td><iframe name="appsform" width="640" height="435" src="../cis/util_appsform.php"></iframe></td>

<td rowspan="2"><iframe name="groupslist" width="400" height="720" src="../cis/util_groupslist.php"></iframe></td>
</tr>
<tr>
<td><iframe name="groupsform" width="640" height="280" src="../cis/util_groupsform.php"></iframe></td>
</tr>
</table></center>
</body></html>
EOT;
?>