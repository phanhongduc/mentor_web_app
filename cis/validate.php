<?php
include_once dirname(__DIR__) . "/Dao/GlobalConfiguration.php";
include_once "Haka/HakaAuth.php";

class AuthClass {
    private $secret_word='secret word';
    public $conn = false;
    function __construct() {
        if (isset($_GET['is_exit']) and ($_GET['is_exit'] == 1)) {
            $_SESSION = array(); //clean  SESSION
            session_destroy();   //delete it
            header('Location: ../cis/login.php?is_exit=0'); //Redirect after exit
            exit(0);
        }
        $SID = session_id(); 
        if(empty($SID)) session_start() or exit(basename(__FILE__).'(): Could not start session');
    }
    public function connect() {
        $conn = pg_connect(GlobalConfiguration::getCisConnectionString());
        if(!$conn) { 
            $this->out(); //
            $this->invitation();
            exit(0);
        }
        return $conn;
    }
    public function invitation() {
        include('view/login/login.php');
    }

    public function out() {
        $_SESSION = array(); //clean  SESSION
        session_destroy();   //delete it
    }
    function auth_ok($s) {
        $_SESSION['user_id'] = $s['id'];  
        $_SESSION['email'] = $s['email'];  
        $_SESSION['name'] = $s['name'];  
        $_SESSION['lang'] = $s['lang'];  
        $_SESSION['ownergroups'] = $s['ownergroups'];  
        $_SESSION['membergroups'] = $s['membergroups'];
//        $_SESSION['ownergroups'] = str_replace('"', '', $s['ownergroups']);  
//        $_SESSION['membergroups'] = str_replace('"', '', $s['membergroups']);
        return true;
    }
    function auth_nok() {
        GlobalConfiguration::clear_auth();
        return true;
    }
    public function isAuth($redirect = true) {
        new HakaAuth();

        if (isset($_SESSION['user_id']) and
        isset($_SESSION['email'])   and
        isset($_SESSION['name'])   and
        isset($_SESSION['lang'])   and
        isset($_SESSION['ownergroups'])   and
        isset($_SESSION['membergroups'])) {
            return array('user_id' => $_SESSION['user_id'],
            'email' => $_SESSION['email'],
            'name' => $_SESSION['name'],
            'lang' => $_SESSION['lang'],
            'ownergroups' => $_SESSION['ownergroups'],
            'membergroups' => $_SESSION['membergroups']); 
        } elseif ($redirect) {
            header("Location: /cis/login.php");
            exit;
        }
        return null;
    }
    public function auth($conn) {
        if(isset($_POST['username'])){
            $username = htmlspecialchars(stripslashes(trim($_POST['username'])));
            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
              $this->auth_nok();//  $emailErr = "Invalid email format"; 
              return false; 
            }
$query = <<<EOT
SELECT id,email,psword,name,lang,ownergroups,membergroups FROM logins WHERE email=$1;
EOT;

            if (!pg_prepare ("", $query)) $last_error = pg_last_error();
            else{
                $login = array($username);
                $result = pg_execute("", $login);
                if (!$result) {//username not found
                    $last_error = pg_last_error($conn);
                    $this->auth_nok();
                    return false; 
                }else{
                    if(isset($_POST['password'])){
                        $password = htmlspecialchars(stripslashes(trim($_POST['password'])));
                        $fi=getValues($result);
                        if($password === $fi[0]['psword']){
                            $this->auth_ok($fi[0]);
                            return true;
                        }else{
                            $this->auth_nok();
                            return false; 
                        }
                    }else{//password not set
                        $this->auth_nok();
                        return false; 
                    }
                }
            }
        } //username not set
        $this->auth_nok();
        return false; 
    }
}
?>