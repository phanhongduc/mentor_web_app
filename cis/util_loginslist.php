<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Users Control Tool</title>
<style>
tr:nth-child(even) {background-color: #f2f2f2}
tr:hover {background-color: #ddd;}
td {
padding-left:6px;
text-align:left;
}
</style>
<script>
EOT;

echo "function setParam(e){";
if(!empty($_POST['frame'])){
   echo "document.getElementById('gid').value=e.dataset.id;";
   echo "document.getElementById('{$_POST['frame']}').submit();";
}
echo "}";
echo "</script></head><body style='font-size:12px;'><center><table>";

//echo "post: ";print_r($_POST);echo "<br>";

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';

$query = 'SELECT id,email,ownergroups,membergroups,name FROM logins';

if($_POST['usefilters']=='usefilters'){
    $qs='';
    $filters = array();
    if(!empty($_POST['fname']))
        $filters[] = "name LIKE '%" . pg_escape_string($_POST['fname']) ."%'";
    if(!empty($_POST['femail']))
        $filters[] = "email LIKE '%" . pg_escape_string($_POST['femail']) ."%'";
    if(!empty($_POST['fownergroups']))
        $filters[] =  "'{".pg_escape_string($_POST['fownergroups'])."}' && ownergroups";
    if(!empty($_POST['fmembergroups']))
        $filters[] =  "'{".pg_escape_string($_POST['fmembergroups'])."}' && membergroups";
    $qs = implode(' AND ',$filters);
    if($qs != '') $query .= ' WHERE ' . $qs;
}
$query .= ' ORDER BY name;';
//echo $query; echo '<br>';
$result = @pg_query($conn, $query);
if (!$result) $last_error = pg_last_error($conn);
else{
    $firows = pg_num_rows($result);
    if($firows >0){
        $fi=getValues($result);
        for ($j=0;$j<$firows;$j++){
            echo "<tr onclick='setParam(this)' data-id='{$fi[$j]['id']}' data-ownergroups='{$fi[$j]['ownergroups']}' data-membergroups='{$fi[$j]['membergroups']}'><td>{$fi[$j]['email']}</td>";
            echo "<td>{$fi[$j]['name']}</td></tr>";
        }
    }
}

echo "</table></center>";
if(!empty($_POST['frame']) and !empty($_POST['action'])){
   echo "<form method='post' action='{$_POST['action']}' target='{$_POST['frame']}' id='{$_POST['frame']}'>";
   echo "<input type='hidden' id='gid' name='gid'></form>"; // value='{$gid}'
}

echo <<<EOT
</body></html>
<!--
for embed:
<iframe name="loginslist" width="400" height="800" src="../cis/util_loginslist.php"></iframe>

<form method="post" action="../cis/util_loginslist.php" target="loginslist" id="loginslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fname" name="fname">
<input type="hidden" id="femail" name="femail">
<input type="hidden" id="fownergroups" name="fownergroups">
<input type="hidden" id="fmembergroups" name="fmembergroups">
</form>
-->
EOT;
?>