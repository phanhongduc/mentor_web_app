<?php
include_once dirname(dirname(__DIR__)) . "/Dao/GlobalConfiguration.php";
include_once dirname(dirname(__DIR__)) . "/Dao/LoginsTable.php";
include_once dirname(dirname(__DIR__)) . "/Dao/HakaAccountsTable.php";

use \Dao\LoginsTable;

class HakaAuth {

    public $haka_attributes;
    public $loginsTable;
    public $hakaAccountsTable;

    public $current_user;

    function __construct()
    {
        session_start();
        $this->haka_attributes  = [];
        $this->loginsTable     = new LoginsTable();
        $this->hakaAccountsTable = new \Dao\HakaAccountsTable();
        $this->current_user     = [];

        if (!empty($_COOKIE["Haka_Info"])) {
            $this->haka_attributes = unserialize(str_replace(GlobalConfiguration::COOKIE_ENCRYPT_KEY, "", base64_decode($_COOKIE["Haka_Info"])));

            if (!empty($this->haka_attributes) && is_array($this->haka_attributes)) {
                $email = $this->haka_attributes['eduPersonPrincipalName'][0];
                $name  = $this->haka_attributes['displayName'][0];
                empty($name) ? $name = $email: '';
                $owners = "{Turku University Manager}";
                $member = "{Turku University Student}";
                //todo: if user is a teacher, the member is "Turku University Staff"
                if(!$this->loginsTable->is_existed_user($email)) {
                    $this->hakaAccountsTable->insert_user($name, $email, json_encode($this->haka_attributes));
                    $this->loginsTable->insert_user($email, "a12eFA1ltP@ssW0rd", $owners, $member, $name);
                }
                $this->current_user = $this->loginsTable->get_user($email);    //user password should be removed
                GlobalConfiguration::set_auth($this->current_user);

                $this->setAuthJwtCookie();
            } else {
                $this->haka_attributes  = [];
                //header("Location: /cis/login.php");
            }
        }
    }

    public function getEncodedAuthJwt() {

        if (!empty($this->current_user)) {
            return GlobalConfiguration::encrypt($this->current_user);
        }
        return '';
    }

    private function setAuthJwtCookie() {
        setcookie("haka", $this->getEncodedAuthJwt(),time()+604800, "/");
    }
}