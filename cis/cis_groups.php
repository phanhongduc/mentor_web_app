<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<style>
td {margin-left: 6px;}
</style>
<title>CIS Groups Control Tool</title>
<script>
window.onload = function(){}
</script>
</head><body style="font-size:12px;">
<center>
<table><caption style="text-align:center;font-size:12px;">
CIS Groups Control Tool (
<a href="../cis/cis_apps.php">CIS Apps Control Tool</a>, 
<a href="../cis/cis_users.php">CIS Users Control Tool</a> or
<a href="../cis/login.php">Exit</a>)
</caption>
<tr><td>Name/Owner</td>
<td></td></tr>
<tr><td><iframe name="groupslist" width="400" height="400" src="../cis/util_groupslist.php"></iframe></td>
<td style="vertical-align:top"><iframe name="groupsform" width="400" height="200" src="../cis/util_groupsform.php"></iframe></td>
</tr></table
</center>
</body></html>
EOT;
?>