#!/usr/local/bin/perl5

#require 'cgi-lib.pl';
#print &PrintHeader();
#print &HtmlTop("CGI Example Using cgi-lib.pl");
#if(&ReadParse(*in)){
#	print &PrintVariables();
#}
#print &HtmlBot();

#!/usr/bin/perl
use DBI;

@drivers = DBI->available_drivers;
print "------------- drivers -----------\n";
for $i (@drivers) {
  print "$i\n";
}
print "---------------------------\n";
exit;

use JSON::PP; #if not already installed, just run "cpan JSON"
use CGI;
$co= new CGI;
#print   $co->header();
print $co->header('application/json;charset=UTF-8');

$id = $co->param('data_id');

#convert  data to JSON
my $op = JSON::PP -> new -> utf8 -> pretty(1);
my $json = $op -> encode({
    result => $id
});
print $json;


#print 	$co->start_html();
#print "From CGI<br>";
#@names = $co->param;

#foreach $key (@names)
#{
#	$value = $co->param($key);
#	print "$key : $value <br>";
#}
	
#print   $co->end_html();
exit;
=pod
<!DOCTYPE html>
<html>
    <head>
        <title>Testing ajax</title> 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script>

            $(document).ready(function() {

                $("#test").click(function(){
                    var ID = 100;
                    $.ajax({
                            type: 'POST',
                            url: '/cgi-bin/ajax/stackCGI/ajax.pl',
                            data: { 'data_id': ID },
                            success: function(res) {

                                                        alert("your ID is: " + res.result);

                                                    },
                            error: function() {alert("did not work");}
                    });
                })

            })



        </script>
    </head>
    <body>

        <button id="test" >Testing</button>

    </body>
</html>
=pod
