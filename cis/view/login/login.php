<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/fe/vendor.css?v=<?= GlobalConfiguration::VERSION ?>">
    <link rel="stylesheet" href="/fe/login/login.css?v=<?= GlobalConfiguration::VERSION ?>">
    <title>Login</title>
</head>
<body>
<div class="login">
    <header>
        <picture>
            <source srcset="/cis/view/login/img/login-header@2x.png" media="(min-width: 768px)">
            <img src="/cis/view/login/img/login-header.png">
        </picture>
    </header>
    <!--@todo remove old form login code later-->
    <!--<form method="POST" action="../cis/login.php">-->
    <!--    <!---->
    <!--    <ul data-role="listview" data-inset="true">-->
    <!--        <li data-icon="mydownload" class="ui-btn ui-btn-left"><a href="../cyf/mymobiletutor.apk" rel="external">Download Android app 'My mobile tutor'</a></li>-->
    <!--    </ul>-->
    <!--    <a href="../cyf/mymobiletutor.apk" rel="external" class="ui-li-icon ui-corner-none ui-btn">-->
    <!--        <img src="../cyf/img/download.png" alt="Download">-->
    <!--        Download Android app 'My mobile tutor'</a>-->
    <!--    <br>-->
    <!--    <input type="submit" name="pcmd" value="Enter as guest">-->
    <!--    <br>-->
    <!--    <!-- <input type="submit" name="pcmd" value="Registration"> -->
    <!--    <input type="text" name="username" id="username" placeholder="Your email...">-->
    <!--    <br>-->
    <!--    <input type="password" name="password" id="password">-->
    <!--    <br>-->
    <!--    <input type="submit" name="pcmd" value="Enter with authorization">-->
    <!--    <br>-->
    <!--</form>-->
    <div class="frame" id="frame" style="display: none">
        <iframe></iframe>
        <button class="back-to-app">Back to app</button>
    </div>
    <div class="login__content">
        <div class="login__form">
            <h1 class="login__title">Login</h1>
            <a href="<?= GlobalConfiguration::HAKA_ENTPOINT ?>" class="button__login button__login--haka">
                <img src="/cis/view/login/img/haka.svg">
                <div class="haka">Login</div>
            </a>
            <a href="/auth/saml/module.php/core/authenticate.php?as=haka-sp" class="button__login">Other</a>
        </div>
        <a data-external="true" data-href="https://chooseyourfuture.fi/user-approval/" href="#" class="login__privacy">
            User approval
        </a>
    </div>
    <footer>
        <picture>
            <source srcset="/cis/view/login/img/login-footer@3x.png" media="(min-width: 1024px)">
            <source srcset="/cis/view/login/img/login-footer@2x.png" media="(min-width: 768px)">
            <img src="/cis/view/login/img/login-footer.png">
        </picture>
    </footer>
</div>
<script src="/fe/vendor.js"></script>
</body>
</html>