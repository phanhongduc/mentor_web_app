function GetTextFromSelected(el)
{
   return el.options[el.options.selectedIndex].text;
}
function GetSimpleListFromSelected(el)
{
    var e = GetTextFromSelected(el);
    var str = e.split("|");
    return str;
}
function GetBraceFromSelected(el) {
    var e = GetTextFromSelected(el);
    var str = e.split("{")[1].split("}")[0];
    return str.trim();
}
function GetListFromSelected(el) {
    var e = GetBraceFromSelected(el);
    var str = e.split(",");
    return str;
}
function GetListAndBraceFromSelected(el) {
    var e = GetTextFromSelected(el);
    var str = e.split("{")[0]; //before {
    var grp = e.split("{")[1].split("}")[0];
//    var str = e.split("[")[1].split("{")[0]; //before {
    var ret = str.split(",");
	ret[ret.length-1] = grp;
    return ret;
}
function GetArrayFromSelected(el)
{
	var s = GetTextFromSelected(el);
	var m = s.match(/"(.*?)"/g);
	return m;
}
////////////////////////////////////
function Go2App(e)
{
	var f = document.createElement("form");
	f.setAttribute('method',"post");
	f.setAttribute('action',e.dataset.href);

var i = document.createElement("input");
	i.setAttribute('type',"hidden");
	i.setAttribute('name',"app");i.setAttribute('value',e.dataset.app);
	f.appendChild(i);

var j = document.createElement("input");
	j.setAttribute('type',"hidden");
	j.setAttribute('name',"rgroups");j.setAttribute('value',e.dataset.rgroups);
	f.appendChild(j);

	document.getElementsByTagName('body')[0].appendChild(f);
	f.submit();
}
