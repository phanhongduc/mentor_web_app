<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
input[type=text] {width:300px;}
textarea {width:300px;}
legend {font-size:1.2em;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
 document.getElementById('frame').value='appsform';
 document.getElementById('action').value='../cis/util_appsform.php';
 if(document.getElementById('ufilters').checked){
   document.getElementById('usefilters').value='usefilters';
   if(document.getElementById('ifname').checked)
     document.getElementById('fname').value=document.getElementById('iname').value;
   if(document.getElementById('ifrungroups').checked)
     document.getElementById('frungroups').value=document.getElementById('irungroups').innerHTML;
   if(document.getElementById('ifmodelurl').checked)
     document.getElementById('fmodelurl').value=document.getElementById('imodelurl').value;
   if(document.getElementById('ificonurl').checked)
     document.getElementById('ficonurl').value=document.getElementById('iiconurl').value;
   if(document.getElementById('ifowner').checked)
     document.getElementById('fowner').value=document.getElementById('iowner').value;
 }
 document.getElementById('appslist').submit();
}
</script>
</head>
<body style="font-size:12px;" onload="setParam();">
EOT;

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
//
//$_POST['gid']
//$_POST['fgname'] $_POST['gname']
//$_POST['fowner'] $_POST['owner']
//$_POST['tcmd'] = Insert Delete View Update
//$_POST['pcmd'] = Groups
//
//echo "post: ";print_r($_POST);echo "<br>";

$gid = 0;
if(!empty($_POST['gid'])) $gid = $_POST['gid'];
$name = $_POST['name'];
$rungroups = $_POST['rungroups'];
$modelurl = $_POST['modelurl'];
$iconurl = $_POST['iconurl'];
$owner = $_POST['owner'];

echo '<form id="f" target="appsform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

if(!empty($_POST['gid']) and !empty($_POST['pcmd']) and !empty($_POST['tcmd'])){
//self    
    if(isset($_POST['tcmd'])){
        if($_POST['tcmd']=="Insert"){
            $query = "INSERT INTO apps (name,rungroups,modelurl,iconurl,owner) VALUES (";
            $query .= " '".pg_escape_string($_POST['name'])."'";
            $query .= ",'{".pg_escape_string($_POST['rungroups'])."}'";
            $query .= ",'".pg_escape_string($_POST['modelurl'])."'";
            $query .= ",'".pg_escape_string($_POST['iconurl'])."'";
            $query .= ",'".pg_escape_string($_POST['owner'])."'";
           $query .= ");";
            echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }else if($_POST['tcmd']=="Update"){
                $query = "UPDATE apps SET";
                $query .= " name='".pg_escape_string($_POST['name'])."'";
                $query .= ",rungroups='{".pg_escape_string($_POST['rungroups'])."}'";
                $query .= ",modelurl='".pg_escape_string($_POST['modelurl'])."'";
                $query .= ",iconurl='".pg_escape_string($_POST['iconurl'])."'";
                $query .= ",owner='".pg_escape_string($_POST['owner'])."'";
                $query .= " WHERE id=".pg_escape_string($_POST['gid']).";";
                echo $query . "<br>";
                $result = @pg_query($conn, $query);
                if (!$result) $last_error = pg_last_error($conn);
        }else if($_POST['tcmd']=="Delete"){
            $query = "DELETE FROM apps WHERE id=".pg_escape_string($_POST['gid']).";";
            echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }
    }
}
if(empty($_POST['usefilters']) and !empty($_POST['gid'])){
//callback
    $q = "SELECT id,name,rungroups,modelurl,iconurl,owner FROM apps WHERE id={$_POST['gid']}";
//    echo $q; echo '<br>';
    $result = @pg_query($conn, $q);
    if (!$result) $last_error = pg_last_error($conn);
    else{
        $firows = pg_num_rows($result);
        if($firows >0){
            $fi=getValues($result);
            for ($j=0;$j<$firows;$j++){
                $gid = $fi[$j]['id'];
                $name = $fi[$j]['name'];
                $rungroups = $fi[$j]['rungroups'];
                $modelurl = $fi[$j]['modelurl'];
                $iconurl = $fi[$j]['iconurl'];
                $owner = $fi[$j]['owner'];
            }
        }
    }
}
echo "<input type='hidden' id='currid' name='gid' value='{$gid}'>";
echo "<center>";

echo "<table><caption style='text-align:center;'>Apps/Filters</caption>";

echo "<tr><td>name:</td><td><input type='text' id='iname' name='name' value='{$name}'></td>";
echo "<td><input type='checkbox' id='ifname' name='ifname'";
if(!empty($_POST['ifname'])) echo ' checked';
echo ">view with name like this</td></tr>";

echo "<tr><td>modelurl:</td><td><input type='text' id='imodelurl' name='modelurl' value='{$modelurl}'></td>";
echo "<td><input type='checkbox' id='ifmodelurl' name='ifmodelurl'";
if(!empty($_POST['ifmodelurl'])) echo ' checked';
echo ">view with modelurl like this</td></tr>";

echo "<tr><td>iconurl:</td><td><input type='text' id='iiconurl' name='iconurl' value='{$iconurl}'></td>";
echo "<td><input type='checkbox' id='ificonurl' name='ificonurl'";
if(!empty($_POST['ificonurl'])) echo ' checked';
echo ">view with iconurl like this</td></tr>";

echo "<tr><td>owner:</td><td><input type='text' id='iowner' name='owner' value='{$owner}'></td>";
echo "<td><input type='checkbox' id='ifowner' name='ifowner'";
if(!empty($_POST['ifowner'])) echo ' checked';
echo ">view with owner like this</td></tr>";

echo "<tr><td>rungroups:</td><td><textarea id='irungroups' name='rungroups'>";
echo rtrim( ltrim($rungroups,'{'),'}')."</textarea></td>";
echo "<td><input type='checkbox' id='ifrungroups' name='ifrungroups'";
if(!empty($_POST['ifrungroups'])) echo ' checked';
echo ">view with rungroups contains</td></tr>";

echo "</table>";

echo <<<EOT
<table><caption>&nbsp;</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td><input type="checkbox" id="ufilters" name="usefilters" value="usefilters"
EOT;
if(!empty($_POST['usefilters'])) echo ' checked';
echo '>use filters</td><td><input type="submit" name="pcmd" value="Apps" style=""></td></tr>';
echo '</table>';

echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
</center>
</form>
<form method="post" action="../cis/util_appslist.php" target="appslist" id="appslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fname" name="fname">
<input type="hidden" id="frungroups" name="frungroups">
<input type="hidden" id="fmodelurl" name="fmodelurl">
<input type="hidden" id="ficonurl" name="ficonurl">
<input type="hidden" id="fowner" name="fowner">
</form>
</body></html>
EOT;
