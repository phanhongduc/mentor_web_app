const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const DotENV = require('dotenv-webpack');

const env = process.env.npm_lifecycle_event;

const common = {
  entry: [
    'react-hot-loader/patch',
    'babel-polyfill',
    './fe_src/index.js',
    'whatwg-fetch',
  ],

  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js|jsx?$/,
        loaders: [
          {
            loader: 'eslint-loader',
            options: {
              fix: true,
              emitWarning: true,
            },
          },
        ],
        exclude: /node_modules/,
        include: [path.join(__dirname, 'fe_src')],
      },
      { enforce: 'pre', test: /\.json$/, loader: 'json-loader' },
      {
        test: /\.js|jsx?$/,
        loaders: [
          {
            loader: 'babel-loader?retainLines=true',
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /(\.css)$/,
        use: ['style-loader', 'css-loader?sourceMap', 'postcss-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
  },

  plugins: [

    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|fi/),
    // do not emit compiled assets that include errors
  ],
};

const dev = merge(common, {
  entry: [
    'webpack-dev-server/client?http://localhost:3001',
    'webpack/hot/only-dev-server',
  ],

  output: {
    filename: 'bundle.js',

    path: path.resolve(__dirname, 'fe'),

    publicPath: '/',
  },

  devtool: 'inline-source-map',

  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        loaders: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'img/[name].[ext]',
            },
          },
        ],
      },
    ],
  },

  plugins: [

    new HtmlWebpackPlugin({
      template: './fe_src/index.html',
    }),

    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),

    new DotENV({
      path: './.env.example',
    }),
  ],

  devServer: {
    host: 'localhost',
    port: 3001,

    historyApiFallback: true,
    // respond to 404s with index.html

    hot: true,
    // enable HMR on the server
  },
});

const production = merge(common, {

  output: {
    filename: 'bundle.[chunkhash].js',
    path: path.resolve(__dirname, 'fe'),
    publicPath: '/fe/',
  },

  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        loaders: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'img/[name]-[hash].[ext]',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './fe_src/index.html',
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      comments: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new DotENV({
      path: './.env',
      safe: true,
    }),
  ],
});

const devPhp = merge(production, {
  plugins: [
    new HtmlWebpackPlugin({
      template: './fe_src/index.html',
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      comments: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],
});


switch (env) {
  case 'fe:dev':
    module.exports = dev;
    break;
  case 'fe:build-react':
  case 'fe:build':
    module.exports = production;
    break;
  case 'fe:dev-php':
    module.exports = devPhp;
    break;
}
