<?php
require_once(dirname(__DIR__) . '/vendor/autoload.php');

use \Firebase\JWT\JWT;

class GlobalConfiguration
{
    /**
     * Must be relative to auth\modules\core\www\authenticate.php
     */
    const COOKIE_ENCRYPT_KEY = "mycookiekey123456";
    const JWT_KEY = "HSDR56*$^!%aate!$#%4913451a6s45@#$%Awsf";

    const PROTOCOL      = "https";
    const VERSION       = "0.1.0";

    const DOMAIN   = "chooseyourfuture.fi";
    //const HAKA_ENTPOINT = self::PROTOCOL . "://" . self::DOMAIN . "/auth/saml/module.php/core/authenticate.php?as=haka-sp";
    const HAKA_ENTPOINT = self::PROTOCOL . "://" . self::DOMAIN . "/auth/saml/module.php/core/authenticate.php?as=haka";

    public static function isDevMode() {
        return $_SERVER['HTTP_HOST'] != "chooseyourfuture.fi";
    }

    public static function getCisConnectionString() {
        if (self::isDevMode()) {
            return "host=cyfapp_db_1 port=5432 dbname=choos562_cis user=choos562 password=choos562";
        }
        return "port=5432 dbname=choos562_cis user=choos562_cisprg password=z0kVhzhA5ogrt";
    }

    public static function getCyfConnectionString() {
        if (self::isDevMode()) {
            return "host=cyfapp_db_1 port=5432 dbname=choos562_cyf user=choos562 password=choos562";
        }
        return "port=5432 dbname=choos562_cyf user=choos562_cyfprg password=nIq7tivyMfwxT6";

    }

    public static function loginToHaka() {
        header("Location: " . self::loginToHaka());
        exit;
    }

    public static function logOut() {
        self::clear_cookie();
        self::clear_cookie("haka");
        self::clear_auth();
        header("Location: /cis/login.php");
        exit;
    }

    public static function set_auth($user) {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['lang'] = $user['lang'];
        $_SESSION['ownergroups'] = $user['ownergroups'];
        $_SESSION['membergroups'] = $user['membergroups'];
        return true;
    }

    public static function clear_auth() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();        // MUST HAVE
        }
        unset($_SESSION['user_id']);
        unset($_SESSION['email']);
        unset($_SESSION['name']);
        unset($_SESSION['lang']);
        unset($_SESSION['ownergroups']);
        unset($_SESSION['membergroups']);
    }

    public  static function clear_cookie($cookie_name = "Haka_Info") {
        setcookie($cookie_name, "",1, "/", "", true);
        setcookie($cookie_name, "",1, "/", "", false);
    }

    public static function encrypt($data, $alg = "HS256"){
        return JWT::encode($data, self::JWT_KEY, $alg);
    }

    public static function decrypt($data, $alg = "HS256") {
        return JWT::decode($data, self::JWT_KEY, [$alg]);
    }

    /**
     * The protocal and url must relate to .env file
     * @return string
     */
    public static function getFullAddressWithPort(){
        $protocol = (isset($_SERVER['HTTPS']) && (strcasecmp('off', $_SERVER['HTTPS']) !== 0)) ? "https://" : "http://";
        $hostname = $_SERVER['HTTP_HOST'];

        $port = (in_array($_SERVER['SERVER_PORT'], ['80','443'])) ? '' : ":" . $_SERVER['SERVER_PORT'];
        $arr_url =parse_url($protocol . $hostname);
        return  $arr_url['scheme'] . "://" . $arr_url['host'] . $port;
    }

    public static function pgGetInsertString($conn, $tab, $set)
    {
        $n = array();
        $i = array();
        foreach ($set as $k => $v) {
            $s[] = $k;
            $i[] = $v;
        }
        $q = "INSERT INTO " . $tab . "(" . implode(',', $s) . ") VALUES (" . implode(',', $i) . ")";

        return $q;
    }

    public static function pgGetUpdateFieldString($set) {
        $q = "";
        foreach ($set as $key=>$value){
            $q .= ",{$key}='" . $value . "'";
        }
        return substr($q, 1, strlen($q));
    }

    /**
     * https://stackoverflow.com/questions/1319903/how-to-flatten-a-multidimensional-array
     *
     * @param array $arr
     * @return array $result
     */
    public static function flattenArray($arr){
        $result = [];
        if (is_array($arr)) {
            array_walk_recursive($arr, function($a) use (&$result) { $result[] = $a; });
        }
        return $result;
    }

    public static function getDistanceMeters($lat1, $long1, $lat2, $long2){
        $p1 = new POI($lat1, $long1);
        $p2 = new POI($lat2, $long2);
        return $p1->getDistanceInMetersTo($p2);
    }
}

/**
 * Class POI
 * http://rosettacode.org/wiki/Haversine_formula#PHP
 */
class POI {
    private $latitude;
    private $longitude;
    public function __construct($latitude, $longitude) {
        $this->latitude = deg2rad($latitude);
        $this->longitude = deg2rad($longitude);
    }
    public function getLatitude() {return $this->latitude;}
    public function getLongitude() {return $this->longitude;}

    /**
     * @param POI $other
     * @return int $distance distance in meters
     */
    public function getDistanceInMetersTo(POI $other) {
        $radiusOfEarth = 6371000;// Earth's radius in meters.
        $diffLatitude = $other->getLatitude() - $this->latitude;
        $diffLongitude = $other->getLongitude() - $this->longitude;
        $a = sin($diffLatitude / 2) * sin($diffLatitude / 2) +
            cos($this->latitude) * cos($other->getLatitude()) *
            sin($diffLongitude / 2) * sin($diffLongitude / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $radiusOfEarth * $c;
        return $distance;
    }
}
