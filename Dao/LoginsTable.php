<?php
namespace Dao;
include_once __DIR__ . "/CisDatabase.php";

class LoginsTable extends CisDatabase
{
    /**
     * LoginsTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->tableName = "logins";
    }

    /**
     * @param $email
     * @param $password
     * @param $owners
     * @param $members
     * @param $name
     * @param string $lang
     *
     * @return resource | bool | mixed
     */
    public function insert_user($email, $password, $owners, $members, $name, $lang = "en") {
        return pg_insert($this->conn, $this->tableName, [
            "email"     => $email,
            "psword"    => $password,
            "ownergroups"   => $owners,
            "membergroups"  => $members,
            "name"          => $name,
            "lang"          => $lang
        ]);
    }
}