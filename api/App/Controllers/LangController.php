<?php

namespace App\Controllers;

class LangController
{

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function get($request, $response, $args)
    {
        $str = file_get_contents(__DIR__ . '/../../lang/'. $_REQUEST['lang'] .'.json');

        $json = json_decode($str, true);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($json);
    }
}
