<?php

namespace App\Controllers;

class EventsController
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createEvent($request, $response, $args)
    {

        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $requestJson = $request->getParsedBody();

        $i = array();
        $i['author'] = "'" . $param['email'] . "'";
        if (!empty($requestJson['sgroups'])) {
            $i['sgroups'] = "'" . pg_escape_string($requestJson['sgroups']) . "'";//availabled
        } else {
            $i['sgroups'] = "'{" . $param['email'] . "}'";//availabled
        }
        $i['descr'] = "'" . pg_escape_string($requestJson['descr']) . "'";
        if (!empty($requestJson['categs'])
            and $requestJson['categs'] != '{}')
            $i['categs'] = "'" . pg_escape_string($requestJson['categs']) . "'"; //subscriptions
        else $i['categs'] = "'{}'";
        if (!empty($requestJson['ldescr']))
            $i['ldescr'] = "'" . pg_escape_string($requestJson['ldescr']) . "'";
        if (!empty($requestJson['url']))
            $i['url'] = "'" . pg_escape_string($requestJson['url']) . "'";
        if (!empty($requestJson['locat']))
            $i['locat'] = "'" . pg_escape_string($requestJson['locat']) . "'";
        if (!empty($requestJson['longt']))
            $i['longt'] = "'" . pg_escape_string($requestJson['longt']) . "'";
        if (!empty($requestJson['lat']))
            $i['lat'] = "'" . pg_escape_string($requestJson['lat']) . "'";
        if (!empty($requestJson['contact']))
            $i['contact'] = "'" . pg_escape_string($requestJson['contact']) . "'";
        if (!empty($requestJson['tel']))
            $i['tel'] = "'" . pg_escape_string($requestJson['tel']) . "'";
        if (!empty($requestJson['email']))
            $i['email'] = "'" . pg_escape_string($requestJson['email']) . "'";
        if (!empty($requestJson['stimes'])
            and $requestJson['stimes'] != ' ')
            $i['stimes'] = "'" . pg_escape_string($requestJson['stimes']) . "'";
        if (!empty($requestJson['etimes'])
            and $requestJson['etimes'] != ' ')
            $i['etimes'] = "'" . pg_escape_string($requestJson['etimes']) . "'";

        $q = \GlobalConfiguration::pgGetInsertString($cyf, 'events', $i);
        $q .= " RETURNING id,descr,ldescr,url,author,sgroups,timest::date as datest,timest::time as timest,stimes::date as sdate,stimes::time as stime,etimes::date as edate,etimes::time as etime,locat,longt,lat,contact,tel,email,status,categs;";

        $result = pg_query($cyf, $q);

        $final = pg_fetch_all($result);

        if (empty($final)) {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event.'
                ]);

        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($final);

    }

    public function updateEvent($request, $response, $args)
    {

        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $requestJson = $request->getParsedBody();

        $q = "UPDATE events SET ";
        if (!empty($requestJson['eid']) AND $requestJson['eid'] > 0
            AND isset($requestJson['descr'])) {
            $q .= "descr='" . pg_escape_string($requestJson['descr']) . "'";
            if (isset($requestJson['ldescr']))
                $q .= ",sgroups='" . pg_escape_string($requestJson['sgroups']) . "'";
            if (isset($requestJson['ldescr']))
                $q .= ",ldescr='" . pg_escape_string($requestJson['ldescr']) . "'";
            if (isset($requestJson['url']))
                $q .= ",url='" . pg_escape_string($requestJson['url']) . "'";
            if (isset($requestJson['locat']))
                $q .= ",locat='" . pg_escape_string($requestJson['locat']) . "'";
            if (isset($requestJson['longt']))
                $q .= ",longt='" . pg_escape_string($requestJson['longt']) . "'";
            if (isset($requestJson['lat']))
                $q .= ",lat='" . pg_escape_string($requestJson['lat']) . "'";
            if (isset($requestJson['contact']))
                $q .= ",contact='" . pg_escape_string($requestJson['contact']) . "'";
            if (isset($requestJson['tel']))
                $q .= ",tel='" . pg_escape_string($requestJson['tel']) . "'";
            if (isset($requestJson['email']))
                $q .= ",email='" . pg_escape_string($requestJson['email']) . "'";
            if (isset($requestJson['categs']))
                $q .= ",categs='" . pg_escape_string($requestJson['categs']) . "'";
            if (isset($requestJson['stimes']) and $requestJson['stimes'] != ' ')
                $q .= ",stimes='" . pg_escape_string($requestJson['stimes']) . "'";
            if (isset($requestJson['etimes']) and $requestJson['etimes'] != ' ')
                $q .= ",etimes='" . pg_escape_string($requestJson['etimes']) . "'";
            if (isset($requestJson['author']))
                $q .= ",author='" . pg_escape_string($requestJson['author']) . "'";
            if (isset($requestJson['agroups']))
                $q .= ",agroups='" . pg_escape_string($requestJson['agroups']) . "'";
            $q .= " WHERE id=" . pg_escape_string($requestJson['eid']);
            $q .= " AND (";
            $q .= " author='" . $param['email'] . "'";
            $q .= " OR ARRAY[author] && '{$param['membergroups']}'";
            $q .= ") ";
            $q .= " RETURNING id,descr,ldescr,url,author,sgroups,timest::date as datest,timest::time as timest,stimes::date as sdate,stimes::time as stime,etimes::date as edate,etimes::time as etime,locat,longt,lat,contact,tel,email,status,categs;";
        } else {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event.'
                ]);
        }

        $result = pg_query($cyf, $q);

        $final = pg_fetch_all($result);


        if (empty($final)) {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event.'
                ]);

        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson(pg_fetch_all($result));

    }

    public function deleteEvent($request, $response)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $requestJson = $request->getParsedBody();
        $id = pg_escape_string($requestJson['id']);


        $q = "DELETE FROM events WHERE id={$id} AND author='{$param['email']}';";

        $res = pg_query($cyf, $q);

        $final = pg_affected_rows($res);

        if ($final === 1) {
            return $response->withHeader(
                'Content-Type',
                'application/json'
            )
                ->withJson('ok');
        } else {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event or you do not have the permission.'
                ]);
        }
    }

    public function get($request, $response, $args)
    {

        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $q = "SELECT id,descr,ldescr,url,author,sgroups,timest::date as datest,timest::time as timest,stimes::date as sdate,stimes::time as stime,etimes::date as edate,etimes::time as etime,locat,longt,lat,contact,tel,email,status,categs FROM events";
        $q .= " WHERE (";
        $q .= " author='{$param['email']}'";

        $q .= " OR sgroups && '{{$param['email']}}'";
        $q .= " OR sgroups && '{$param['membergroups']}'";
        $q .= " OR sgroups && '{$param['ownergroups']}'";
        $q .= ")";
        if (!empty($_REQUEST['fcategs']) and $_REQUEST['fcategs'] != '{}')
            $q .= " AND categs && '" . pg_escape_string($_REQUEST['fcategs']) . "'";

        if (!empty($_REQUEST['filters'])) {
            switch ($_REQUEST['filters']) {
                case 'all':
                    break;
                case 'lmonth':
                    $q .= " AND stimes > (now()::timestamp - interval '1 month') AND stimes < now()::timestamp";
                    break;
                case 'lweek':
                    $q .= " AND stimes > (now()::timestamp - interval '7 days') AND stimes < now()::timestamp";
                    break;
                case 'lday':
                    $q .= " AND stimes > (now()::timestamp - interval '24 hours') AND stimes < now()::timestamp";
                    break;
                case 'tday':
                    $q .= " AND stimes >= (now()::timestamp + interval '1 hours') AND stimes < (now()::timestamp + interval '24 hours')";
                    break;
                case 'nmonth':
                    $q .= " AND stimes BETWEEN date_trunc('month', CURRENT_TIMESTAMP) + interval '1 month' AND date_trunc('month', CURRENT_TIMESTAMP) + interval '2 month'";
                    break;
                case 'future':
                    $q .= " AND stimes > now()::timestamp";
                    break;
                case 'notime':
                    $q .= " AND quote_nullable(stimes::text)='NULL'";
                    break;
                case 'tomorrow':
                    $q .= " AND stimes >= (now()::timestamp + interval '1 day') AND stimes < (now()::timestamp + interval '2 day')";
                    break;
                case 'tweek':
                    $q .= " AND stimes BETWEEN NOW()::DATE-EXTRACT(DOW FROM NOW())::INTEGER AND NOW()::DATE-EXTRACT(DOW from NOW())::INTEGER+7";
                    break;
                case 'nweek':
                    $q .= " AND stimes BETWEEN NOW()::DATE-EXTRACT(DOW FROM NOW())::INTEGER+8 AND NOW()::DATE-EXTRACT(DOW from NOW())::INTEGER+15";
                    break;
                case 'tmonth':
                    $q .= " AND stimes BETWEEN date_trunc('month', CURRENT_TIMESTAMP) AND date_trunc('month', CURRENT_TIMESTAMP) + interval '1 month'";
                    break;
                case 'previous':
                    $q .= " AND stimes < now()::timestamp";
                    break;
                default:
                    $q .= " AND (quote_nullable(stimes::text)='NULL' OR stimes > now()::timestamp)";
            }
        }

        $q .= " ORDER BY stimes,author,descr;";

        $result = pg_query($cyf, $q);

        $fetchAll = $this->addFavorite(pg_fetch_all($result));

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($fetchAll == false ? [] : $fetchAll);
    }

    public function count($request, $response, $args) {
        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $q = "SELECT count(id) FROM events";
        $q .= " WHERE (";
        $q .= " author='{$param['email']}'";

        $q .= " OR sgroups && '{{$param['email']}}'";
        $q .= " OR sgroups && '{$param['membergroups']}'";
        $q .= " OR sgroups && '{$param['ownergroups']}'";
        $q .= ")";
        if (!empty($_REQUEST['fcategs']) and $_REQUEST['fcategs'] != '{}')
            $q .= " AND categs && '" . pg_escape_string($_REQUEST['fcategs']) . "'";
        $q .= " AND stimes > now()::timestamp";
        $result = pg_query($cyf, $q);
        $fetchAll = pg_fetch_all($result);
        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($fetchAll == false ? [] : $fetchAll);

    }

    private function addFavorite($events){
        $userEventController = new UserEventController($this->container);
        $userEvents = \GlobalConfiguration::flattenArray($userEventController->getEventIdsByUser());

        $result = [];
        foreach ($events as $event) {
            if (in_array($event['id'], $userEvents)) {
                $event['favourite_flag'] = true;
            } else {
                $event['favourite_flag'] = false;
            }
            $result[] = $event;
        }
        return $result;
    }
}
