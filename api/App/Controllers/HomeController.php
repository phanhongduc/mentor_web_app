<?php

namespace App\Controllers;

class HomeController
{

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function get($request, $response, $args)
    {
        $str = file_get_contents('home.json');

        $json = json_decode($str, true);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($json);
    }
}
