<?php

namespace App\Controllers;

class GroupsController
{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function get($request, $response, $args)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();

        var_dump($param);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($param['membergroups']);
    }
}