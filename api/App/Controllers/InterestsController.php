<?php

namespace App\Controllers;

class InterestsController
{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function get($request, $response, $args)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $q = "SELECT id, content FROM interests ORDER BY id;";

        $result = pg_query($cyf, $q);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson(pg_fetch_all($result));
    }
}
