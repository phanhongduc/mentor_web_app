<?php

namespace App\Controllers;

class SubscriptionsController
{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function get($request, $response, $args)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
        $q.=" WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}'";
        $q.=" OR allowgroups && '{$param['membergroups']}'";
        $q.=" OR allowgroups && '{$param['ownergroups']}'";
        $q.=" ORDER BY theme";
        $q .= ";";

        $result = pg_query($cyf, $q);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson(pg_fetch_all($result));
    }
}