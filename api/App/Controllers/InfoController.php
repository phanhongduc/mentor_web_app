<?php

namespace App\Controllers;

class InfoController
{
    protected $container;
    protected  $cyf;
    protected $param;

    public function __construct($container) {
        $this->container = $container;
//        $auth = new \AuthClass();
//        $this->param = $auth->isAuth();
        //$this->param['lang'] = 'en';
        $this->cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());
    }

    /**
        GET /api/index.php/info?page=12&lang=fi
        GET /api/index.php/info?page=12
        GET /api/index.php/info?page=TRANSPORT&lang=fi
        GET /api/index.php/info?page=TRANSPORT
     */
    public function get($request, $response, $args)
    {
        $language = 'en';
        if (isset($this->param['lang'])) {      //if login, get param from user
            $language = $this->param['lang'];
        } elseif (isset($_REQUEST['lang'])) {   // get param from url query
            $language = pg_escape_string($_REQUEST['lang']);
        }

        if (isset($_REQUEST['id'])) {
            $page_id = pg_escape_string($_REQUEST['id']);

            $q = "SELECT i.id, lang, epage_id, content, e.descr, e.etype, e.owner, e.sgroups FROM info_pages i JOIN epages e ON i.epage_id = e.id WHERE i.id = " . $page_id . " AND lang = '" . $language . "';";
            $result = pg_query($this->cyf, $q);
            $final = pg_fetch_all($result);

            if ($final == false) {      //no other version, get EN version
                $language = 'en';
                $q = "SELECT i.id, lang, epage_id, content, e.descr, e.etype, e.owner, e.sgroups FROM info_pages i JOIN epages e ON i.epage_id = e.id WHERE i.id = " . $page_id . " AND lang = '" . $language . "';";
                $result = pg_query($this->cyf, $q);
                $final = pg_fetch_all($result);
            }

            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withJson($final == false ? [] : $final);
        } elseif (isset($_REQUEST['page'])){
            $page_title = pg_escape_string($_REQUEST['page']);
            $q = "SELECT i.id, lang, epage_id, content, e.descr, e.etype, e.owner, e.sgroups FROM info_pages i JOIN epages e ON i.epage_id = e.id WHERE e.descr LIKE '" . $page_title . "' AND lang = '" . $language . "';";
            $result = pg_query($this->cyf, $q);
            $final = pg_fetch_all($result);

            if ($final == false) {      //no other version, get EN version
                $language = 'en';
                $q = "SELECT i.id, lang, epage_id, content, e.descr, e.etype, e.owner, e.sgroups FROM info_pages i JOIN epages e ON i.epage_id = e.id WHERE e.descr LIKE '" . $page_title . "' AND lang = '" . $language . "';";
                $result = pg_query($this->cyf, $q);
                $final = pg_fetch_all($result);
            }

            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withJson($final == false ? [] : $final[0]);
        }
        else {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Info page is not found.'
                ]);
        }

    }

    /**
     * Default limit is 10 items
     *
     * @example
     *  https://chooseyourfuture.fi/api/index.php/info/search?keyword=food&page=0&limit=3
     *
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function search($request, $response, $args) {
        $keyword = pg_escape_string($_REQUEST['keyword']);
        $limit = 10;
        if (isset($_REQUEST['limit']) && is_numeric($_REQUEST['limit'])) {
            $limit = pg_escape_string($_REQUEST['limit']);
        }

        $page = 0;
        if (isset($_REQUEST['page']) && is_numeric($_REQUEST['page'])) {
            $page = pg_escape_string($_REQUEST['page']);
        }
        $offset = $page * $limit;

        $q = "SELECT i.id, lang, epage_id, content, e.descr, e.etype, e.owner, e.sgroups FROM info_pages i JOIN epages e ON i.epage_id = e.id
        WHERE to_tsvector(i.content) @@ to_tsquery('" . $keyword . "') OFFSET " . $offset . " LIMIT " . $limit;

        $result = pg_query($this->cyf, $q);
        $final = pg_fetch_all($result);

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($final == false ? [] : $final);
    }

    /*
    public function get($request, $response, $args)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $page = $_REQUEST['page'];
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());

        $q = "SELECT id,owner,array_to_string(sgroups,'~^~') as sgr,etype as type,array_to_string(items,'~^~') as arg  FROM epages WHERE descr=".'$h$'.$page.'$h$'." AND (sgroups && '{$param['membergroups']}' OR sgroups && '{$param['ownergroups']}' OR sgroups && '{{$param['email']}}' OR owner='{$param['email']}');";

        $res = pg_query($cyf, $q);

        $responseFe = '';


        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $r = pg_fetch_assoc($res);

            $sgr= json_encode(explode('~^~',$r['sgr'] ));
            $arg= json_encode(explode('~^~',$r['arg']));
            $responseFe = '{"id":"'.$r['id'].'","owner":"'.$r['owner'].'","title":"'.$page.'","page":"'.$page.'","type":"'.$r['type'].'","sgroups":'.$sgr.',"args":'.$arg.'}';
        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->getBody()->write($responseFe);
    }
    */
}
