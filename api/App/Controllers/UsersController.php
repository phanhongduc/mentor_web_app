<?php

namespace App\Controllers;

class UsersController
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function check($request, $response, $args)
    {
        $auth = new \AuthClass();
        $param = $auth->isAuth();

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($param);
    }

    public function changeLang($request, $response, $args)
    {

        $auth = new \AuthClass();
        $param = $auth->isAuth();
        $cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());
        $cis = pg_connect(\GlobalConfiguration::getCisConnectionString());

        $requestJson = $request->getParsedBody();
        if (in_array($requestJson['lang'], ['en', 'fi', 'rus'])) {

            $q = "UPDATE users SET lang='{$requestJson['lang']}'";
            $q .= " WHERE email='{$param['email']}';";
            $res = pg_query($cyf, $q);

            $q = "UPDATE logins SET lang='{$requestJson['lang']}'";
            $q .= " WHERE email='{$param['email']}';";
            $res = pg_query($cis, $q);

            $_SESSION['lang'] = $requestJson['lang'];

            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withJson(pg_affected_rows($res));
        } else {
            return $response
                ->withStatus(405)
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withJson([
                    'message' => 'Language is not supported.'
                ]);
        }
    }
}