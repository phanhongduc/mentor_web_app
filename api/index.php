<?php
/**
 * JSON Web Service for CYF App
 */
require_once dirname(__DIR__) . "/Dao/GlobalConfiguration.php";
require_once dirname(__DIR__) . "/vendor/autoload.php";

include_once '../cis/validate.php';

spl_autoload_register(function ($class_name) {
    $classes_dir = realpath(__DIR__) . DIRECTORY_SEPARATOR;
    $class_file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
    require_once $classes_dir . $class_file;
});

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
    'errorHandler' => function ($c) {
        return function ($request, $response, $exception) use ($c) {
            return $c['response']->withStatus(500)
                ->withHeader('Content-Type', 'application/json')
                ->withJson([
                    'code' => $exception->getCode(),
                    'trace' => $exception->getTraceAsString(),
                    'technicalMessage' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'message' => 'Something went wrong!'
                ]);
        };
    }
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
$container = $app->getContainer();

$container["jwt"] = function ($container) {
    return new StdClass;
};

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', \GlobalConfiguration::getFullAddressWithPort())
        ->withHeader('Access-Control-Allow-Credentials', 'true')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, access-control-allow-origin, Access-Control-Allow-Origin')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->add(function ($request, $response, $next) {
    if (!$request->isOptions()) {
        $response = $next($request, $response);
        $a = $response->getBody();
        $response->getBody()->rewind();
        $object = json_decode($response->getBody());
        $newBodyJson = [
            'detail' => $a,
            'status' => $response->getStatusCode(),
            'response' => isset($object->response) ? $object->response : $object,
            'message' => isset($object->message) ? $object->message : '',
        ];
        return $response->withJson($newBodyJson);
    }
    $response = $next($request, $response);
    return $response;
});

//$app->add(new \Slim\Middleware\JwtAuthentication([
//    "secure"    => false,
//    "secret" => GlobalConfiguration::JWT_KEY,
//    "algorithm" => ["HS256"],
//    "callback" => function ($request, $response, $arguments) use ($container) {
//        $container["jwt"] = $arguments["decoded"];
//        if (empty($container["jwt"]->id) || empty($container["jwt"]->email)) {
//            return false;
//        }
//    }
//]));


/*
GET /api/index.php/hello/world HTTP/1.1
Host: cyf-app.local
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyNyIsImVtYWlsIjoidGVwcG9AeWxpb3Bpc3RvLmZpIiwicHN3b3JkIjoiYTEyZUZBMWx0UEBzc1cwcmQiLCJvd25lcmdyb3VwcyI6IntcIlR1cmt1IFVuaXZlcnNpdHkgTWFuYWdlclwifSIsIm1lbWJlcmdyb3VwcyI6IntcIlR1cmt1IFVuaXZlcnNpdHkgU3R1ZGVudFwifSIsIm5hbWUiOiJUZXBwbyIsImxhbmciOiJlbiJ9.R-SXc-CizlpxHcfbdlyXkN4yo5yC0PK4DmwHWdoVXpw
Cache-Control: no-cache
Postman-Token: 0b78f5da-367f-fa9f-39ce-6e7ad1d45cb8
 */
//$app->get('/hello/{name}', function ($request, $response, $args) {
//    return $response->write(json_encode([$args['name'], $this->jwt]));
//});

$app->get('/events', App\Controllers\EventsController::class . ':get');
$app->get('/events/count', App\Controllers\EventsController::class . ':count');
$app->post('/events', App\Controllers\EventsController::class . ':createEvent');
$app->put('/events', App\Controllers\EventsController::class . ':updateEvent');
$app->delete('/events', App\Controllers\EventsController::class . ':deleteEvent');

$app->get('/subscriptions', App\Controllers\SubscriptionsController::class . ':get');
$app->get('/interests', App\Controllers\InterestsController::class . ':get');
$app->get('/groups', App\Controllers\GroupsController::class . ':get');
$app->post('/user', App\Controllers\UsersController::class . ':check');
$app->post('/user/change-lang', App\Controllers\UsersController::class . ':changeLang');
$app->get('/info', App\Controllers\InfoController::class . ':get');
$app->get('/info/search', App\Controllers\InfoController::class . ':search');

$app->post('/user-event', App\Controllers\UserEventController::class . ':add');
$app->delete('/user-event', App\Controllers\UserEventController::class . ':delete');

$app->get('/pois', App\Controllers\PoisController::class . ':get');
$app->post('/pois', App\Controllers\PoisController::class . ':create');
$app->put('/pois', App\Controllers\PoisController::class . ':update');
$app->delete('/pois', App\Controllers\PoisController::class . ':delete');

$app->get('/home', App\Controllers\HomeController::class . ':get');
$app->get('/lang', App\Controllers\LangController::class . ':get');

$app->run();
