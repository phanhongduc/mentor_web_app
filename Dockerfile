FROM php:5.6-apache
RUN apt-get update && apt-get install -y libpq-dev vim && docker-php-ext-install pdo pdo_pgsql pgsql && a2enmod rewrite && service apache2 restart
