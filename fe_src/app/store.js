import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';

const sagaMiddleware = createSagaMiddleware();

const sagaMiddlewareControl = (function sagaMiddlewareControlFunc(middleware) {
  const running = {};

  return {

    check(key) {
      return running[key] || false;
    },

    run(key, saga) {
      if (!running[key]) {
        middleware.run(saga);
        running[key] = true;
      }
    },
  };
}(sagaMiddleware));

const store = createStore(
  reducers,
  /* eslint-disable no-underscore-dangle */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware),
  /* eslint-enable */
);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./reducers', () => {
    const nextRootReducer = require('./reducers');
    store.replaceReducer(nextRootReducer);
  });
}

export {
  store,
  sagaMiddleware,
  sagaMiddlewareControl,
};
