import { pure, withProps } from 'recompose';
import merge from 'lodash.merge';

export default function withTheme(component, theme) {
  return pure(withProps(ownerProps =>
  ownerProps.theme ?
    { theme: merge(theme, ownerProps.theme) } :
    { theme })(component));
}

