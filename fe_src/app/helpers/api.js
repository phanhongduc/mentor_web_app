import merge from 'deepmerge';

function APIError(setOpts) {
  const defaultOpts = {
    message: 'Api Error',
    response: null,
  };

  const opts = Object.assign({}, defaultOpts, setOpts);

  this.name = 'APIError';
  this.code = opts.code;
  this.message = opts.message;
  this.response = opts.response;
}

APIError.prototype = Object.create(Error.prototype);

function fetchApi(url, opts, isTimeout) {
  const defaultOpts = {
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };

  const finalOpts = merge(defaultOpts, opts);


  const timeout = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new APIError({ message: 'Request time out' }));
    }, isTimeout ? 50 : 15000);
  });

  const call = fetch(url, finalOpts)
    .then(response => Promise.resolve(response))
    .then(resp => resp.json())
    .then((resp) => {
      if (resp.status && resp.status >= 200 && resp.status < 300) {
        return resp;
      }
      throw new APIError(resp);
    })
    .catch((e) => {
      if (e instanceof APIError) {
        throw new APIError(e);
      } else {
        throw new Error(e);
      }
    });

  return Promise
    .race([timeout, call])
    .then(res => res)
    .catch((e) => {
      if (e instanceof APIError) {
        throw new APIError(e);
      } else {
        throw new Error(e);
      }
    });
}

export { fetchApi, APIError };
