import React from 'react';
import PropType from 'prop-types';
import styled from 'styled-components';
import debounce from 'lodash.debounce';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchApi } from '../../helpers/api';

import config from '../../styledConfig';
import Header from './Header';
import IconHome from './icon/Home';
import IconUser from './icon/User';
import IconSearch from './icon/Search';

const Icon = styled.div`
  font-size: 60px;
  color: ${({ active }) => active ? '#000' : '#fff'}
`;

const IconUserWrap = styled(Icon)`
  font-size: 35px;
`;

const IconUserSearch = styled(Icon)`
  font-size: 35px;
`;

const SearchBoxIcon = styled.div`
  position: absolute;
  left: 5px;
  top: 6px;
  font-size: 20px;
`;

const SearchBox = styled.div`
  box-sizing: border-box;
  height: ${config.searchHeight}px;
  background-color: rgba(0,0,0,0.3);
  padding: 6px 0;
  position: fixed;
  top: ${config.headerHeight}px;
  z-index: ${config.zIndexMax - 1000};
  width: 100%;
  font-size: 20px;
`;

const SearchBoxContent = styled.div`
    position: relative;
    margin: 0 auto;
    width: 300px;
  > input {
    width: 100%;
    padding: 3px 15px 3px 30px;
    outline-offset: 0;
    border: 0;
    outline: 0;
  }
`;

class HeaderFinal extends React.PureComponent {

  componentWillMount = () => {
    this.searchKeyUp = debounce((e) => {
      fetchApi(`${process.env.API_URL}/info/search?keyword=${e.target.value}&limit=10&lang=${this.props.lang}`, {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      }).then((res) => {
        if (res.status === 200) {
          this.props.pushResult(res.response);
        }
      });
    }, 400);
  };

  componentWillUnmount = () => {
    this.searchKeyUp.cancel();
  };

  _onSearch = (e) => {
    e.persist();
    this.searchKeyUp(e);
  };

  _toogleSearchBox = () => {
    this.props.toogleSearchBox(!this.props.isOpenSearchBox);
  };

  render() {
    const { location, isOpenSearchBox } = this.props;
    return (
      <Header location={location}>
        <Link to="/">
          <Icon active={location.pathname === '/'}>
            <IconHome opacity={location.pathname === '/' ? 0.7 : 1} />
          </Icon>
        </Link>
        <IconUserSearch onClick={this._toogleSearchBox} active={isOpenSearchBox}>
          <IconSearch opacity={isOpenSearchBox ? 0.7 : 1} />
        </IconUserSearch>
        <Link to="/user">
          <IconUserWrap active={location.pathname === '/user'}>
            <IconUser opacity={location.pathname === '/user' ? 0.7 : 1} />
          </IconUserWrap>
        </Link>
        {
          isOpenSearchBox ? (
            <SearchBox>
              <SearchBoxContent>
                <SearchBoxIcon>
                  <IconSearch />
                </SearchBoxIcon>
                <input type="search" name="search" onKeyUp={this._onSearch} />
              </SearchBoxContent>
            </SearchBox>
          ) : ''
        }
      </Header>
    );
  }
}

HeaderFinal.propTypes = {
  lang: PropType.string,
  location: PropType.object,
  toogleSearchBox: PropType.func.isRequired,
  isOpenSearchBox: PropType.bool,
  pushResult: PropType.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  toogleSearchBox: (payload) => {
    dispatch({ type: 'TOGGLE_SEARCH_BOX', payload });
  },
  pushResult: (payload) => {
    dispatch({ type: 'SEARCH_RESULTS', payload });
  },
});

const mapStateToProps = state => ({
  lang: state.user.lang,
  isOpenSearchBox: state.search.isOpenSearchBox,
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderFinal);
