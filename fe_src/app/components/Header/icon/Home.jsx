import React from 'react';

const Home = ({ ...props }) => (
  <svg fill="currentColor" height="1em" {...props} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500">
    <path className="st0" d="M251.1 141.1L77.3 290.8l56-.1v138.4H217v-94.4h59.6v94.4h91.5v-139h63.8z" />
  </svg>
);

export default Home;
