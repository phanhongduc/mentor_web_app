import React from 'react';

const Search = ({ ...props }) => (
  <svg fill="currentColor" height="1em" {...props} xmlns="http://www.w3.org/2000/svg" viewBox="1949 9 35.8 34.9">
    <g>
      <path id="Path_2" d="M1953.066 12.965a13.3 13.3 0 0 0 0 19.14 14.07 14.07 0 0 0 17.493 1.7l10.334 10.1 3.9-3.813-10.333-10.035a13.36 13.36 0 0 0-1.737-17.022 14.083 14.083 0 0 0-19.66-.07zm15.686 15.326a8.472 8.472 0 0 1-11.783 0 8.01 8.01 0 0 1 0-11.51 8.472 8.472 0 0 1 11.78 0 8.01 8.01 0 0 1 0 11.513z" />
    </g>
  </svg>
);

export default Search;
