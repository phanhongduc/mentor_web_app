import React from 'react';

const User = ({ ...props }) => (
  <svg fill="currentColor" height="1em" {...props} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37.68 47.03">
    <g>
      <g>
        <g id="Person">
          <circle className="cls-1" cx="18.84" cy="10.22" r="10.22" />
          <path className="cls-1" d="M37.68 37.61V47H0v-9.39a26.62 26.62 0 0 1 37.68 0z" />
        </g>
      </g>
    </g>
  </svg>
);

export default User;
