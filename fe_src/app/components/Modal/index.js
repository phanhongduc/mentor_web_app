import styled from 'styled-components';
import ModalBase from './Modal';
import styledConfig from '../../styledConfig';

const ModalBody = styled.div`
  ${({ full }) => full ? '' :
  `
  padding-top: 13px;
  padding-bottom: 17px;
  padding-left: 17px;
  padding-right: 17px;
  
  `}
`;

const ModalFooter = styled.footer`
  padding: 9px 20px;
  border-top: 1px solid ${styledConfig.colorBorder};
`;

export {
  ModalBase,
  ModalBody,
  ModalFooter,
};
