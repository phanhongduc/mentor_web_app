import styled from 'styled-components';
import styledConfig from '../../styledConfig';

function getBgColor(props) {
  if (props.danger) {
    return styledConfig.colorError;
  }

  if (props.light) {
    return '#95989A';
  }

  return '#666';
}
function checkDisplay({ hidden, display }) {
  if (hidden) return 'none';
  return display || 'inline-block';
}

const Button = styled.button`
  display: ${checkDisplay};
  min-width: ${props => props.width ? props.width : '100px'};
  border-radius: 5px;
  padding: ${props => props.big ? '3px 20px' : '10px 20px'};
  background-color:  ${getBgColor};
  border: 0;
  color: white;
  font-size: ${props => props.big ? '31px' : '20px'};
  font-family: ${styledConfig.fontFamily};
  outline: none;
`;

export default Button;
