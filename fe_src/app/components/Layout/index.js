import styled from 'styled-components';

const Spacing = styled.div`
  ${props => props.display ? `display:${props.display}` : ''};
  ${props => props.top ? `margin-top:${props.top}` : ''};
  ${props => props.bottom ? `margin-top:${props.bottom}` : ''};
  ${props => props.left ? `margin-left:${props.left}` : ''};
  ${props => props.right ? `margin-right:${props.right}` : ''};
`;

const CenterLR = styled.div`
  text-align: center;
  margin-left: auto;
  margin-right: auto;
`;

export { Spacing, CenterLR };
