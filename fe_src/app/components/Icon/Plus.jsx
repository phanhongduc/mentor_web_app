import React from 'react';

const Plus = ({ ...props }) => (
  <svg {...props} xmlns="http://www.w3.org/2000/svg" viewBox="345 3088 15 15" fill="currentColor" height="1em">
    <g id="Plus_1_" transform="translate(473 3222)">
      <path id="Path_32" d="M8.5,8.5V15h-2V8.5H0v-2H6.5V0h2V6.5H15v2Z" transform="translate(-128 -134)" />
    </g>
  </svg>
);

export default Plus;
