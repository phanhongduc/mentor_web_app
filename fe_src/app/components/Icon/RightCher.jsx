import React from 'react';

const RightCher = ({ ...props }) => (
  <svg {...props} xmlns="http://www.w3.org/2000/svg" viewBox="3865 1635 11.261 17.5" fill="currentColor" height="1em">
    <path className="cls-1" d="M3.4,16.79a1.706,1.706,0,0,1-2.739,0,1.706,1.706,0,0,1,0-2.739L5.225,9.486a1.382,1.382,0,0,0,0-1.522L.66,3.4A1.706,1.706,0,0,1,.66.66,1.706,1.706,0,0,1,3.4.66l7.609,7.609a1.382,1.382,0,0,1,0,1.522Z" transform="translate(3865.025 1635.025)" />
  </svg>
);

export default RightCher;
