import IconPlus from './Plus';
import IconStar from './Star';
import IconHeart from './Heart';
import MapMarker from './MapMarker';
import IconCancel from './Cancel';
import IconUser from './User';
import IconMagnifyingGlass from './MagnifyingGlass';
import IconMinus from './Minus';
import IconRightCher from './RightCher';
import IconCurrentLocation from './CurrentLocation';
import IconPlaces from './Places';

export {
  IconCurrentLocation,
  IconRightCher,
  IconMinus,
  IconUser,
  IconPlaces,
  IconPlus,
  IconStar,
  IconHeart,
  MapMarker,
  IconCancel,
  IconMagnifyingGlass,
};
