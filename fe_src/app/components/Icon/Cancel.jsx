import React from 'react';

const Cancel = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="934.207 3869.167 16.667 16.667" fill="currentColor" height="1em">
    <g transform="translate(934.207 3869.167)">
      <path id="Path_40" d="M10.3,8.31l6.371,6.371-1.985,1.985L8.31,10.3,1.939,16.667,0,14.681,6.371,8.31,0,1.939,1.939,0,8.31,6.371,14.681,0l1.985,1.939Z" />
    </g>
  </svg>
);

export default Cancel;
