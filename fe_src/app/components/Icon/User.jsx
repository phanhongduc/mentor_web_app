import React from 'react';
import PropTypes from 'prop-types';

const User = ({ height }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="2094.2 3036.5 28.32 35.4" fill="currentColor" height={height || '1em'}>
    <g transform="translate(2060.5 3007.5)">
      <path id="Path_67" className="cls-1" d="M7.683,0A7.683,7.683,0,1,1,0,7.683,7.683,7.683,0,0,1,7.683,0Z" transform="translate(40.177 29)" />
      <path id="Path_1" className="cls-1" d="M62.02,64.675v7.08H33.7v-7.08A20,20,0,0,1,62.02,64.675Z" transform="translate(0 -7.355)" />
    </g>
  </svg>
);

User.propTypes = {
  height: PropTypes.string,
};

export default User;
