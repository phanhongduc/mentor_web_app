import React from 'react';

const Places = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="52 3857 32 32" height="1em">
    <g transform="translate(97.7 3517.6)">
      <circle fill="#2cb036" stroke="#fff" strokeMiterlimit="10" strokeWidth="2px" cx="15" cy="15" r="15" transform="translate(-44.7 340.4)" />
      <path fill="#fff" d="M-1.335,366.951a33.4,33.4,0,0,1-5.4.635,11.612,11.612,0,0,1-2.91-.608v-.5a1.29,1.29,0,0,0,.529-1.032,1.242,1.242,0,0,0-1.243-1.243,1.242,1.242,0,0,0-1.243,1.243,1.29,1.29,0,0,0,.529,1.032V382a.711.711,0,0,0,.714.714A.711.711,0,0,0-9.642,382v-6.3a8.448,8.448,0,0,0,2.143.344c2.54,0,3.677-.9,6.323-1.085a7.863,7.863,0,0,1,3.148.9V367.11A11.978,11.978,0,0,0-1.335,366.951Z" transform="translate(-24.343 -17.504)" />
    </g>
  </svg>
);

export default Places;
