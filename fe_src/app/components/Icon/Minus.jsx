import React from 'react';

const Minus = ({ ...props }) => (
  <svg {...props} xmlns="http://www.w3.org/2000/svg" viewBox="1375 1870.468 15 2" fill="currentColor" width="1em">
    <g id="Minus_1_" transform="translate(1375 1870.468)">
      <rect className="cls-1" width="15" height="2" />
    </g>
  </svg>
);

export default Minus;
