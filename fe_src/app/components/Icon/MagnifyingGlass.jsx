import React from 'react';

const MagnifyingGlass = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="936 3917 21 20.472" fill="currentColor" height="1em">
    <g transform="translate(1061.025 3798.013)">
      <path d="M2.36,2.313a7.8,7.8,0,0,0,0,11.228,8.253,8.253,0,0,0,10.261.994l6.064,5.925,2.29-2.237-6.064-5.883a7.836,7.836,0,0,0-1.018-9.985A8.261,8.261,0,0,0,2.36,2.313Zm9.2,8.991a4.97,4.97,0,0,1-6.912,0,4.7,4.7,0,0,1,0-6.753,4.97,4.97,0,0,1,6.912,0A4.7,4.7,0,0,1,11.562,11.3Z" transform="translate(-125 119)" />
    </g>
  </svg>
);

export default MagnifyingGlass;
