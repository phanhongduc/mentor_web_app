import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { injectIntl, intlShape } from 'react-intl';
import query from 'query-string';
import config from '../../styledConfig';
import IconCity from './icon/City';
import IconStudy from './icon/Study';
import IconStudyActive from './icon/StudyActive';
import IconEvents from './icon/Events';
import IconMap from './icon/Maps';
import footerLocales from './footerLocales';

const FooterBase = styled.footer`
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  height: ${config.footerHeight}px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  z-index: 99;
`;

const FooterHome = FooterBase.extend`
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover; 
  background-image: url("${require('./img/footer.png')}");
  
  @media (min-width: 600px) {
    background-image: url("${require('./img/footer@2x.png')}");
  }
  
  @media (min-width: 1000px) {
    background-image: url("${require('./img/footer@3x.png')}");
  }
`;

const FooterUser = FooterBase.extend`
  background-color: #a50082;
`;

const FooterEvents = FooterBase.extend`
  background-color: #14AA3C;
`;

const FooterMap = FooterBase.extend`
  background-color: #C80041;
`;

const Text = styled.div`
  color: #fff;
  font-size: 20px;
`;

const IconWrap = styled.div`
  display: flex;
  border-radius: 50%;
  font-size: 30px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const IconButton = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
  a {
    text-decoration: none;
  }
  
  ${Text} {
    display: ${({ active }) => active ? 'block' : 'none'};
  }
  
  ${IconWrap} {
    margin: 0 auto;
    background-color: ${({ active }) => active ? 'rgba(0,0,0,.5)' : '#fff'};
    width: ${({ active }) => active ? '33px' : '50px'};
    height: ${({ active }) => active ? '33px' : '50px'};
    color: ${({ active }) => active ? '#fff' : '#656565'};
  }
`;

function getActive(location, path, type) {
  if (type === 'page') {
    const queryParse = query.parse(location.search);

    return queryParse.page && queryParse.page.toLowerCase() === path;
  }

  if (path === 'info') {
    const queryParse = query.parse(location.search);
    return (location.pathname.split('/')[1] === path)
      && (!queryParse.page || queryParse.page.toLowerCase() !== 'study');
  }

  return (location.pathname.split('/')[1] === path);
}

const Footer = ({ location, intl }) => {
  let FooterRender;
  switch (location.pathname.split('/')[1]) {
    case 'user':
      FooterRender = FooterUser;
      break;
    case 'events':
      FooterRender = FooterEvents;
      break;
    case 'map':
      FooterRender = FooterMap;
      break;
    default:
      FooterRender = FooterHome;
      break;
  }

  return (
    <FooterRender>
      <IconButton active={getActive(location, 'info')}>
        <Link to="/info">
          <IconWrap>
            <IconCity width="80%" />
          </IconWrap>
          <Text>{intl.formatMessage(footerLocales.city)}</Text>
        </Link>
      </IconButton>
      <IconButton active={getActive(location, 'study', 'page')}>
        <Link to="/info/detail?page=Study">
          <IconWrap>
            {
              getActive(location, 'study', 'page') ?
                (<IconStudyActive width="100%" />) :
                (<IconStudy width="100%" />)
            }
          </IconWrap>
          <Text>{intl.formatMessage(footerLocales.study)}</Text>
        </Link>
      </IconButton>
      <IconButton active={getActive(location, 'events')}>
        <Link to="/events">
          <IconWrap>
            <IconEvents width="75%" />
          </IconWrap>
          <Text>{intl.formatMessage(footerLocales.events)}</Text>
        </Link>
      </IconButton>
      <IconButton active={getActive(location, 'map')}>
        <Link to="/map">
          <IconWrap>
            <IconMap width="80%" />
          </IconWrap>
          <Text>{intl.formatMessage(footerLocales.map)}</Text>
        </Link>
      </IconButton>
    </FooterRender>
  );
};

Footer.propTypes = {
  location: PropTypes.object,
  intl: intlShape.isRequired,
};

export default injectIntl(Footer);
