import { defineMessages } from 'react-intl';

const messages = defineMessages({
  city: {
    id: 'footer.city',
    defaultMessage: 'City',
  },
  study: {
    id: 'footer.study',
    defaultMessage: 'Study',
  },
  events: {
    id: 'footer.events',
    defaultMessage: 'Events',
  },
  map: {
    id: 'footer.map',
    defaultMessage: 'Map',
  },
});

export default messages;
