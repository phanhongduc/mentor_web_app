import { HOCInput } from 'react-hoc-form-validatable';
import InputValidate from './InputValidate';
import Input from './Input';
import DatePicker from './DatePicker';
import TextArea from './TextArea';
import Select from './Select';

const InputValidatable = HOCInput(InputValidate(Input));
const SelectValidatable = HOCInput(InputValidate(Select));
const TextAreaValidatable = HOCInput(InputValidate(TextArea));
const DatePickerValidatable = HOCInput(InputValidate(DatePicker));

export {
  Input,
  InputValidatable,
  SelectValidatable,
  TextAreaValidatable,
  DatePicker,
  DatePickerValidatable,
  TextArea,
  Select,
};
