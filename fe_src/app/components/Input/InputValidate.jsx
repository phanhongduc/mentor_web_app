import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';

function getErrorMessage(error, lang) {
  if (!error && !lang) return '';
  if (typeof error === 'string') return error;
  return (error && error[lang]) ? error[lang] : '';
}

const InputValidatable = (ComponentNeedWrap) => {
  const InputValidate = (
    { className,
      multiple,
      value,
      label,
      validated,
      error,
      dirty,
      pending,
      errorMessage,
      lang,
      type,
      name,
      onChange,
      onBlur,
      disabled,
      submitted,
      style,
      data,
      hidden,
      displayFormat,
    }) =>
    (
      <ComponentNeedWrap
        displayFormat={displayFormat}
        hidden={hidden}
        multiple={multiple}
        controlled
        data={data}
        style={style}
        disabled={disabled || submitted}
        className={className}
        value={value}
        label={label}
        valid={Boolean(
          validated && !error && dirty && !pending)
        }
        pending={pending}
        error={error}
        errorMessage={getErrorMessage(errorMessage, lang)}
        type={type}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
      />
    );

  InputValidate.propTypes = {
    data: PropTypes.array,
    lang: PropTypes.string,
    displayFormat: PropTypes.string,
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    label: PropTypes.string,
    name: PropTypes.string,
    error: PropTypes.bool,
    dirty: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string,
    ]),
    validated: PropTypes.bool,
    submitted: PropTypes.bool,
    disabled: PropTypes.bool,
    pending: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.object,
      PropTypes.string,
    ]),
    style: PropTypes.object,
    multiple: PropTypes.bool,
    hidden: PropTypes.bool,
  };

  return InputValidate;
};


export default InputValidatable;
