import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Spinner from './Spinner';

const LP = styled.div`
  position: fixed;
  z-index: 999999999999;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.4);
  height: 100vh;
  display: flex;
  align-items: center;
`;

const LoadingPage = ({ spinnerSize }) => (
  <LP>
    <Spinner size={spinnerSize} />
  </LP>
);

LoadingPage.propTypes = {
  spinnerSize: PropTypes.string,
};

export default LoadingPage;
