import React from 'react';
import PropTypes from 'prop-types';
import { UnmountClosed } from 'react-collapse';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import styled from 'styled-components';
import config from '../../styledConfig';
import * as Icon from './icon';
import infoLocales from './infoLocales';

function getFileName(title) {
  return title.toLowerCase().split(' ').join('');
}

const Wrap = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: 100vh;
  padding-bottom: ${config.footerHeight}px;
  padding-top: ${config.headerHeight}px;
`;

const GroupTop = styled.section`
  height: 100%;
  overflow: auto;
`;

const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #000;
  opacity: .3;
`;

const Title = styled.div`
  color: white;
  font-size: 37px;
  position: relative;
  z-index: 1;
`;

const TopInfo = styled.section`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 33.33%;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover;
  background-image: ${({ title }) => `url("${require(`./img/${getFileName(title)}.jpg`)}")`};
  
  ${config.mediaMedium} {
    background-image: ${({ title }) => `url("${require(`./img/${getFileName(title)}@2x.jpg`)}")`};
  }
  
  ${config.mediaLarge} {
    background-image: ${({ title }) => `url("${require(`./img/${getFileName(title)}@3x.jpg`)}")`};
  }
`;

const ChildInfoSection = styled.ul`
  box-sizing: border-box;
  padding-bottom: 12px;
  padding-left: 14px;
  padding-right: 14px;
  margin: 0;
  list-style-type: none;
  display: flex;
  flex-wrap: wrap;
  overflow: hidden;
`;

const ChildInfo = styled.li`
  margin-top: 12px;
  text-align: center;
  width: ${({ width }) => width || '33.33%'};
  font-size: 20px;
  
  a {
    text-decoration: none;
    color: ${config.colorText}
  }
`;

const ChildInfoText = styled.div`
  margin-top: 3px;
  font-size: 19px;
`;

const ChildIcon = styled.div`
  width: ${({ width }) => width || '65px'};
  height: ${({ width }) => width || '65px'};
  margin: 0 auto;
  color: ${config.colorText};
  font-size: 65px;
`;

const ChildIconHold = ({ page, text, IconComponent, IconSrc, width, widthIcon }) => (
  <ChildInfo width={width}>
    <Link to={`/info/detail?page=${page}`}>
      <ChildIcon width={widthIcon}>
        {
          IconComponent ? (<IconComponent />) : ''
        }
        {
          IconSrc ? (<img src={IconSrc} alt={text} />) : ''
        }
      </ChildIcon>
      <ChildInfoText>
        {text}
      </ChildInfoText>
    </Link>
  </ChildInfo>

);

ChildIconHold.propTypes = {
  text: PropTypes.string,
  page: PropTypes.string,
  IconComponent: PropTypes.func,
  IconSrc: PropTypes.string,
  width: PropTypes.string,
  widthIcon: PropTypes.string,
};

class Info extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpenDaily: false,
      isOpenHobbies: false,
      isOpenFood: false,
    };
  }

  _toggleHobbies = (e) => {
    cancelAnimationFrame(this.ani);
    e.persist();
    const cur = e.currentTarget;
    this.setState({
      [cur.getAttribute('data-setState')]: !this.state[cur.getAttribute('data-setState')],
    }, () => {
      setTimeout(() => {
        if (this.state[cur.getAttribute('data-setState')]) {
          const scrollPosition = cur.offsetTop - 50;
          const currentPos = this.$content.scrollTop;
          this.ani = requestAnimationFrame(
            this._animationScrollTo.bind(
              '',
              currentPos <= scrollPosition ? 'top' : 'bottom',
              this.$content,
              scrollPosition,
              currentPos,
            ),
          );
        }
      }, 550);
    });
  };

  _animationScrollTo = (type, element, toPosition, current) => {
    if (
      (current <= toPosition && type === 'bottom') ||
      (current >= toPosition && type === 'top')
    ) {
      cancelAnimationFrame(this.ani);
      element.scrollTop = toPosition;
    } else {
      const currentPosition = type === 'bottom' ? current - 10 : current + 10;
      element.scrollTop = currentPosition;
      this.ani = requestAnimationFrame(
        this._animationScrollTo.bind('', type, element, toPosition, currentPosition),
      );
    }
  };

  render() {
    const intl = this.props.intl;
    return (
      <Wrap>
        <GroupTop innerRef={(ele) => { this.$content = ele; }}>
          <TopInfo title="EVERYDAY LIFE" onClick={this._toggleHobbies} data-setState={'isOpenDaily'}>
            <Overlay />
            <Title>{intl.formatMessage(infoLocales.everyDayLife)}</Title>
          </TopInfo>
          <UnmountClosed isOpened={this.state.isOpenDaily}>
            <ChildInfoSection>
              <ChildIconHold
                text={intl.formatMessage(infoLocales.transport)}
                page="Transport"
                IconComponent={Icon.Transport}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.shopping)}
                page="Shopping"
                IconComponent={Icon.Shopping}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.groceries)}
                page="Groceries"
                IconComponent={Icon.Groceries}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.housing)}
                page="Housing"
                IconComponent={Icon.Housing}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.services)}
                page="Services"
                IconComponent={Icon.PublicOffices}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.heath)}
                page="Health"
                IconComponent={Icon.Health}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.fun)}
                page="Entertainment"
                IconComponent={Icon.Entertainment}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.safety)}
                page="Safety"
                IconComponent={Icon.Safety}
              />
              <ChildIconHold
                text={intl.formatMessage(infoLocales.religion)}
                page="Religion"
                IconComponent={Icon.Religion}
              />
            </ChildInfoSection>
          </UnmountClosed>
          <TopInfo title="Hobbies" onClick={this._toggleHobbies} data-setState={'isOpenHobbies'}>
            <Overlay />
            <Title>{intl.formatMessage(infoLocales.hobbies)}</Title>
          </TopInfo>
          <UnmountClosed isOpened={this.state.isOpenHobbies}>
            <ChildInfoSection>
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                text={intl.formatMessage(infoLocales.sports)}
                page="Sports"
                IconSrc={require('./icon/sports.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Music"
                text={intl.formatMessage(infoLocales.music)}
                IconSrc={require('./icon/music.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Arts and Crafts"
                text={intl.formatMessage(infoLocales.artCraft)}
                IconSrc={require('./icon/art&crafts.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Outdoors"
                text={intl.formatMessage(infoLocales.outdoors)}
                IconSrc={require('./icon/outdoors.svg')}
              />
            </ChildInfoSection>
          </UnmountClosed>
          <TopInfo title="Food & Drinks" onClick={this._toggleHobbies} data-setState={'isOpenFood'}>
            <Overlay />
            <Title>{intl.formatMessage(infoLocales.foodDrink)}</Title>
          </TopInfo>
          <UnmountClosed isOpened={this.state.isOpenFood}>
            <ChildInfoSection>
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                text={intl.formatMessage(infoLocales.studentLunch)}
                page="Student lunches"
                IconSrc={require('./icon/studentlunch.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Restaurants"
                text={intl.formatMessage(infoLocales.restaurants)}
                IconSrc={require('./icon/restaurant.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Cafes"
                text={intl.formatMessage(infoLocales.cafes)}
                IconSrc={require('./icon/cafes.svg')}
              />
              <ChildIconHold
                width="50%"
                widthIcon="90px"
                page="Nightlife"
                text={intl.formatMessage(infoLocales.nightlife)}
                IconSrc={require('./icon/nightlife.svg')}
              />
            </ChildInfoSection>
          </UnmountClosed>
        </GroupTop>
      </Wrap>
    );
  }
}

export default injectIntl(Info);
