import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'poi.title',
    defaultMessage: 'Points of interest',
  },
  addPoi: {
    id: 'poi.addPoi',
    defaultMessage: 'Add POI',
  },
  editPoi: {
    id: 'poi.editPoi',
    defaultMessage: 'Edit POI',
  },
  mustHaveLocation: {
    id: 'poi.mustHaveLocation',
    defaultMessage: 'POI must have a location, Please choose on the map.',
  },
  deleteSuccess: {
    id: 'poi.deleteSuccess',
    defaultMessage: 'Delete POI success!',
  },
  editSuccess: {
    id: 'poi.editSuccess',
    defaultMessage: 'Edit POI success!',
  },
  createSuccess: {
    id: 'poi.createSuccess',
    defaultMessage: 'Create POI success!',
  },
  confirmDelete: {
    id: 'poi.confirmDelete',
    defaultMessage: 'Are you sure you want to delete this place?',
  },
});

export default messages;
