import React from 'react';
import styled from 'styled-components';
import { HOCForm } from 'react-hoc-form-validatable';
import { injectIntl, intlShape } from 'react-intl';
import PropTypes from 'prop-types';
import { Spacing } from '../../../components/Layout/';
import { form as formLocales, global as globalLocales } from '../../../globalLocales';
import eventLocales from '../../Events/eventLocales';
import MapModal from '../../Events/modals/MapModal';
import Button from '../../../components/Button';
import { InputValidatable, TextAreaValidatable, SelectValidatable, DatePickerValidatable, Input } from '../../../components/Input';


const FormAction = styled.div`
  display: flex;
  
  ${Button} {
    width: 100%;
    border-radius: 0;
  }
  
  ${Button}:first-child {
     border-bottom-left-radius: 5px;
  }
  
  ${Button}:last-child {
     border-bottom-right-radius: 5px;
  }
`;

const Form = styled.div`
  padding: 15px 15px 0 15px;
`;

class AddPoiForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpenSelectAddress: false,
      mapAddress: '',
      addressLat: null,
      addressLng: null,
    };
  }

  _openSelectAddress = () => {
    this.setState({
      isOpenSelectAddress: true,
    });
  };

  _closeSelectAddress = () => {
    this.setState({
      isOpenSelectAddress: false,
    });
  };

  _selectAddress = (e) => {
    this.setState({
      isOpenSelectAddress: false,
      mapAddress: e.address,
      addressLat: e.lat,
      addressLng: e.lng,
    });
    this.props.chooseMap({
      mapAddress: e.address,
      addressLat: e.lat,
      addressLng: e.lng,
    });
  };

  _clearLocation = () => {
    this.setState({
      isOpenSelectAddress: false,
      mapAddress: null,
      addressLat: null,
      addressLng: null,
    });
    this.props.clearAddToPoi();
  };

  _submit = (inputs, done) => {
    this.props.onSubmit(inputs, done, {
      address: this.state.mapAddress,
      lat: this.state.addressLat,
      lng: this.state.addressLng,
    });
  };

  render() {
    const intl = this.props.intl;
    const poi = this.props.poi;
    const addToPoi = this.props.addToPoi;
    return (
      <form
        noValidate
        onSubmit={this._submit}
      >
        <Form>
          <InputValidatable
            defaultValue={poi ? poi.poiname : undefined}
            name="name"
            label={intl.formatMessage(formLocales.name)}
            rule="notEmpty"
            type="text"
          />
          <Spacing top="10px" />
          {
            addToPoi ? (
              <Input
                type="text"
                label={intl.formatMessage(formLocales.location)}
                value={addToPoi.mapAddress}
                disabled
              />
            ) : ''
          }
          {
            this.state.mapAddress ?
              (
                <Input
                  type="text"
                  label={intl.formatMessage(formLocales.location)}
                  value={this.state.mapAddress}
                  disabled
                />
              ) :
              (
                <InputValidatable
                  hidden={!!addToPoi}
                  disabled
                  defaultValue={
                    poi && poi.locat ? poi.locat : undefined}
                  label={intl.formatMessage(formLocales.location)}
                  name="location"
                  type="text"
                />
              )
          }
          {
            this.state.mapAddress || addToPoi ?
              (<Button type="button" onClick={this._clearLocation}>
                {intl.formatMessage(eventLocales.clearLocation)}
              </Button>) :
              (<Button type="button" onClick={this._openSelectAddress}>
                {intl.formatMessage(eventLocales.chooseOnMap)}
              </Button>)
          }
          <Spacing top="10px" />
          <TextAreaValidatable
            defaultValue={poi ? poi.descr : undefined}
            type="textarea"
            name="description"
            rows="4"
            label={intl.formatMessage(formLocales.description)}
          />
          <Spacing top="10px" />
          <InputValidatable
            defaultValue={poi ? poi.aday : undefined}
            label={intl.formatMessage(formLocales.openingTimes)}
            name="openingTimes"
            type="text"
          />
          <Spacing top="10px" />
          <InputValidatable
            defaultValue={poi ? poi.url : undefined}
            label={intl.formatMessage(formLocales.url)}
            name="url"
            type="text"
          />
          <Spacing top="10px" />
          <InputValidatable
            label={intl.formatMessage(formLocales.contact)}
            name="contact"
            type="text"
            defaultValue={poi ? poi.contact : undefined}
          />
          <Spacing top="10px" />
          <InputValidatable
            defaultValue={poi ? poi.tel : undefined}
            label={intl.formatMessage(formLocales.tel)}
            name="tel"
            type="text"
          />
          <Spacing top="10px" />
        </Form>
        <FormAction>
          <Button
            display="block"
            disabled={this.props.submitted}
            light
            type="button"
            onClick={this.props.onRequestClose}
          >
            {this.props.intl.formatMessage(globalLocales.cancel)}
          </Button>
          <Button display="block" disabled={this.props.submitted} type="submit">
            {this.props.intl.formatMessage(globalLocales.save)}
          </Button>
        </FormAction>
        {
          this.state.isOpenSelectAddress ? (
            <MapModal
              onChoose={this._selectAddress}
              isOpen={this.state.isOpenSelectAddress}
              onRequestClose={this._closeSelectAddress}
            />
          ) : null
        }
      </form>
    );
  }
}

AddPoiForm.propTypes = {
  poi: PropTypes.object,
  intl: intlShape.isRequired,
  onSubmit: PropTypes.func,
  chooseMap: PropTypes.func,
  onRequestClose: PropTypes.func,
  clearAddToPoi: PropTypes.func,
  submitted: PropTypes.bool,
};

export default injectIntl(HOCForm(AddPoiForm));
