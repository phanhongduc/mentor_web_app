import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import trunc from 'trunc-html';
import { Link } from 'react-router-dom';
import { UnmountClosed } from 'react-collapse';
import { pure, withHandlers } from 'recompose';
import Button from '../../components/Button';
import config from '../../styledConfig';


const WrapAll = styled.div`
  border-bottom: 1px solid ${config.colorBorder};
  overflow: hidden;
`;

const Wrap = styled.div`
  padding: 12px 16px;
  position: relative;
`;

const WrapWithBg = Wrap.extend`
  color: #fff;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover;
  background-image: ${({ bg }) => `url("${bg}")`};
`;

const Intro = styled.div`
  margin-bottom: 10px;
  position: relative;
  z-index: 1;
`;

const Detail = styled.div`
  font-size: 18px;
  box-sizing: border-box;
  padding: 20px 16px 20px 16px;
`;

const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #000;
  opacity: .3;
`;

const Title = styled.div`
  font-size: 41px;
  margin-bottom: 5px;
`;

const IntroText = styled.div`
  font-size: 18px;
`;

const TextDetail = styled.div`
  font-size: 18px;
  margin-bottom: 10px;
`;

const TextDetailTimes = styled(TextDetail)`
  display: flex;
  flex-direction: row;
  
  span:first-child {
    margin-right: 13px;
  }
`;

const ShowOnMap = styled.div`
  text-align: right;
  margin-bottom: 5px;
`;

const Actions = styled.div`
  button {
    margin-left: 5px;
  }
`;

function formatLongDesc(text) {
  return {
    __html: trunc(text, 120).html,
  };
}

const PoiDisplay = (
  {
    description,
    url,
    tel,
    contact,
    isOwned,
    isOpened,
    onClick,
    onClickDelete,
    onClickEdit,
    openTimes,
    address,
    descr,
    lng,
    lat,
    bg,
    title,
    textEdit,
    textDelete,
    textShowOnMap,
  },
) => {
  const GirdIntro = bg ? WrapWithBg : Wrap;
  return (
    <WrapAll>
      <GirdIntro bg={bg} onClick={onClick}>
        {bg ? (<Overlay />) : ''}
        <Intro>
          <Title>
            {title}
          </Title>
          <IntroText hidden={isOpened}>
            {address}
          </IntroText>
          <IntroText hidden={isOpened}>
            {openTimes}
          </IntroText>
        </Intro>
      </GirdIntro>
      <UnmountClosed isOpened={(isOpened === true || isOpened === false) ? isOpened : false}>
        <Detail>
          {
            openTimes ? (
              <TextDetailTimes>
                <span>Open:</span> <span>{openTimes}</span>
              </TextDetailTimes>
            ) : null
          }
          {
            descr ? (
              <div dangerouslySetInnerHTML={formatLongDesc(descr)} />
            ) : null
          }
          <TextDetail>{description}</TextDetail>
          <TextDetail>{address}</TextDetail>
          {
            lng && lat ? (
              <ShowOnMap>
                <Link to={`/map?poi=${title}&lat=${lat}&longt=${lng}`}><Button>{textShowOnMap}</Button></Link>
              </ShowOnMap>
            ) : null
          }
          {
            url ? (
              <TextDetail><a href={url} target="_blank">{url}</a></TextDetail>
            ) : null
          }
          {
            isOwned && (
              <Actions>
                <Button danger onClick={onClickDelete}>{textDelete}</Button>
                <Button onClick={onClickEdit}>{textEdit}</Button>
              </Actions>
            )
          }
        </Detail>
      </UnmountClosed>
    </WrapAll>
  );
};

PoiDisplay.propTypes = {
  onClick: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  onClickEdit: PropTypes.func.isRequired,
  bg: PropTypes.string,
  title: PropTypes.string,
  textEdit: PropTypes.string,
  textDelete: PropTypes.string,
  textShowOnMap: PropTypes.string,
};

const PoiDisplayComposed = withHandlers({
  onClick: props => () => {
    props.onClickIntro(props.id);
  },
  onClickEdit: props => () => {
    props.onClickEdit(props.index);
  },
  onClickDelete: props => () => {
    props.onClickDelete(props.index);
  },
})(PoiDisplay);

export default PoiDisplayComposed;
