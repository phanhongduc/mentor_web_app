import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { injectIntl, intlShape, FormattedPlural } from 'react-intl';
import { fetchApi } from '../../helpers/api';
import homeLocales from './homeLocales';
import config from '../../styledConfig';

const Wrap = styled.main`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: 100vh;
  padding-top: 50px;
  padding-bottom: 70px;
`;

const BlockTop = styled.section`
  position: relative;
  height: 56%;
  width: 100%;
  
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover; 
  background-image: ${({ bg }) => `url("${bg.small}")`};
  
  display: flex;
  align-items: center;
  justify-content: flex-end;
  
  ${config.mediaMedium} {
    background-image: ${({ bg }) => `url("${bg.medium}")`};
  }
  
  ${config.mediaLarge} {
    background-image: ${({ bg }) => `url("${bg.large}")`};
  }
`;

const BlockSide = styled.section`
  position: relative;
  height: 100%;
  width: 50%;
  
  display: flex;
  align-items: center;
  justify-content: flex-start;
  
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover; 
  background-image: ${({ bg }) => `url("${bg.small}")`};
  
  ${config.mediaMedium} {
    background-image: ${({ bg }) => `url("${bg.medium}")`};
  }
  
 ${config.mediaLarge} {
    background-image: ${({ bg }) => `url("${bg.large}")`};
  }
`;

const WrapBlockSide = styled.div`
  display: flex;
  height: 44%;
`;

const Title = styled.div`
  position: relative;
  z-index: 1;
  padding: ${({ padding }) => `${padding}`};
  font-family: arial,sans-serif;
  font-size: ${({ size }) => `${size}px`};
  color: #fff;
  text-align: ${({ align }) => `${align}`};
`;

const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #000;
  opacity: .3;
`;

class Home extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      top: null,
      currentTop: 0,
    };
  }

  componentDidMount = () => {
    try {
      Promise.all(
        [
          `${process.env.API_URL}/events/count`,
          `${process.env.API_URL}/home`,
        ]
        .map(
          url => fetchApi(
            url,
            {
              headers: {
                'Content-Type': 'application/json',
              },
              method: 'get',
            }),
      )).then((datas) => {
        const dataUp = datas[0].response;
        this.setState({
          upcoming: dataUp && dataUp.length > 0 ? parseInt(dataUp[0].count, 10) : 0,
          top: datas[1].response.top,
        }, () => {
          this.timer = setInterval(this._runTop, 5000);
        });
      });
    } catch (e) {
      // this.props.addNotification({
      //   message: e.message,
      //   level: 'error',
      // });
    }
  };

  componentWillUnmount = () => {
    if (this.timer) {
      clearInterval(this.timer);
    }
  };

  _runTop = () => {
    if (this.state.top) {
      if (this.state.currentTop + 1 === this.state.top.length) {
        this.setState({
          currentTop: 0,
        });
      } else {
        this.setState({
          currentTop: this.state.currentTop + 1,
        });
      }
    }
  };

  _openLink = (e) => {
    const link = e.currentTarget.getAttribute('data-link');
    const linkType = e.currentTarget.getAttribute('data-link-type');
    if (linkType === 'external') {
      const win = window.open(link, '_blank');
      win.focus();
    } else {
      this.props.history.push(link);
    }
  };

  render() {
    const lang = this.props.user.lang;
    const currentTop = this.state.top && this.state.top[0] ?
      this.state.top[this.state.currentTop] : null;
    return (
      <Wrap>
        {
          currentTop ? (
            <BlockTop
              onClick={this._openLink}
              data-link={currentTop.link}
              data-link-type={currentTop.linkType}
              bg={{
                small: require(`./img/${currentTop.img}.jpg`),
                medium: require(`./img/${currentTop.img}@2x.jpg`),
                large: require(`./img/${currentTop.img}@3x.jpg`),
              }}
            >
              <Overlay />
              <Title size={33} align="right" padding="0 30px">
                {currentTop.lang[lang]}
              </Title>
            </BlockTop>
          ) : null
        }
        <WrapBlockSide>
          <BlockSide
            onClick={this._openLink}
            data-link="https://murkinat.appspot.com/"
            data-link-type="external"
            bg={{
              small: require('./img/lunch.jpg'),
              medium: require('./img/lunch@2x.jpg'),
              large: require('./img/lunch@3x.jpg'),
            }}
          >
            <Overlay />
            <Title size={25} align="right" padding="0 24px">
              {this.props.user.lang === 'fi' ? 'Lounaslistat' : 'Lunches'}
            </Title>
          </BlockSide>
          <BlockSide
            onClick={this._openLink}
            data-link="/events"
            data-link-type="internal"
            bg={{
              small: require('./img/event_dash.jpg'),
              medium: require('./img/event_dash@2x.jpg'),
              large: require('./img/event_dash@3x.jpg'),
            }}
          >
            <Overlay />
            <Title size={25} align="right" padding="0 24px">
              {
                this.state.upcoming ?
                  (
                    <FormattedPlural
                      value={this.state.upcoming}
                      other={this.props.intl.formatMessage(homeLocales.otherUpcomingEvent, {
                        event: this.state.upcoming,
                      })}
                      one={this.props.intl.formatMessage(homeLocales.oneUpcomingEvent, {
                        event: this.state.upcoming,
                      })}
                    />
                  ) : this.props.intl.formatMessage(homeLocales.noUpcomingEvent)
              }
            </Title>
          </BlockSide>
        </WrapBlockSide>
      </Wrap>
    );
  }
}

Home.propTypes = {
  intl: intlShape,
  history: PropTypes.object,
  user: PropTypes.object,
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default connect(mapStateToProps, null)(injectIntl(Home));
