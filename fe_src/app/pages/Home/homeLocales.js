import { defineMessages } from 'react-intl';

const homeLocales = defineMessages({
  noUpcomingEvent: {
    id: 'home.noUpcomingEvent',
    defaultMessage: 'Events',
  },
  oneUpcomingEvent: {
    id: 'home.oneUpcomingEvent',
    defaultMessage: '{event} up coming event',
  },
  otherUpcomingEvent: {
    id: 'home.otherUpcomingEvent',
    defaultMessage: '{event} up coming events',
  },
});

export default homeLocales;
