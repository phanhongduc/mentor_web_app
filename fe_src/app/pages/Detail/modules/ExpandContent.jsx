import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { UnmountClosed } from 'react-collapse';
import { IconPlus, IconMinus } from '../../../components/Icon';

const Wrap = styled.div`
  padding: ${({ type }) => type === 'subPage' ? '0' : '10px 25px'};
`;

const ButtonExpand = styled.div`
  position: relative;
  padding: 14px;
  background-color: ${({ isOpened }) => isOpened ? 'rgba(0,0,0,.6)' : '#95989A'};
  color: #fff;
  text-align: center;
  font-size: 18px;
  border-radius: 5px
`;

const Icon = styled.div`
  position: absolute;
  font-size: 16px;
  top: 0;
  left: 10px;
  bottom: 0;
  display: flex;
  align-items: center;
`;

const Content = styled.div`
  padding: 15px 13px;
  box-shadow: 0px 3px 6px rgba(0,0,0,0.16);
`;

const SubPageButton = styled.div`
  margin-top: 10px;
  padding: 14px;
  background-color: #ccc;
  font-size: 18px;
  color: #fff;
`;

class ExpandContent extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpened: false,
    };
  }

  _open = () => {
    this.setState({
      isOpened: !this.state.isOpened,
    });
  };

  _renderButton = () => {
    if (this.props.type === 'subPage') {
      return (
        <SubPageButton isOpened={this.state.isOpened} onClick={this._open}>
          {this.props.title}
        </SubPageButton>
      );
    }

    return (

      <ButtonExpand isOpened={this.state.isOpened} onClick={this._open}>
        <Icon>
          <IconMinus hidden={!this.state.isOpened} />
          <IconPlus hidden={this.state.isOpened} />
        </Icon>
        {this.props.title}
      </ButtonExpand>
    );
  }

  render() {
    return (
      <Wrap type={this.props.type}>
        {this._renderButton()}
        <UnmountClosed isOpened={this.state.isOpened}>
          <Content>
            {this.props.children}
          </Content>
        </UnmountClosed>
      </Wrap>
    );
  }
}

ExpandContent.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string,
  type: PropTypes.string,
};


export default ExpandContent;
