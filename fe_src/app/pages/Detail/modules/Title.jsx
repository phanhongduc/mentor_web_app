import styled from 'styled-components';

const Title = styled.div`
  text-align: center;
  color: #fff;
  padding 12px;
  background-color: #A50082;
  font-size: 20px;
`;

export default Title;
