import { defineMessages } from 'react-intl';

const messages = defineMessages({
  confirmLogout: {
    id: 'user.confirmLogout',
    defaultMessage: 'Are you sure you want to logout?',
  },
  welcome: {
    id: 'about.welcome',
    defaultMessage: 'Welcome to My mobile tutor!',
  },
  enjoy: {
    id: 'about.enjoy',
    defaultMessage: 'Enjoy your stay in the city and University of Turku',
  },
  intro: {
    id: 'about.intro',
    defaultMessage: `The service is designed to help you as a student of University of Turku,
        to get to know your city and university,
        find places and create and attend various events.
        My mobile tutor is in a pilot phase,
        so any feedback for further development is highly appreciated.`,
  },
  languageChangeSuccess: {
    id: 'user.languageChangeSuccess',
    defaultMessage: 'Change the language success!',
  },
});


export default messages;
