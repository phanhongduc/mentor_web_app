import React from 'react';
import styled from 'styled-components';
import { UnmountClosed } from 'react-collapse';
import { pure, compose, withState, withHandlers } from 'recompose';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import trunc from 'trunc-html';
import Button from '../../../components/Button';
import { IconStar, IconHeart } from '../../../components/Icon';
import config from '../../../styledConfig';
import { Spacing } from '../../../components/Layout/';

const WrapAll = styled.div`
  border-bottom: 1px solid ${config.colorBorder};
  overflow: hidden;
`;

const Wrap = styled.div`
  padding: 12px 16px;
  position: relative;
`;

const WrapWithBg = Wrap.extend`
  color: #fff;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover;
  background-image: ${({ bg }) => `url("${bg}")`};
`;

const Intro = styled.div`
  position: relative;
  z-index: 1;
`;

const Detail = styled.div`
  font-size: 18px;
  box-sizing: border-box;
  padding: 24px 16px 20px 16px;
`;

const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #000;
  opacity: .3;
`;

const Star = styled(IconStar)`
  position: absolute;
  right: 5px;
  top: 5px;
  font-size: 17px;
`;

const Title = styled.div`
  font-size: 41px;
  margin-bottom: 5px;
`;

const Address = styled.div`
  font-size: 18px;
`;

const DateTime = styled.div`
  margin-top: 3px;
  font-size: 18px;
`;

const Heart = styled(IconHeart)`
  font-size: 12px;
  margin-right: 5px;
`;

const LinkURL = styled.div`
  color: #1437A5;
`;

const ShowOnMap = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const AuthorAction = styled.div`
  & > button {
    margin-left: 5px;
  } 
`;

function formatDate(date) {
  if (!date) return date;
  const arrDate = date.split('-');
  return `${arrDate[2]}.${arrDate[1]}.${arrDate[0]}`;
}

function formatTime(time) {
  return !time ? '' : time.slice(0, time.length - 3);
}

function formatLongDesc(text, isFull) {
  return {
    __html: isFull ? text : trunc(text, 120).html,
  };
}

function formatCategs(categs, data, lang) {
  if (categs) {
    const cats = categs.replace(new RegExp(/\"|\{|\}/, 'g'), '').split(',');
    return cats.reduce((format, cat) => {
      data.forEach((da) => {
        if (da.id === cat) {
          return format.push(JSON.parse(da.content)[lang]);
        }
      });
      return format;
    }, []).join(', ');
  }
  return '';
}


const Event = ({
  isOwned,
  onClick,
  onClickEdit,
  onClickDelete,
  onClickFav,
  toggleOpenTextDetail,
  isOpened,
  isOpenTextDetail,
  bg,
  title,
  address,
  stime,
  etime,
  sdate,
  edate,
  ldescr,
  categs,
  contact,
  email,
  tel,
  url,
  lat,
  longt,
  messages,
  deleteText,
  editText,
  categsData,
  lang,
  ...props
}) => {
  const GirdIntro = bg ? WrapWithBg : Wrap;
  const formattedCategs = formatCategs(categs, categsData, lang);
  const formattedSDate = formatDate(sdate);
  const formattedEDate = formatDate(edate);
  const formattedStime = formatTime(stime);
  const formattedEtime = formatTime(etime);
  return (
    <WrapAll {...props}>
      <GirdIntro bg={bg} onClick={onClick}>
        {bg ? (<Overlay />) : ''}
        <Intro>
          <Star hidden onClick={onClickFav} />
          <Title>
            {title}
          </Title>
          <div hidden={isOpened}>
            <Address>
              {address}
            </Address>
            <DateTime>
              {formattedSDate}{formattedEDate ? ` - ${formattedEDate}` : ''}
            </DateTime>
            <DateTime>
              {formattedStime}{etime ? ` - ${formattedEtime}` : ''}
            </DateTime>
          </div>
        </Intro>
      </GirdIntro>
      <UnmountClosed isOpened={(isOpened === true || isOpened === false) ? isOpened : false}>
        <Detail>
          <div
            onClick={toggleOpenTextDetail}
            dangerouslySetInnerHTML={formatLongDesc(ldescr, isOpenTextDetail)}
          />
          {
            formattedCategs ? (
              <Spacing top="13px">
                <Heart />
                <span>{formattedCategs}</span>
              </Spacing>
            ) : ''
          }
          <Spacing top="13px">{address}</Spacing>
          {
            lat && longt ? (
              <ShowOnMap>
                <Link to={`/map?event=${title}&lat=${lat}&longt=${longt}`}>
                  <Button>{messages.showOnMap}</Button>
                </Link>
              </ShowOnMap>
            ) : null
          }
          {
            formattedSDate ? (
              <Spacing top="13px">{messages.date} {formattedSDate} </Spacing>
            ) : ''
          }
          {
            stime ? (
              <Spacing top="11px" left="20px">{messages.time} {formattedStime}{etime ? ` - ${formattedEtime}` : ''}</Spacing>
            ) : ''
          }
          {
            contact ? (
              <Spacing top="13px">{messages.contact} <span>{contact}</span></Spacing>
            ) : ''
          }
          {
            email || tel ? (
              <Spacing top="11px" left="20px">{tel} {email}</Spacing>
            ) : ''
          }
          {
           url ? (
             <Spacing top="21px">
               <LinkURL>
                 <a href={url} target="_blank" rel="noopener">{url}</a>
               </LinkURL>
             </Spacing>
           ) : ''
          }
          {
            isOwned ? (
              <Spacing top="20px">
                <AuthorAction>
                  <Button danger onClick={onClickDelete}>{deleteText}</Button>
                  <Button onClick={onClickEdit}>{editText}</Button>
                </AuthorAction>
              </Spacing>
            ) : null
          }
        </Detail>
      </UnmountClosed>
    </WrapAll>
  );
};

Event.propTypes = {
  isOwned: PropTypes.bool,
  lang: PropTypes.string,
  editText: PropTypes.string,
  deleteText: PropTypes.string,
  bg: PropTypes.string,
  lat: PropTypes.string,
  longt: PropTypes.string,
  title: PropTypes.string.isRequired,
  address: PropTypes.string,
  stime: PropTypes.string,
  etime: PropTypes.string,
  sdate: PropTypes.string,
  edate: PropTypes.string,
  ldescr: PropTypes.string,
  categs: PropTypes.string,
  categsData: PropTypes.array,
  contact: PropTypes.string,
  email: PropTypes.string,
  url: PropTypes.string,
  tel: PropTypes.string,
  isOpened: PropTypes.bool,
  isOpenTextDetail: PropTypes.bool,
  onClickIntro: PropTypes.func,
  onClick: PropTypes.func,
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func,
  onClickFav: PropTypes.func,
  toggleOpenTextDetail: PropTypes.func,
  messages: PropTypes.object.isRequired,
};

const EventComposed = compose(
  withState('isOpenTextDetail', 'toggleOpenTextDetail', false),
  withHandlers({
    toggleOpenTextDetail: ({ toggleOpenTextDetail }) => () => toggleOpenTextDetail(bool => !bool),
    onClick: props => () => {
      props.onClickIntro(props.index);
    },
    onClickEdit: props => () => {
      props.onClickEdit(props.index);
    },
    onClickDelete: props => () => {
      props.onClickDelete(props.index);
    },
    onClickFav: props => () => {
      props.onClickFav(props.index);
    },
  }),
)(Event);

export default pure(EventComposed);
