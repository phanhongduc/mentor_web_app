import { defineMessages } from 'react-intl';

const messages = defineMessages({
  new: {
    id: 'events.new',
    defaultMessage: 'New event',
  },
  update: {
    id: 'events.update',
    defaultMessage: 'Edit event',
  },
  addSuccess: {
    id: 'events.addEventSuccess',
    defaultMessage: 'Add event success',
  },
  updateSuccess: {
    id: 'events.updateEventSuccess',
    defaultMessage: 'Update event success',
  },
  deleteSuccess: {
    id: 'events.deleteEventSuccess',
    defaultMessage: 'Delete event success',
  },
  confirmDelete: {
    id: 'events.confirmDelete',
    defaultMessage: 'Are you sure you want to delete?',
  },
  showOnMap: {
    id: 'events.showOnMap',
    defaultMessage: 'Show on map',
  },
  chooseOnMap: {
    id: 'events.chooseOnMap',
    defaultMessage: 'Choose on map',
  },
  clearLocation: {
    id: 'events.clearLocation',
    defaultMessage: 'Clear location',
  },
  warnEditLocation: {
    id: 'events.warnEditLocation',
    defaultMessage: 'Edit address directly will delete the location on the map.',
  },
  clickOrSearch: {
    id: 'events.clickOrSearch',
    defaultMessage: 'Search address or click on the map',
  },
  setAddress: {
    id: 'events.setAddress',
    defaultMessage: 'Set this address',
  },
});

export default messages;
