import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';
import { defaultRules } from 'react-hoc-form-validatable';
import { ModalBase } from '../../../components/Modal';
import AddEventForm from '../forms/AddEventForm';

const AddEventModal = ({
  chooseMap,
  contentLabel,
  onRequestClose,
  isOpen,
  subscriptions,
  memberGroupsData,
  submitHandler,
  event,
}) => (
  <ModalBase
    isOpen={isOpen}
    onRequestClose={onRequestClose}
    contentLabel={contentLabel}
  >
    <AddEventForm
      chooseMap={chooseMap}
      event={event}
      onRequestClose={onRequestClose}
      memberGroupsData={memberGroupsData}
      subscriptions={subscriptions}
      submitCallback={submitHandler}
      validateLang="en"
      rules={defaultRules}
    />
  </ModalBase>
);

AddEventModal.propTypes = {
  event: PropTypes.object,
  submitHandler: PropTypes.func,
  chooseMap: PropTypes.func,
  subscriptions: PropTypes.array,
  memberGroupsData: PropTypes.array,
  isOpen: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  contentLabel: PropTypes.string,
};

export default pure(AddEventModal);
