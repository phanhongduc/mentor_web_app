import update from 'immutability-helper';

const PoiReducer = (state, action) => {
  switch (action.type) {
    case 'SET_POIS':
      return update(state, { $set: action.payload.response });
    case 'PUSH_POI':
      return update(state, { $push: action.payload.response });
    case 'UPDATE_POIS_ENTITY':
      return update(state, { $splice: [[action.meta.index, 1, action.payload.response[0]]] });
    case 'DELETE_POIS_ENTITY':
      return update(state, { $splice: [[action.payload, 1]] });
    default:
      return state || [];
  }
};

export default PoiReducer;
