import { put, takeLatest, all, call } from 'redux-saga/effects';
import { fetchApi, APIError } from '../../helpers/api';

function* getEvents(action) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const filterDate = action.filters && action.filters.filterDate ?
      action.filters.filterDate : 'all';
    const filterInterest = action.filters && action.filters.filterInterest ?
      action.filters.filterInterest : '';

    const res = yield call(fetchApi,
      `${process.env.API_URL}/events?filters=${filterDate}&fcategs={${filterInterest}}`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      },
    );

    yield put({
      type: 'SET_EVENTS',
      payload: res || [],
    });

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    action.meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  }
}

function* createEvent({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const res = yield call(fetchApi,
      `${process.env.API_URL}/events`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify(payload),
      },
    );

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });

    meta.callback(true);

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
    meta.callback();
  }
}

function* updateEvent({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const res = yield call(fetchApi,
      `${process.env.API_URL}/events`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'put',
        body: JSON.stringify(payload),
      },
    );

    yield put({
      type: 'UPDATE_EVENTS_ENTITY',
      payload: res,
      meta: {
        index: meta.index,
      },
    });

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });

    meta.callback(true);

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
    meta.callback();
  }
}

function* deleteEvent({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    yield call(fetchApi,
      `${process.env.API_URL}/events`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'delete',
        body: JSON.stringify({
          id: payload.id,
        }),
      },
    );

    yield put({
      type: 'DELETE_EVENTS_ENTITY',
      payload: payload.index,
    });

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.callback();

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
  }
}

function* favEvent({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    yield call(fetchApi,
      `${process.env.API_URL}/user-event`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify({
          id: payload.id,
        }),
      },
    );

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });


    // meta.addNotification({
    //   message: meta.successMessage,
    //   level: 'success',
    // });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    // meta.addNotification({
    //   message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
    //   level: 'error',
    // });
  }
}

function* watchSagaEvents() {
  yield takeLatest('GET_EVENTS', getEvents);
  yield takeLatest('CREATE_EVENT', createEvent);
  yield takeLatest('UPDATE_EVENT', updateEvent);
  yield takeLatest('DELETE_EVENT', deleteEvent);
  yield takeLatest('FAV_EVENT', favEvent);
}

export default function* rootSaga() {
  yield all([
    watchSagaEvents(),
  ]);
}
