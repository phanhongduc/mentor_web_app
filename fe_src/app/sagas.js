import { takeLatest, all } from 'redux-saga/effects';

function* userLogin() {
  yield takeLatest('USER_LOGIN', userLogin);
}

function* watchUser() {
  yield takeLatest('USER_LOGIN', userLogin);
}

export default function* rootSaga() {
  yield all([
    watchUser(),
  ]);
}
