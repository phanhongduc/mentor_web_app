import { defineMessages } from 'react-intl';

const messages = defineMessages({
  // global: {
  //   language: {
  //     id: 'global.language',
  //     defaultMessage: 'Language',
  //   },
  //   about: {
  //     id: 'global.about',
  //     defaultMessage: 'About',
  //   },
  //   cancel: {
  //     id: 'global.cancel',
  //     defaultMessage: 'Cancel',
  //   },
  //   feedback: {
  //     id: 'global.feedback',
  //     defaultMessage: 'Feedback',
  //   },
  // },
  // filters: {
  //   all: {
  //     id: 'filters.all',
  //     defineMessages: 'All',
  //   },
  //   lastMonth: {
  //     id: 'filters.lastMonth',
  //     defineMessages: 'Last month',
  //   },
  //   lastWeek: {
  //     id: 'filters.lastWeek',
  //     defineMessages: 'Last week',
  //   },
  //   yesterday: {
  //     id: 'filters.yesterday',
  //     defineMessages: 'Yesterday',
  //   },
  //   today: {
  //     id: 'filters.today',
  //     defineMessages: 'Yesterday',
  //   },
  //   nextWeek: {
  //     id: 'filters.nextWeek',
  //     defineMessages: 'Next week',
  //   },
  //   nextMonth: {
  //     id: 'filters.nextMonth',
  //     defineMessages: 'Next month',
  //   },
  //   future: {
  //     id: 'filters.future',
  //     defineMessages: 'In the future',
  //   },
  //   wot: {
  //     id: 'filters.wot',
  //     defineMessages: 'Without time',
  //   },
  // },
  // about: {
  //   welcome: {
  //     id: 'about.welcome',
  //     defaultMessage: 'Welcome to My mobile tutor!',
  //   },
  //   enjoy: {
  //     id: 'about.enjoy',
  //     defaultMessage: 'Enjoy your stay in the city and University of Turku',
  //   },
  //   intro: {
  //     id: 'about.intro',
  //     defaultMessage: `The service is designed to help you as a student of University of Turku,
  //       to get to know your city and university,
  //       find places and create and attend various events.
  //       My mobile tutor is in a pilot phase,
  //       so any feedback for further development is highly appreciated.`,
  //   },
  // },
});

export default messages;
