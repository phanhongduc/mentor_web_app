import { combineReducers } from 'redux';
import update from 'immutability-helper';

import subscriptionsReducer from './modules/Subscriptions/SubscriptionsReducer';
import poiReducer from './modules/Poi/PoiReducer';
import searchReducer from './modules/Searchs/SearchReducer';

const EMPTY_ARRAY = [];
const EMPTY_OBJECT = {};
const user = (state, action) => {
  switch (action.type) {
    case 'CHECK_USER_RESPONSE':
      return update(state, { $set: action.payload.response });
    case 'CHANGE_LANGUAGE':
      return update(state, { lang: { $set: action.payload.lang } });
    default:
      return state || EMPTY_OBJECT;
  }
};

const events = (state, action) => {
  switch (action.type) {
    case 'SET_EVENTS': {
      const response = action.payload;
      return state.length === 0 ?
        update(state, { $push: response && response.response ? response.response : [] })
        : update(state, { $set: response && response.response ? response.response : [] });
    }
    case 'UPDATE_EVENTS_ENTITY' : {
      const newEventUpdated = action.payload.response[0];
      return update(state, { $splice: [[action.meta.index, 1, newEventUpdated]] });
    }
    case 'DELETE_EVENTS_ENTITY' : {
      return update(state, { $splice: [[action.payload, 1]] });
    }
    default:
      return state || EMPTY_ARRAY;
  }
};

const loading = (state, action) => {
  switch (action.type) {
    case 'LOADING_FULL_PAGE':
      return update(state, { isFullPage: {
        $set: action.payload,
      } });
    default:
      return state || EMPTY_OBJECT;
  }
};

const map = (state, action) => {
  switch (action.type) {
    case 'MAP_SHOW_POI':
      return update(state, { currentPoi: {
        $set: action.payload,
      } });
    default:
      return state || EMPTY_OBJECT;
  }
};

const lang = (state, action) => {
  switch (action.type) {
    case 'SET_LANG': {
      const response = action.payload;
      return update(state, { $set: response.response });
    }
    default:
      return state || EMPTY_OBJECT;
  }
};

const reducers = combineReducers({
  search: searchReducer,
  pois: poiReducer,
  user,
  events,
  loading,
  subscriptions: subscriptionsReducer,
  map,
  lang,
});

export default reducers;
