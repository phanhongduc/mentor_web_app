import { AppContainer } from 'react-hot-loader';

import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';

import App from './app/App';

// if (process.env.NODE_ENV !== 'production') {
//   {
//     let createClass = React.createClass;
//     Object.defineProperty(React, 'createClass', {
//       set: (nextCreateClass) => {
//         createClass = nextCreateClass;
//       },
//     });
//   }
//   const { whyDidYouUpdate } = require('why-did-you-update');
//   whyDidYouUpdate(React);
// }

const rootEl = document.getElementById('root');
const render = (Component) => {
  if (!window.Intl) {
    require.ensure([
      'intl',
      'intl/locale-data/jsonp/en.js',
      'intl/locale-data/jsonp/es.js',
      'intl/locale-data/jsonp/fr.js',
      'intl/locale-data/jsonp/it.js',
    ], (require) => {
      require('intl');
      require('intl/locale-data/jsonp/en.js');
      require('intl/locale-data/jsonp/fi.js');
      return ReactDOM.render(
        <AppContainer>
          <Component />
        </AppContainer>,
        rootEl,
      );
    });
  } else {
    return ReactDOM.render(
      <AppContainer>
        <Component />
      </AppContainer>,
      rootEl,
    );
  }
};

render(App);
if (module.hot) module.hot.accept('./app/App', () => render(App));
