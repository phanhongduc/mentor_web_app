import 'picturefill';

(function init() {
  const linkExternal = document.querySelectorAll('a[data-external]')[0];
  const $frame = document.getElementById('frame');
  linkExternal.addEventListener('click', (e) => {
    e.preventDefault();
    const link = e.currentTarget.getAttribute('data-href');
    if ($frame.firstElementChild.src !== link) {
      $frame.firstElementChild.src = link;
    }
    $frame.style.display = 'block';
  }, false);

  $frame.lastElementChild.addEventListener('click', () => {
    $frame.style.display = 'none';
  }, false);
}());
