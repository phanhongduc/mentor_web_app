--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

-- DROP SEQUENCE user_event_id_seq;

CREATE SEQUENCE user_event_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 9
  CACHE 1;
ALTER TABLE user_event_id_seq OWNER TO choos562;


--
-- Name: user_event; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE user_event (
    id integer DEFAULT nextval('user_event_id_seq'::regclass) NOT NULL,
    email text NOT NULL,
    event_id integer NOT NULL,
    favorite_flag boolean DEFAULT true NOT NULL,
    description text
);


ALTER TABLE public.user_event OWNER TO choos562;

--
-- Data for Name: user_event; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY user_event (id, email, event_id, favorite_flag, description) FROM stdin;
30	teppo@yliopisto.fi	16	t	
31	teppo@yliopisto.fi	17	t	
\.


--
-- PostgreSQL database dump complete
--

