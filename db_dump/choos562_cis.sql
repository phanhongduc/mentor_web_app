--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: apikeys; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE apikeys (
    id text NOT NULL,
    key text
);


ALTER TABLE public.apikeys OWNER TO choos562;

--
-- Name: apps; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE apps (
    id integer NOT NULL,
    name text,
    rungroups text[],
    owner text,
    modelurl text,
    iconurl text
);


ALTER TABLE public.apps OWNER TO choos562;

--
-- Name: apps_id_seq; Type: SEQUENCE; Schema: public; Owner: choos562
--

CREATE SEQUENCE apps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.apps_id_seq OWNER TO choos562;

--
-- Name: apps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: choos562
--

ALTER SEQUENCE apps_id_seq OWNED BY apps.id;


--
-- Name: apps_id_seq; Type: SEQUENCE SET; Schema: public; Owner: choos562
--

SELECT pg_catalog.setval('apps_id_seq', 24, true);


--
-- Name: groups; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    gname text,
    owner text
);


ALTER TABLE public.groups OWNER TO choos562;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: choos562
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO choos562;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: choos562
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: choos562
--

SELECT pg_catalog.setval('groups_id_seq', 53, true);


--
-- Name: logins; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE logins (
    id integer NOT NULL,
    email text,
    psword text,
    ownergroups text[],
    membergroups text[],
    name text,
    lang text DEFAULT 'en'::text
);


ALTER TABLE public.logins OWNER TO choos562;

--
-- Name: logins_id_seq; Type: SEQUENCE; Schema: public; Owner: choos562
--

CREATE SEQUENCE logins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.logins_id_seq OWNER TO choos562;

--
-- Name: logins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: choos562
--

ALTER SEQUENCE logins_id_seq OWNED BY logins.id;


--
-- Name: logins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: choos562
--

SELECT pg_catalog.setval('logins_id_seq', 126, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: choos562
--

ALTER TABLE ONLY apps ALTER COLUMN id SET DEFAULT nextval('apps_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: choos562
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: choos562
--

ALTER TABLE ONLY logins ALTER COLUMN id SET DEFAULT nextval('logins_id_seq'::regclass);


--
-- Data for Name: apikeys; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY apikeys (id, key) FROM stdin;
android	AIzaSyCAzHzxLh0q0D8_5Xbo0geT-TrG1MyVXV4
web	AIzaSyDXUJpVG-9VwNCRg6E2GVkJenwOxJhzx3Y
\.


--
-- Data for Name: apps; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY apps (id, name, rungroups, owner, modelurl, iconurl) FROM stdin;
1	CIS Users Control Tool	{CISadmin}	Victor Panin	../cis/cis_users.php	
2	CIS Apps Control Tool	{CISadmin}	Victor Panin	../cis/cis_apps.php	
3	CIS Groups Control Tool	{CISadmin}	Victor Panin	../cis/cis_groups.php	
8	CIS Contracts Control Tool	{CISadmin}	Victor Panin	../cis/cis_contracts.php	
10	CIS Logins Control Tool	{CISadmin}	Victor Panin	../cis/cis_logins.php	
11	CYF Manager Contracts Control	{"CYF Manager"}	Victor Panin	../cyf/cyf_contracts.php	
13	Turku University Contracts Control	{"Turku University"}	Victor Panin	../cyf/cyf_contracts.php	
14	Turku University Users Control	{"Turku University"}	Victor Panin	../cyf/cyf_users.php	
16	CYF Manager User Events Tool	{"CYF Manager"}	Victor Panin	../cyf/cyf_events.php	
17	CYF Manager User POIs Tool	{"CYF Manager"}	Victor Panin	../cyf/cyf_pois.php	
18	CYF Manager User POI Categories Tool	{"CYF Manager"}	Victor Panin	../cyf/cyf_poicategories.php	
15	CYF Manager User Event Subscriptions Tool	{"CYF Manager"}	Victor Panin	../cyf/cyf_subscriptions.php	
19	CYF Manager User Pages Tool	{"CYF Manager"}	Victor Panin	../cyf/cyf_pages.php	
21	CYF Manager Groups Control	{"CYF Manager"}	Victor Panin	../cyf/cyf_groups.php	
6	CYF Digital Service for Manager	{"CYF Manager"}	Kate Panina	../cyf/cyf_digital_service.php	
12	CYF Manager Users Control	{"CYF Manager"}	Victor Panin	../cyf/cyf_users.php	
23	Mail	{"",All,guest}	Kate Panina	https://mail.google.com	
24	Google	{"",All,guest}	Kate Panina	https://www.google.com	
20	My mobile tutor	{"CYF Manager","CYF User","Turku University Student","Turku University Staff"}	Kate Panina	../cyf/cyf_digital_service_user.php	
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY groups (id, gname, owner) FROM stdin;
1	CISadmin	Victor Panin
2	CYFadmin	Victor Panin
5	CYF User	CYF Manager
6	Coca-Cola manager	CYF Manager
7	Coca-cola	Coca-Cola manager
8	Helsinki University manager	CYF Manager
9	Helsinki University	Helsinki University manager
10	Turku University Manager	CYF Manager
17	Turku University	Turku University Manager
18	Turku Municipality Manager	CYF Manager
19	Turku Municipality	Turku Municipality Manager
46	All	Victor Panin
4	CYF Manager	CYF Manager
29	Turku Municipality1	Turku Municipality1 Manager
49	Turku University Staff	Turku University Manager
43	samba	CYF Manager
48	Turku University Student	Turku University Manager
\.


--
-- Data for Name: logins; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY logins (id, email, psword, ownergroups, membergroups, name, lang) FROM stdin;
1	vic2pan@gmail.com	p@ssW0rd	{CISadmin,CYFadmin,CYFuser,CYFcustomer,CustomerUsers}	{CISadmin}	Victor Panin	en
2	hanna@gmail.com	p@ssW0rd	{"CYF User"}	{"CYF Manager"}	Hanna	en
3	hu@gmail.com	p@ssW0rd	{"Helsinki University"}	{"Helsinki University manager"}	Jukka Kola	en
4	tm1@gmail.com	p@ssW0rd	{"Turku Municipality1"}	{"Turku Municipality1 Manager"}	Minna Arve	en
5	CYFuser2@gmail.com	p@ssW0rd	{CYFuser2@gmail.com}	{"CYF User"}	CYFuser2	en
6	susanna@gmail.com	p@ssW0rd	{"CYF User"}	{"CYF Manager"}	Susanna	en
7	smlaht@utu.fi	p@ssW0rd	{smlaht@utu.fi}	{"Turku University Student"}	Susanna Lahtinen	en
8	kati.kaarlehto@utu.fi	p@ssW0rd	{kati.kaarlehto@utu.fi,"Turku University Student"}	{"Turku University Staff"}	Kati Kaarlehto	en
\.


--
-- Name: apikeys_pkey; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY apikeys
    ADD CONSTRAINT apikeys_pkey PRIMARY KEY (id);


--
-- Name: apps_name_key; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY apps
    ADD CONSTRAINT apps_name_key UNIQUE (name);


--
-- Name: apps_pkey; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY apps
    ADD CONSTRAINT apps_pkey PRIMARY KEY (id);


--
-- Name: groups_gname_key; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_gname_key UNIQUE (gname);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: logins_email_key; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY logins
    ADD CONSTRAINT logins_email_key UNIQUE (email);


--
-- Name: logins_pkey; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY logins
    ADD CONSTRAINT logins_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: apikeys; Type: ACL; Schema: public; Owner: choos562
--

REVOKE ALL ON TABLE apikeys FROM PUBLIC;
REVOKE ALL ON TABLE apikeys FROM choos562;
GRANT ALL ON TABLE apikeys TO choos562;
GRANT ALL ON TABLE apikeys TO choos562_cis;


--
-- Name: apps; Type: ACL; Schema: public; Owner: choos562
--

REVOKE ALL ON TABLE apps FROM PUBLIC;
REVOKE ALL ON TABLE apps FROM choos562;
GRANT ALL ON TABLE apps TO choos562;
GRANT ALL ON TABLE apps TO choos562_cis;


--
-- Name: groups; Type: ACL; Schema: public; Owner: choos562
--

REVOKE ALL ON TABLE groups FROM PUBLIC;
REVOKE ALL ON TABLE groups FROM choos562;
GRANT ALL ON TABLE groups TO choos562;
GRANT ALL ON TABLE groups TO choos562_cis;


--
-- Name: logins; Type: ACL; Schema: public; Owner: choos562
--

REVOKE ALL ON TABLE logins FROM PUBLIC;
REVOKE ALL ON TABLE logins FROM choos562;
GRANT ALL ON TABLE logins TO choos562;
GRANT ALL ON TABLE logins TO choos562_cis;


--
-- PostgreSQL database dump complete
--

